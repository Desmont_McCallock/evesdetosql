﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace EVESDEToSQL.Constants
{
    internal static class TableNamesConstant
    {
        internal const string CrtCertificateTableName = "crtCertificates";
        internal const string CrtClassesTableName = "crtClasses";
        internal const string CrtRecommendationsTableName = "crtRecommendations";
        internal const string CrtRelationshipsTableName = "crtRelationships";
        internal const string DgmMasteriesTableName = "dgmMasteries";
        internal const string DgmTraitsTableName = "dgmTraits";
        internal const string DgmTypeMasteriesTableName = "dgmTypeMasteries";
        internal const string DgmTypeTraitsTableName = "dgmTypeTraits";
        internal const string EveGraphicsTableName = "eveGraphics";
        internal const string EveIconsTableName = "eveIcons";
        internal const string InvBlueprintTypesTableName = "invBlueprintTypes";
        internal const string InvCategoriesTableName = "invCategories";
        internal const string InvGroupsTableName = "invGroups";
        internal const string InvTypesTableName = "invTypes";
        internal const string InvNamesTableName = "invNames";
        internal const string RamTypeRequirementsTableName = "ramTypeRequirements";
        internal const string TranslationTableName = "translationTables";
        internal const string TrnTranslationColumnsTableName = "trnTranslationColumns";
        internal const string TrnTranslationsTableName = "trnTranslations";
        internal const string MapCelestialStatisticsTableName = "mapCelestialStatistics";
        internal const string MapConstellationJumpsTableName = "mapConstellationJumps";
        internal const string MapConstellationsTableName = "mapConstellations";
        internal const string MapDenormalizeTableName = "mapDenormalize";
        internal const string MapJumpsTableName = "mapJumps";
        internal const string MapLandmarksTableName = "mapLandmarks";
        internal const string MapLocationScenesTableName = "mapLocationScenes";
        internal const string MapLocationWormholeClassesTableName = "mapLocationWormholeClasses";
        internal const string MapRegionJumpsTableName = "mapRegionJumps";
        internal const string MapRegionsTableName = "mapRegions";
        internal const string MapSolarSystemJumpsTableName = "mapSolarSystemJumps";
        internal const string MapSolarSystemsTableName = "mapSolarSystems";
        internal const string SknLicensesTableName = "sknLicenses";
        internal const string SknMaterialsTableName = "sknMaterials";
        internal const string SknSkinsTableName = "sknSkins";
    }
}
