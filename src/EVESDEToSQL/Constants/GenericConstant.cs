﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace EVESDEToSQL.Constants
{
    internal static class GenericConstant
    {
        internal const string StarText = "star";
        internal const string PlanetsText = "planets";
        internal const string NpcStationsText = "npcStations";
        internal const string MoonsText = "moons";
        internal const string AsteroidBeltsText = "asteroidBelts";
        internal const string LocationIDText = "locationID";
        internal const string StargateIDText = "stargateID";
        internal const string CelestialIDText = "celestialID";
        internal const string ItemIDText = "itemID";
        internal const string RegionIDText = "regionID";
        internal const string ConstellationIDText = "constellationID";
        internal const string SolarSystemIDText = "solarSystemID";
        internal const string BlueprintTypeIDText = "blueprintTypeID";
        internal const string MaxProductionLimitText = "maxProductionLimit";
        internal const string QuantityText = "quantity";
        internal const string LevelText = "level";
        internal const string RaceIDText = "raceID";
        internal const string ProbabilityText = "probability";
        internal const string ConsumeText = "consume";

        // blueprints.yaml
        internal const string ActivitiesText = "activities";
        internal const string MaterialsText = "materials";
        internal const string ProductsText = "products";
        internal const string SkillsText = "skills";
        internal const string TimeText = "time";

        // invBlueprintTypes
        internal const string ParentBlueprintTypeIDText = "parentBlueprintTypeID";
        internal const string ProductTypeIDText = "productTypeID";
        internal const string ProductionTimeText = "productionTime";
        internal const string TechLevelText = "techLevel";
        internal const string ResearchProductivityTimeText = "researchProductivityTime";
        internal const string ResearchMaterialTimeText = "researchMaterialTime";
        internal const string ResearchCopyTimeText = "researchCopyTime";
        internal const string ResearchTechTimeText = "researchTechTime";
        internal const string DuplicatingTimeText = "duplicatingTime";
        internal const string ReverseEngineeringTimeText = "reverseEngineeringTime";
        internal const string InventionTimeText = "inventionTime";
        internal const string ProductivityModifierText = "productivityModifier";
        internal const string MaterialModifierText = "materialModifier";
        internal const string WasteFactorText = "wasteFactor";

        // ramTypeRequirements
        internal const string TypeIDText = "typeID";
        internal const string ActivityIDText = "activityID";
        internal const string RequiredTypeIDText = "requiredTypeID";
        internal const string DamagePerJobText = "damagePerJob";
        internal const string RecycleText = "recycle";

        internal const string NameText = "name";

        // invCategories
        internal const string CategoryIDText = "categoryID";
        internal const string CategoryNameText = "categoryName";
        internal const string DescriptionText = "description";
        internal const string IconIDText = "iconID";
        internal const string PublishedText = "published";

        // Translations
        internal const string TranslationCategoriesGroupID = "4";
        internal const string TranslationCategoriesID = "6";

        // certificates.yaml
        internal const string GroupIDText = "groupID";
        internal const string RecommendedForText = "recommendedFor";
        internal const string SkillTypesText = "skillTypes";

        // crtCertificates
        internal const string CertificateIDText = "certificateID";

        // crtClasses
        internal const string ClassIDText = "classID";
        internal const string ClassNameText = "className";

        // crtRecommendations
        internal const string RecommendationIDText = "recommendationID";
        internal const string ShipTypeIDText = "shipTypeID";

        // crtRelationships
        internal const string RelationshipIDText = "relationshipID";
        internal const string ParentTypeIDText = "parentTypeID";
        internal const string ParentLevelText = "parentLevel";
        internal const string ChildIDText = "childID";
        internal const string GradeText = "grade";

        internal const string GraphicIDText = "graphicID";
        internal const string GraphicFileText = "graphicFile";
        internal const string ObsoleteText = "obsolete";
        internal const string GraphicTypeText = "graphicType";
        internal const string CollidableText = "collidable";
        internal const string DirectoryIDText = "directoryID";
        internal const string GraphicNameText = "graphicName";
        internal const string GfxRaceIDText = "gfxRaceID";
        internal const string ColorSchemeText = "colorScheme";
        internal const string SofHullNameText = "sofHullName";

        internal const string GroupNameText = "groupName";
        internal const string UseBasePriceText = "useBasePrice";
        internal const string AllowManufactureText = "allowManufacture";
        internal const string AllowRecyclerText = "allowRecycler";
        internal const string AnchoredText = "anchored";
        internal const string AnchorableText = "anchorable";
        internal const string FittableNonSingletonText = "fittableNonSingleton";

        // Translations
        internal const string TranslationGroupsID = "7";
        internal const string TranslationGroupsGroupID = "5";

        internal const string IconFileText = "iconFile";

        internal const string LicenseTypeIDText = "licenseTypeID";
        internal const string SkinIDText = "skinID";
        internal const string DurationText = "duration";

        internal const string SkinMaterialIDText = "skinMaterialID";
        internal const string MaterialSetIDText = "materialSetID";
        internal const string DisplayNameIDText = "displayNameID";

        // Obsolete since Galatea 1.0
        internal const string MaterialText = "material";
        internal const string ColorHullText = "colorHull";
        internal const string ColorWindowText = "colorWindow";
        internal const string ColorPrimaryText = "colorPrimary";
        internal const string ColorSecondaryText = "colorSecondary";
        internal const string InternalNameText = "internalName";
        internal const string TypesText = "types";
        internal const string AllowCCPDevsText = "allowCCPDevs";
        internal const string VisibleSerenityText = "visibleSerenity";
        internal const string VisibleTranquilityText = "visibleTranquility";

        // translationTables and trnTranslationColumns and trnTranslations
        internal const string TcIDText = "tcID";

        // translationTables and trnTranslationColumns
        internal const string TcGroupIDText = "tcGroupID";

        // translationTables
        internal const string SourceTableText = "sourceTable";
        internal const string DestinationTableText = "destinationTable";
        internal const string TranslatedKeyText = "translatedKey";

        // trnTranslationColumns
        internal const string TableNameText = "tableName";
        internal const string ColumnNameText = "columnName";
        internal const string MasterIDText = "masterID";

        // trnTranslations
        internal const string KeyIDText = "keyID";
        internal const string LanguageIDText = "languageID";
        internal const string TextText = "text";

        internal const string EnglishLanguageIDText = "en";
        internal const string MasteriesText = "masteries";
        internal const string TraitsText = "traits";

        // Types
        internal const string TypeNameText = "typeName";
        internal const string MassText = "mass";
        internal const string VolumeText = "volume";
        internal const string CapacityText = "capacity";
        internal const string PortionSizeText = "portionSize";
        internal const string BasePriceText = "basePrice";
        internal const string MarketGroupIDText = "marketGroupID";
        internal const string ChanceOfDuplicatingText = "chanceOfDuplicating";
        internal const string FactionIDText = "factionID";
        internal const string RadiusText = "radius";
        internal const string SoundIDText = "soundID";

        // Traits
        internal const string BonusText = "bonus";
        internal const string BonusTextText = "bonusText";
        internal const string UnitIDText = "unitID";
        internal const string ImportanceText = "importance";

        // TypeTraits
        internal const string TraitIDText = "traitID";

        // TypeMasteries
        internal const string MasteryIDText = "masteryID";

        // Translations
        internal const string TranslationTypesGroupID = "5";
        internal const string TranslationTypesDescriptionID = "33";
        internal const string TranslationTypesTypeNameID = "8";
        internal const string TranslationTraitsGroupID = "90";
        internal const string TranslationTraitsBonusTextID = "140";

        internal const string LandmarkIDText = "landmarkID";
        internal const string LandmarkNameText = "landmarkName";

        internal const string FromRegionIDText = "fromRegionID";
        internal const string FromConstellationIDText = "fromConstellationID";
        internal const string FromSolarSystemIDText = "fromSolarSystemID";
        internal const string ToSolarSystemIDText = "toSolarSystemID";
        internal const string ToConstellationIDText = "toConstellationID";
        internal const string ToRegionIDText = "toRegionID";

        internal const string RegionNameText = "regionName";
        internal const string WormholeClassIDText = "wormholeClassID";
        internal const string StatisticsText = "statistics";
        internal const string ItemNameText = "itemName";
        internal const string ConstellationNameText = "constellationName";
        internal const string SecurityText = "security";
        internal const string SecondarySunText = "secondarySun";
        internal const string CelestialIndexText = "celestialIndex";
        internal const string StargatesText = "stargates";
        internal const string OrbitIDText = "orbitID";
        internal const string OrbitIndexText = "orbitIndex";
        internal const string SolarSystemNameText = "solarSystemName";
        internal const string LuminosityText = "luminosity";
        internal const string BorderText = "border";
        internal const string FringeText = "fringe";
        internal const string CorridorText = "corridor";
        internal const string HubText = "hub";
        internal const string InternationalText = "international";
        internal const string RegionalText = "regional";
        internal const string SunTypeIDText = "sunTypeID";
        internal const string SecurityClassText = "securityClass";
        internal const string TemperatureText = "temperature";
        internal const string SpectralClassText = "spectralClass";
        internal const string AgeText = "age";
        internal const string LifeText = "life";
        internal const string OrbitRadiusText = "orbitRadius";
        internal const string EccentricityText = "eccentricity";
        internal const string MassDustText = "massDust";
        internal const string MassGasText = "massGas";
        internal const string FragmentedText = "fragmented";
        internal const string DensityText = "density";
        internal const string SurfaceGravityText = "surfaceGravity";
        internal const string EscapeVelocityText = "escapeVelocity";
        internal const string OrbitPeriodText = "orbitPeriod";
        internal const string RotationRateText = "rotationRate";
        internal const string LockedText = "locked";
        internal const string PressureText = "pressure";

        internal const string CenterText = "center";
        internal const string PositionText = "position";
        internal const string IDText = "id";
        internal const string MaxText = "max";
        internal const string MinText = "min";

        internal const string MoonNameText = "moonName";
        internal const string AsteroidBeltNameText = "asteroidBeltName";
        internal const string OwnerNameText = "ownerName";
    }
}
