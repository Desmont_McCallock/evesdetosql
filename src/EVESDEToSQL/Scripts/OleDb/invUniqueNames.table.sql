﻿CREATE TABLE [invUniqueNames](
	[itemID] int NOT NULL,
	[itemName] varchar(200) NOT NULL,
	[groupID] int NULL,
 CONSTRAINT [invUniqueNames_PK] PRIMARY KEY ([itemID])
);

CREATE INDEX [invUniqueNames_IX_GroupName] ON [invUniqueNames] ([groupID] ASC, [itemName] ASC);

CREATE INDEX [invUniqueNames_UQ] ON [invUniqueNames] ([itemName] ASC);