﻿CREATE TABLE [planetSchematicsPinMap](
	[schematicID] smallint NOT NULL,
	[pinTypeID] int NOT NULL,
 CONSTRAINT [planetSchematicsPinMap_PK] PRIMARY KEY ([schematicID], [pinTypeID])
);