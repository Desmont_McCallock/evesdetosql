﻿CREATE TABLE [agtAgentTypes](
	[agentTypeID] int NOT NULL,
	[agentType] varchar(50) NULL,
 CONSTRAINT [agtAgentTypes_PK] PRIMARY KEY ([agentTypeID])
);