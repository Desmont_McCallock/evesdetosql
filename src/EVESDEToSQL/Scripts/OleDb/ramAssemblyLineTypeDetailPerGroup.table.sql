﻿CREATE TABLE [ramAssemblyLineTypeDetailPerGroup](
	[assemblyLineTypeID] tinyint NOT NULL,
	[groupID] int NOT NULL,
	[timeMultiplier] double NULL,
	[materialMultiplier] double NULL,
	[costMultiplier] double NULL,
 CONSTRAINT [ramAssemblyLineTypeDetailPerGroup_PK] PRIMARY KEY ([assemblyLineTypeID], [groupID])
);