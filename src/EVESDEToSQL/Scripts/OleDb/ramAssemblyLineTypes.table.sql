﻿CREATE TABLE [ramAssemblyLineTypes](
	[assemblyLineTypeID] tinyint NOT NULL,
	[assemblyLineTypeName] varchar(100) NULL,
	[description] memo NULL,
	[baseTimeMultiplier] double NULL,
	[baseMaterialMultiplier] double NULL,
	[baseCostMultiplier] double NULL,
	[volume] double NULL,
	[activityID] tinyint NULL,
	[minCostPerHour] double NULL,
 CONSTRAINT [ramAssemblyLineTypes_PK] PRIMARY KEY ([assemblyLineTypeID])
);