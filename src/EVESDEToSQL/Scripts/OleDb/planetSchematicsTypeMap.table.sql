﻿CREATE TABLE [planetSchematicsTypeMap](
	[schematicID] smallint NOT NULL,
	[typeID] int NOT NULL,
	[quantity] smallint NULL,
	[isInput] bit NULL,
 CONSTRAINT [planetSchematicsTypeMap_PK] PRIMARY KEY ([schematicID], [typeID])
);