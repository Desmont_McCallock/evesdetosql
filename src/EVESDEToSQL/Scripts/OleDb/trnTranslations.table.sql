﻿CREATE TABLE [trnTranslations](
	[tcID] smallint NOT NULL,
	[keyID] int NOT NULL,
	[languageID] varchar(50) NOT NULL,
	[text] memo NOT NULL,
 CONSTRAINT [trnTranslations_PK] PRIMARY KEY ([tcID], [keyID], [languageID])
);