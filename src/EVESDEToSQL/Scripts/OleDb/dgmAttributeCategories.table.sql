﻿CREATE TABLE [dgmAttributeCategories](
	[categoryID] tinyint NOT NULL,
	[categoryName] varchar(50) NULL,
	[categoryDescription] varchar(200) NULL,
 CONSTRAINT [dgmAttributeCategories_PK] PRIMARY KEY ([categoryID])
);