﻿CREATE TABLE [trnTranslationColumns](
	[tcGroupID] smallint NULL,
	[tcID] smallint NOT NULL,
	[tableName] memo NOT NULL,
	[columnName] varchar(128) NOT NULL,
	[masterID] varchar(128) NULL,
 CONSTRAINT [trnTranslationColumns_PK] PRIMARY KEY ([tcID])
);