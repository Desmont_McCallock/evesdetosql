﻿CREATE TABLE [planetSchematics](
	[schematicID] smallint NOT NULL,
	[schematicName] varchar(255) NULL,
	[cycleTime] int NULL,
 CONSTRAINT [planetSchematics_PK] PRIMARY KEY ([schematicID])
);