﻿CREATE TABLE [ramInstallationTypeContents](
	[installationTypeID] int NOT NULL,
	[assemblyLineTypeID] tinyint NOT NULL,
	[quantity] tinyint NULL,
 CONSTRAINT [ramInstallationTypeContents_PK] PRIMARY KEY ([installationTypeID], [assemblyLineTypeID])
);