﻿CREATE TABLE [invTypeReactions](
	[reactionTypeID] int NOT NULL,
	[input] bit NOT NULL,
	[typeID] int NOT NULL,
	[quantity] smallint NULL,
 CONSTRAINT [invTypeReactions_PK] PRIMARY KEY ([reactionTypeID], [input], [typeID])
);