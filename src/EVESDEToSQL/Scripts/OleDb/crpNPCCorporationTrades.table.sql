﻿CREATE TABLE [crpNPCCorporationTrades](
	[corporationID] int NOT NULL,
	[typeID] int NOT NULL,
 CONSTRAINT [crpNPCCorporationTrades_PK] PRIMARY KEY ([corporationID], [typeID])
);