﻿CREATE TABLE [mapUniverse](
	[universeID] int NOT NULL,
	[universeName] varchar(100) NULL,
	[x] double NULL,
	[y] double NULL,
	[z] double NULL,
	[xMin] double NULL,
	[xMax] double NULL,
	[yMin] double NULL,
	[yMax] double NULL,
	[zMin] double NULL,
	[zMax] double NULL,
	[radius] double NULL,
 CONSTRAINT [mapUniverse_PK] PRIMARY KEY ([universeID])
);