﻿CREATE TABLE [mapSolarSystems](
	[regionID] int NULL,
	[constellationID] int NULL,
	[solarSystemID] int NOT NULL,
	[solarSystemName] varchar(100) NULL,
	[x] double NULL,
	[y] double NULL,
	[z] double NULL,
	[xMin] double NULL,
	[xMax] double NULL,
	[yMin] double NULL,
	[yMax] double NULL,
	[zMin] double NULL,
	[zMax] double NULL,
	[luminosity] double NULL,
	[border] bit NULL,
	[fringe] bit NULL,
	[corridor] bit NULL,
	[hub] bit NULL,
	[international] bit NULL,
	[regional] bit NULL,
	[constellation] bit NULL,
	[security] double NULL,
	[factionID] int NULL,
	[radius] double NULL,
	[sunTypeID] int NULL,
	[securityClass] varchar(2) NULL,
 CONSTRAINT [mapSolarSystems_PK] PRIMARY KEY ([solarSystemID])
);

CREATE INDEX [mapSolarSystems_IX_constellation] ON [mapSolarSystems] ([constellationID] ASC);

CREATE INDEX [mapSolarSystems_IX_region] ON [mapSolarSystems] ([regionID] ASC);

CREATE INDEX [mapSolarSystems_IX_security] ON [mapSolarSystems] ([security] ASC);