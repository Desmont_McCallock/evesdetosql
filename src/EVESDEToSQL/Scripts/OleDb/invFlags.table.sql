﻿CREATE TABLE [invFlags](
	[flagID] smallint NOT NULL,
	[flagName] varchar(200) NULL,
	[flagText] varchar(100) NULL,
	[orderID] int NULL,
 CONSTRAINT [invFlags_PK] PRIMARY KEY ([flagID])
);