﻿CREATE TABLE [mapLandmarks](
	[landmarkID] smallint NOT NULL,
	[landmarkName] varchar(100) NULL,
	[description] memo NULL,
	[locationID] int NULL,
	[x] double NULL,
	[y] double NULL,
	[z] double NULL,
	[radius] double NULL,
	[iconID] int NULL,
	[importance] tinyint NULL,
 CONSTRAINT [mapLandmarks_PK] PRIMARY KEY ([landmarkID])
);