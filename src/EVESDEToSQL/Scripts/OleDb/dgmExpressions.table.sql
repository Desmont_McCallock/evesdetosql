﻿CREATE TABLE [dgmExpressions](
	[expressionID] int NOT NULL,
	[operandID] int NULL,
	[arg1] int NULL,
	[arg2] int NULL,
	[expressionValue] varchar(100) NULL,
	[description] memo NULL,
	[expressionName] memo NULL,
	[expressionTypeID] int NULL,
	[expressionGroupID] smallint NULL,
	[expressionAttributeID] smallint NULL,
 CONSTRAINT [dgmExpressions_PK] PRIMARY KEY ([expressionID])
);