﻿CREATE TABLE [sknSkins](
	[skinID] int NOT NULL,
	[internalName] varchar(100) NOT NULL DEFAULT '',
	[skinMaterialID] int NULL,
	[typeID] int NULL,
	[allowCCPDevs] bit NOT NULL DEFAULT 0,
	[visibleSerenity] bit NOT NULL DEFAULT 0,
	[visibleTranquility] bit NOT NULL DEFAULT 0,
 CONSTRAINT [sknSkins_PK] PRIMARY KEY ([skinID])
);

CREATE INDEX [sknSkins_IX_skinMaterial] ON [sknSkins] ([skinMaterialID] ASC);