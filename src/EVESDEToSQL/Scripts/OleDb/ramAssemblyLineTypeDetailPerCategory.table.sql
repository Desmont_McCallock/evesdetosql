﻿CREATE TABLE [ramAssemblyLineTypeDetailPerCategory](
	[assemblyLineTypeID] tinyint NOT NULL,
	[categoryID] int NOT NULL,
	[timeMultiplier] double NULL,
	[materialMultiplier] double NULL,
	[costMultiplier] double NULL,
 CONSTRAINT [ramAssemblyLineTypeDetailPerCategory_PK] PRIMARY KEY ([assemblyLineTypeID], [categoryID])
);