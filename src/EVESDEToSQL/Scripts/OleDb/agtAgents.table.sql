﻿CREATE TABLE [agtAgents](
	[agentID] int NOT NULL,
	[divisionID] tinyint NULL,
	[corporationID] int NULL,
	[locationID] int NULL,
	[level] tinyint NULL,
	[quality] smallint NULL,
	[agentTypeID] int NULL,
	[isLocator] bit NULL,
 CONSTRAINT [agtAgents_PK] PRIMARY KEY ([agentID])
);

CREATE INDEX [agtAgents_IX_corporation] ON [agtAgents] ([corporationID] ASC);

CREATE INDEX [agtAgents_IX_station] ON [agtAgents] ([locationID] ASC);