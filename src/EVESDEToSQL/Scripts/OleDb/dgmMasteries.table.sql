﻿CREATE TABLE [dgmMasteries](
	[masteryID] int NOT NULL,
	[certificateID] int NOT NULL,
	[grade] tinyint NOT NULL,
 CONSTRAINT [dgmMasteries_PK] PRIMARY KEY ([masteryID])
);

CREATE UNIQUE INDEX [dgmMasteries_UC_certificate_grade] ON [dgmMasteries] ([certificateID] ASC, [grade] ASC);

CREATE INDEX [dgmMasteries_IX_certificate] ON [dgmMasteries] ([certificateID] ASC);