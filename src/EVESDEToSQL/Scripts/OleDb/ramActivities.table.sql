﻿CREATE TABLE [ramActivities](
	[activityID] tinyint NOT NULL,
	[activityName] varchar(100) NULL,
	[iconNo] varchar(5) NULL,
	[description] memo NULL,
	[published] bit NULL,
 CONSTRAINT [ramActivities_PK] PRIMARY KEY ([activityID])
);