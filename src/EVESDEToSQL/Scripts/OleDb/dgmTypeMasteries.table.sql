﻿CREATE TABLE [dgmTypeMasteries](
	[typeID] int NOT NULL,
	[masteryID] smallint NOT NULL,
 CONSTRAINT [dgmTypeMasteries_PK] PRIMARY KEY ([typeID], [masteryID])
);