﻿CREATE TABLE [dgmTypeTraits](
	[typeID] int NOT NULL,
	[parentTypeID] int NOT NULL,
	[traitID] int NOT NULL,
	[bonus] double NULL,
 CONSTRAINT [dgmTypeTraits_PK] PRIMARY KEY ([typeID], [parentTypeID], [traitID])
);