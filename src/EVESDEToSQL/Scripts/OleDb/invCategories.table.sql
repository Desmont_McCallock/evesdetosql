﻿CREATE TABLE [invCategories](
	[categoryID] int NOT NULL,
	[categoryName] varchar(100) NULL,
	[description] memo NULL,
	[iconID] int NULL,
	[published] bit NULL,
 CONSTRAINT [invCategories_PK] PRIMARY KEY ([categoryID])
);