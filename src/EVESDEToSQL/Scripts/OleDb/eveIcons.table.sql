﻿CREATE TABLE [eveIcons](
	[iconID] int NOT NULL,
	[iconFile] memo NOT NULL DEFAULT '',
	[description] memo NOT NULL DEFAULT '',
 CONSTRAINT [eveIcons_PK] PRIMARY KEY ([iconID])
);