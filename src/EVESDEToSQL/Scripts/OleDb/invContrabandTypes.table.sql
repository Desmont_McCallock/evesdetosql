﻿CREATE TABLE [invContrabandTypes](
	[factionID] int NOT NULL,
	[typeID] int NOT NULL,
	[standingLoss] double NULL,
	[confiscateMinSec] double NULL,
	[fineByValue] double NULL,
	[attackMinSec] double NULL,
 CONSTRAINT [invContrabandTypes_PK] PRIMARY KEY ([factionID], [typeID])
);

CREATE INDEX [invContrabandTypes_IX_type] ON [invContrabandTypes] ([typeID] ASC);