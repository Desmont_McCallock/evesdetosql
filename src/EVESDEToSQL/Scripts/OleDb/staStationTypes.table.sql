﻿CREATE TABLE [staStationTypes](
	[stationTypeID] int NOT NULL,
	[dockEntryX] double NULL,
	[dockEntryY] double NULL,
	[dockEntryZ] double NULL,
	[dockOrientationX] double NULL,
	[dockOrientationY] double NULL,
	[dockOrientationZ] double NULL,
	[operationID] tinyint NULL,
	[officeSlots] tinyint NULL,
	[reprocessingEfficiency] double NULL,
	[conquerable] bit NULL,
 CONSTRAINT [staStationTypes_PK] PRIMARY KEY ([stationTypeID])
);