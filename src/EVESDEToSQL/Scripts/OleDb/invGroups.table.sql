﻿CREATE TABLE [invGroups](
	[groupID] int NOT NULL,
	[categoryID] int NULL,
	[groupName] varchar(100) NULL,
	[description] memo NULL,
	[iconID] int NULL,
	[useBasePrice] bit NULL,
	[allowManufacture] bit NULL,
	[allowRecycler] bit NULL,
	[anchored] bit NULL,
	[anchorable] bit NULL,
	[fittableNonSingleton] bit NULL,
	[published] bit NULL,
 CONSTRAINT [invGroups_PK] PRIMARY KEY ([groupID])
);

CREATE INDEX [invGroups_IX_category] ON [invGroups] ([categoryID] ASC);