﻿CREATE TABLE [crpNPCCorporationDivisions](
	[corporationID] int NOT NULL,
	[divisionID] tinyint NOT NULL,
	[size] tinyint NULL,
 CONSTRAINT [crpNPCCorporationDivisions_PK] PRIMARY KEY ([corporationID], [divisionID])
);