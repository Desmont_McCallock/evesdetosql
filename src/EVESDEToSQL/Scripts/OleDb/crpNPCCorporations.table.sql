﻿CREATE TABLE [crpNPCCorporations](
	[corporationID] int NOT NULL,
	[size] char(1) NULL,
	[extent] char(1) NULL,
	[solarSystemID] int NULL,
	[investorID1] int NULL,
	[investorShares1] tinyint NULL,
	[investorID2] int NULL,
	[investorShares2] tinyint NULL,
	[investorID3] int NULL,
	[investorShares3] tinyint NULL,
	[investorID4] int NULL,
	[investorShares4] tinyint NULL,
	[friendID] int NULL,
	[enemyID] int NULL,
	[publicShares] long NULL,
	[initialPrice] int NULL,
	[minSecurity] double NULL,
	[scattered] bit NULL,
	[fringe] tinyint NULL,
	[corridor] tinyint NULL,
	[hub] tinyint NULL,
	[border] tinyint NULL,
	[factionID] int NULL,
	[sizeFactor] double NULL,
	[stationCount] smallint NULL,
	[stationSystemCount] smallint NULL,
	[description] memo NULL,
	[iconID] int NULL,
 CONSTRAINT [crpNPCCorporations_PK] PRIMARY KEY ([corporationID])
);