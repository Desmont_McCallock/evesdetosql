﻿CREATE TABLE [invItems](
	[itemID] long NOT NULL,
	[typeID] int NOT NULL,
	[ownerID] int NOT NULL,
	[locationID] long NOT NULL,
	[flagID] smallint NOT NULL,
	[quantity] int NOT NULL,
 CONSTRAINT [invItems_PK] PRIMARY KEY ([itemID])
);

CREATE INDEX [items_IX_Location] ON [invItems] ([locationID] ASC);

CREATE INDEX [items_IX_OwnerLocation] ON [invItems] ([ownerID] ASC, [locationID] ASC);