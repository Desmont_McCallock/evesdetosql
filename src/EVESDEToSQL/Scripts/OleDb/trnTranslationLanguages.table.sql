﻿CREATE TABLE [trnTranslationLanguages](
	[numericLanguageID] int NOT NULL,
	[languageID] varchar(50) NULL,
	[languageName] varchar(200) NULL,
 CONSTRAINT [trnTranslationLanguages_PK] PRIMARY KEY ([numericLanguageID])
);