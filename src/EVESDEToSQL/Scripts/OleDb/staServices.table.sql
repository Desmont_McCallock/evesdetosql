﻿CREATE TABLE [staServices](
	[serviceID] int NOT NULL,
	[serviceName] varchar(100) NULL,
	[description] memo NULL,
 CONSTRAINT [staServices_PK] PRIMARY KEY ([serviceID])
);