﻿CREATE TABLE [invMarketGroups](
	[marketGroupID] int NOT NULL,
	[parentGroupID] int NULL,
	[marketGroupName] varchar(100) NULL,
	[description] memo NULL,
	[iconID] int NULL,
	[hasTypes] bit NULL,
 CONSTRAINT [invMarketGroups_PK] PRIMARY KEY ([marketGroupID])
);