﻿CREATE TABLE [agtResearchAgents](
	[agentID] int NOT NULL,
	[typeID] int NOT NULL,
 CONSTRAINT [agtResearchAgents_PK] PRIMARY KEY ([agentID], [typeID])
);

CREATE INDEX [agtResearchAgents_IX_type] ON [agtResearchAgents] ([typeID] ASC);