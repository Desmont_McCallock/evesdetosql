﻿CREATE TABLE [invControlTowerResourcePurposes](
	[purpose] tinyint NOT NULL,
	[purposeText] varchar(100) NULL,
 CONSTRAINT [invControlTowerResourcePurposes_PK] PRIMARY KEY ([purpose])
);