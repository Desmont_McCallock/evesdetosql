﻿CREATE TABLE [ramAssemblyLineStations](
	[stationID] int NOT NULL,
	[assemblyLineTypeID] tinyint NOT NULL,
	[quantity] tinyint NULL,
	[stationTypeID] int NULL,
	[ownerID] int NULL,
	[solarSystemID] int NULL,
	[regionID] int NULL,
 CONSTRAINT [ramAssemblyLineStations_PK] PRIMARY KEY ([stationID], [assemblyLineTypeID])
);

CREATE INDEX [ramAssemblyLineStations_IX_owner] ON [ramAssemblyLineStations] ([ownerID] ASC);

CREATE INDEX [ramAssemblyLineStations_IX_region] ON [ramAssemblyLineStations] ([regionID] ASC);