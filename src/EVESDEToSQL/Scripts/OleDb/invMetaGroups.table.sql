﻿CREATE TABLE [invMetaGroups](
	[metaGroupID] smallint NOT NULL,
	[metaGroupName] varchar(100) NULL,
	[description] memo NULL,
	[iconID] int NULL,
 CONSTRAINT [invMetaGroups_PK] PRIMARY KEY ([metaGroupID])
);