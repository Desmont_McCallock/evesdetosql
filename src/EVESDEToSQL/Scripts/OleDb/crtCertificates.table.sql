CREATE TABLE [crtCertificates](
	[certificateID] int NOT NULL,
	[groupID] smallint NULL,
	[classID] int NULL,
	[grade] tinyint NULL,
	[corpID] int NULL,
	[iconID] int NULL,
	[description] memo NULL,
 CONSTRAINT [crtCertificates_PK] PRIMARY KEY ([certificateID])
);

CREATE INDEX [crtCertificates_IX_group] ON [crtCertificates] ([groupID] ASC);

CREATE INDEX [crtCertificates_IX_class] ON [crtCertificates] ([classID] ASC);