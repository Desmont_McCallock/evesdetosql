﻿CREATE TABLE [crpNPCDivisions](
	[divisionID] tinyint NOT NULL,
	[divisionName] varchar(100) NULL,
	[description] memo NULL,
	[leaderType] varchar(100) NULL,
 CONSTRAINT [crpNPCDivisions_PK] PRIMARY KEY ([divisionID])
);