﻿CREATE TABLE [mapSolarSystemJumps](
	[fromRegionID] int NULL,
	[fromConstellationID] int NULL,
	[fromSolarSystemID] int NOT NULL,
	[toSolarSystemID] int NOT NULL,
	[toConstellationID] int NULL,
	[toRegionID] int NULL,
 CONSTRAINT [mapSolarSystemJumps_PK] PRIMARY KEY ([fromSolarSystemID], [toSolarSystemID])
);

CREATE INDEX [mapSolarSystemJumps_IX_fromConstellation] ON [mapSolarSystemJumps] ([fromConstellationID] ASC);

CREATE INDEX [mapSolarSystemJumps_IX_fromRegion] ON [mapSolarSystemJumps] ([fromRegionID] ASC);