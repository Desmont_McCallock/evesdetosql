﻿--
-- Table structure for table `mapJumps`
--

DROP TABLE IF EXISTS `mapJumps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapJumps` (
	`stargateID` INT(10) unsigned NOT NULL,
	`celestialID` INT(10) unsigned NULL,
	PRIMARY KEY (`stargateID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapJumps`
--

LOCK TABLES `mapJumps` WRITE;
/*!40000 ALTER TABLE `mapJumps` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapJumps` ENABLE KEYS */;
UNLOCK TABLES;