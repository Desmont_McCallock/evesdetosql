﻿--
-- Table structure for table `crpNPCCorporationDivisions`
--

DROP TABLE IF EXISTS `crpNPCCorporationDivisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crpNPCCorporationDivisions` (
	`corporationID` INT(10) unsigned NOT NULL,
	`divisionID` TINYINT(3) unsigned NOT NULL,
	`size` TINYINT(3) NULL,
	PRIMARY KEY (`corporationID` ASC, `divisionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crpNPCCorporationDivisions`
--

LOCK TABLES `crpNPCCorporationDivisions` WRITE;
/*!40000 ALTER TABLE `crpNPCCorporationDivisions` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crpNPCCorporationDivisions` ENABLE KEYS */;
UNLOCK TABLES;