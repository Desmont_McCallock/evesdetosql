﻿--
-- Table structure for table `warCombatZoneSystems`
--

DROP TABLE IF EXISTS `warCombatZoneSystems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warCombatZoneSystems` (
	`solarSystemID` INT(10) unsigned NOT NULL,
	`combatZoneID` INT(10) unsigned NULL,
	PRIMARY KEY (`solarSystemID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warCombatZoneSystems`
--

LOCK TABLES `warCombatZoneSystems` WRITE;
/*!40000 ALTER TABLE `warCombatZoneSystems` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `warCombatZoneSystems` ENABLE KEYS */;
UNLOCK TABLES;