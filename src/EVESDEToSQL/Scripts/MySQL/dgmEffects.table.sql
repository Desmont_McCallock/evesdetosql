﻿--
-- Table structure for table `dgmEffects`
--

DROP TABLE IF EXISTS `dgmEffects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmEffects` (
	`effectID` SMALLINT(6) unsigned NOT NULL,
	`effectName` VARCHAR(400) NULL,
	`effectCategory` SMALLINT(6) NULL,
	`preExpression` INT(10) NULL,
	`postExpression` INT(10) NULL,
	`description` VARCHAR(1000) NULL,
	`guid` VARCHAR(60) NULL,
	`iconID` INT(10) unsigned NULL,
	`isOffensive` TINYINT(1) NULL,
	`isAssistance` TINYINT(1) NULL,
	`durationAttributeID` SMALLINT(6) unsigned NULL,
	`trackingSpeedAttributeID` SMALLINT(6) unsigned NULL,
	`dischargeAttributeID` SMALLINT(6) unsigned NULL,
	`rangeAttributeID` SMALLINT(6) unsigned NULL,
	`falloffAttributeID` SMALLINT(6) unsigned NULL,
	`disallowAutoRepeat` TINYINT(1) NULL,
	`published` TINYINT(1) NULL,
	`displayName` VARCHAR(100) NULL,
	`isWarpSafe` TINYINT(1) NULL,
	`rangeChance` TINYINT(1) NULL,
	`electronicChance` TINYINT(1) NULL,
	`propulsionChance` TINYINT(1) NULL,
	`distribution` TINYINT(3) unsigned NULL,
	`sfxName` VARCHAR(20) NULL,
	`npcUsageChanceAttributeID` SMALLINT(6) unsigned NULL,
	`npcActivationChanceAttributeID` SMALLINT(6) unsigned NULL,
	`fittingUsageChanceAttributeID` SMALLINT(6) unsigned NULL,
	`modifierInfo` VARCHAR(8000) NULL,
	PRIMARY KEY (`effectID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmEffects`
--

LOCK TABLES `dgmEffects` WRITE;
/*!40000 ALTER TABLE `dgmEffects` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmEffects` ENABLE KEYS */;
UNLOCK TABLES;