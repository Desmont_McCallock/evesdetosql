﻿--
-- Table structure for table `agtResearchAgents`
--

DROP TABLE IF EXISTS `agtResearchAgents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agtResearchAgents` (
	`agentID` INT(10) unsigned NOT NULL,
	`typeID` INT(10) unsigned NOT NULL,
	PRIMARY KEY (`agentID` ASC, `typeID` ASC),
	KEY agtResearchAgents_IX_type (`typeID`)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agtResearchAgents`
--

LOCK TABLES `agtResearchAgents` WRITE;
/*!40000 ALTER TABLE `agtResearchAgents` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `agtResearchAgents` ENABLE KEYS */;
UNLOCK TABLES;