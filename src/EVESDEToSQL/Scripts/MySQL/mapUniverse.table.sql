﻿--
-- Table structure for table `mapUniverse`
--

DROP TABLE IF EXISTS `mapUniverse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapUniverse` (
	`universeID` INT(10) unsigned NOT NULL,
	`universeName` VARCHAR(100) NULL,
	`x` DOUBLE NULL,
	`y` DOUBLE NULL,
	`z` DOUBLE NULL,
	`xMin` DOUBLE NULL,
	`xMax` DOUBLE NULL,
	`yMin` DOUBLE NULL,
	`yMax` DOUBLE NULL,
	`zMin` DOUBLE NULL,
	`zMax` DOUBLE NULL,
	`radius` DOUBLE NULL,
	PRIMARY KEY (`universeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapUniverse`
--

LOCK TABLES `mapUniverse` WRITE;
/*!40000 ALTER TABLE `mapUniverse` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapUniverse` ENABLE KEYS */;
UNLOCK TABLES;