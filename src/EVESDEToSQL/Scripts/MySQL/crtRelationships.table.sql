--
-- Table structure for table `crtRelationships`
--

DROP TABLE IF EXISTS `crtRelationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crtRelationships` (
	`relationshipID` INT(10) unsigned NOT NULL,
	`parentID` INT(10) unsigned NULL,
	`parentTypeID` INT(10) unsigned NULL,
	`parentLevel` TINYINT(3) NULL,
	`childID` INT(10) unsigned NULL,
	`grade` TINYINT(3) NULL,
	PRIMARY KEY (`relationshipID` ASC),
	KEY `crtRelationships_IX_child` (`childID` ASC),
	KEY `crtRelationships_IX_parent` (`parentID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crtRelationships`
--

LOCK TABLES `crtRelationships` WRITE;
/*!40000 ALTER TABLE `crtRelationships` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crtRelationships` ENABLE KEYS */;
UNLOCK TABLES;