﻿--
-- Table structure for table `planetSchematicsPinMap`
--

DROP TABLE IF EXISTS `planetSchematicsPinMap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planetSchematicsPinMap` (
	`schematicID` SMALLINT(6) unsigned NOT NULL,
	`pinTypeID` INT(10) unsigned NOT NULL,
	PRIMARY KEY (`schematicID` ASC,	`pinTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planetSchematicsPinMap`
--

LOCK TABLES `planetSchematicsPinMap` WRITE;
/*!40000 ALTER TABLE `planetSchematicsPinMap` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `planetSchematicsPinMap` ENABLE KEYS */;
UNLOCK TABLES;