﻿--
-- Table structure for table `mapConstellationJumps`
--

DROP TABLE IF EXISTS `mapConstellationJumps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapConstellationJumps` (
	`fromRegionID` INT(10) unsigned NULL,
	`fromConstellationID` INT(10) unsigned NOT NULL,
	`toConstellationID` INT(10) unsigned NOT NULL,
	`toRegionID` INT(10) unsigned NULL,
	PRIMARY KEY (`fromConstellationID` ASC,	`toConstellationID` ASC),
	KEY `mapConstellationJumps_IX_fromRegion` (`fromRegionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapConstellationJumps`
--

LOCK TABLES `mapConstellationJumps` WRITE;
/*!40000 ALTER TABLE `mapConstellationJumps` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapConstellationJumps` ENABLE KEYS */;
UNLOCK TABLES;