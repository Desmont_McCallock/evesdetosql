﻿--
-- Table structure for table `mapSolarSystemJumps`
--

DROP TABLE IF EXISTS `mapSolarSystemJumps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapSolarSystemJumps` (
	`fromRegionID` INT(10) unsigned NULL,
	`fromConstellationID` INT(10) unsigned NULL,
	`fromSolarSystemID` INT(10) unsigned NOT NULL,
	`toSolarSystemID` INT(10) unsigned NOT NULL,
	`toConstellationID` INT(10) unsigned NULL,
	`toRegionID` INT(10) unsigned NULL,
	PRIMARY KEY (`fromSolarSystemID` ASC, `toSolarSystemID` ASC),
	KEY `mapSolarSystemJumps_IX_fromConstellation` (`fromConstellationID` ASC),
	KEY `mapSolarSystemJumps_IX_fromRegion` (`fromRegionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapSolarSystemJumps`
--

LOCK TABLES `mapSolarSystemJumps` WRITE;
/*!40000 ALTER TABLE `mapSolarSystemJumps` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapSolarSystemJumps` ENABLE KEYS */;
UNLOCK TABLES;