﻿--
-- Table structure for table `invNames`
--

DROP TABLE IF EXISTS `invNames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invNames` (
	`itemID` BIGINT(20) unsigned NOT NULL,
	`itemName` NVARCHAR(200) NOT NULL,
	PRIMARY KEY (`itemID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invNames`
--

LOCK TABLES `invNames` WRITE;
/*!40000 ALTER TABLE `invNames` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invNames` ENABLE KEYS */;
UNLOCK TABLES;