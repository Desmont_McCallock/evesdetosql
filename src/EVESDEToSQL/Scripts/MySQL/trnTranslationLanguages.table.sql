﻿--
-- Table structure for table `trnTranslationLanguages`
--

DROP TABLE IF EXISTS `trnTranslationLanguages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trnTranslationLanguages` (
	`numericLanguageID` INT(10) unsigned NOT NULL,
	`languageID` VARCHAR(50) NULL,
	`languageName` NVARCHAR(200) NULL,
	PRIMARY KEY (`numericLanguageID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trnTranslationLanguages`
--

LOCK TABLES `trnTranslationLanguages` WRITE;
/*!40000 ALTER TABLE `trnTranslationLanguages` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `trnTranslationLanguages` ENABLE KEYS */;
UNLOCK TABLES;