﻿--
-- Table structure for table `mapLocationScenes`
--

DROP TABLE IF EXISTS `mapLocationScenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapLocationScenes` (
	`locationID` INT(10) unsigned NOT NULL,
	`graphicID` INT(10) unsigned NULL,
	PRIMARY KEY (`locationID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapLocationScenes`
--

LOCK TABLES `mapLocationScenes` WRITE;
/*!40000 ALTER TABLE `mapLocationScenes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapLocationScenes` ENABLE KEYS */;
UNLOCK TABLES;