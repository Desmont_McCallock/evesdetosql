﻿--
-- Table structure for table `invControlTowerResources`
--

DROP TABLE IF EXISTS `invControlTowerResources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invControlTowerResources` (
	`controlTowerTypeID` INT(10) unsigned NOT NULL,
	`resourceTypeID` INT(10) unsigned NOT NULL,
	`purpose` TINYINT(3) unsigned NULL,
	`quantity` INT(10) NULL,
	`minSecurityLevel` DOUBLE NULL,
	`factionID` INT(10) unsigned NULL,
	PRIMARY KEY (`controlTowerTypeID` ASC, `resourceTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invControlTowerResources`
--

LOCK TABLES `invControlTowerResources` WRITE;
/*!40000 ALTER TABLE `invControlTowerResources` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invControlTowerResources` ENABLE KEYS */;
UNLOCK TABLES;