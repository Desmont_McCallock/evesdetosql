﻿--
-- Table structure for table `invControlTowerResourcePurposes`
--

DROP TABLE IF EXISTS `invControlTowerResourcePurposes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invControlTowerResourcePurposes` (
	`purpose` TINYINT(3) unsigned NOT NULL,
	`purposeText` VARCHAR(100) NULL,
	PRIMARY KEY (`purpose` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invControlTowerResourcePurposes`
--

LOCK TABLES `invControlTowerResourcePurposes` WRITE;
/*!40000 ALTER TABLE `invControlTowerResourcePurposes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invControlTowerResourcePurposes` ENABLE KEYS */;
UNLOCK TABLES;