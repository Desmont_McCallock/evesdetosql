﻿--
-- Table structure for table `dgmTypeAttributes`
--

DROP TABLE IF EXISTS `dgmTypeAttributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmTypeAttributes` (
	`typeID` INT(10) unsigned NOT NULL,
	`attributeID` SMALLINT(6) unsigned NOT NULL,
	`valueInt` INT(10) NULL,
	`valueFloat` DOUBLE NULL,
	PRIMARY KEY (`typeID` ASC, `attributeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmTypeAttributes`
--

LOCK TABLES `dgmTypeAttributes` WRITE;
/*!40000 ALTER TABLE `dgmTypeAttributes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmTypeAttributes` ENABLE KEYS */;
UNLOCK TABLES;