﻿--
-- Table structure for table `crpNPCCorporationTrades`
--

DROP TABLE IF EXISTS `crpNPCCorporationTrades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crpNPCCorporationTrades` (
	`corporationID` INT(10) unsigned NOT NULL,
	`typeID` INT(10) unsigned NOT NULL,
	PRIMARY KEY (`corporationID` ASC, `typeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crpNPCCorporationTrades`
--

LOCK TABLES `crpNPCCorporationTrades` WRITE;
/*!40000 ALTER TABLE `crpNPCCorporationTrades` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crpNPCCorporationTrades` ENABLE KEYS */;
UNLOCK TABLES;