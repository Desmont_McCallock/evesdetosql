﻿--
-- Table structure for table `staOperations`
--

DROP TABLE IF EXISTS `staOperations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staOperations` (
	`activityID` TINYINT(3) unsigned NULL,
	`operationID` TINYINT(3) unsigned NOT NULL,
	`operationName` NVARCHAR(100) NULL,
	`description` NVARCHAR(1000) NULL,
	`fringe` TINYINT(3) NULL,
	`corridor` TINYINT(3) NULL,
	`hub` TINYINT(3) NULL,
	`border` TINYINT(3) NULL,
	`ratio` TINYINT(3) NULL,
	`caldariStationTypeID` INT(10) unsigned NULL,
	`minmatarStationTypeID` INT(10) unsigned NULL,
	`amarrStationTypeID` INT(10) unsigned NULL,
	`gallenteStationTypeID` INT(10) unsigned NULL,
	`joveStationTypeID` INT(10) unsigned NULL,
	PRIMARY KEY (`operationID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staOperations`
--

LOCK TABLES `staOperations` WRITE;
/*!40000 ALTER TABLE `staOperations` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `staOperations` ENABLE KEYS */;
UNLOCK TABLES;