﻿--
-- Table structure for table `dgmTypeTraits`
--

DROP TABLE IF EXISTS `dgmTypeTraits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmTypeTraits` (
	`typeID` INT(10) unsigned NOT NULL,
	`parentTypeID` INT(10) NOT NULL,
	`traitID` INT(10) unsigned NOT NULL,
	`bonus` DOUBLE NULL,
	PRIMARY KEY (`typeID` ASC, `parentTypeID` ASC, `traitID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmTypeTraits`
--

LOCK TABLES `dgmTypeTraits` WRITE;
/*!40000 ALTER TABLE `dgmTypeTraits` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmTypeTraits` ENABLE KEYS */;
UNLOCK TABLES;