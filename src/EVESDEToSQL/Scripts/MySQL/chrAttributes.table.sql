﻿--
-- Table structure for table `chrAttributes`
--

DROP TABLE IF EXISTS `chrAttributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chrAttributes` (
	`attributeID` TINYINT(3) unsigned NOT NULL,
	`attributeName` VARCHAR(100) NULL,
	`description` VARCHAR(1000) NULL,
	`iconID` INT(10) unsigned NULL,
	`shortDescription` NVARCHAR(500) NULL,
	`notes` NVARCHAR(500) NULL,
	PRIMARY KEY (`attributeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chrAttributes`
--

LOCK TABLES `chrAttributes` WRITE;
/*!40000 ALTER TABLE `chrAttributes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `chrAttributes` ENABLE KEYS */;
UNLOCK TABLES;