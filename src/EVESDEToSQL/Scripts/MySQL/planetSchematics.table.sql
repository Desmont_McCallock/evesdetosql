﻿--
-- Table structure for table `planetSchematics`
--

DROP TABLE IF EXISTS `planetSchematics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planetSchematics` (
	`schematicID` SMALLINT(6) unsigned NOT NULL,
	`schematicName` NVARCHAR(255) NULL,
	`cycleTime` INT(10) NULL,
	PRIMARY KEY (`schematicID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planetSchematics`
--

LOCK TABLES `planetSchematics` WRITE;
/*!40000 ALTER TABLE `planetSchematics` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `planetSchematics` ENABLE KEYS */;
UNLOCK TABLES;