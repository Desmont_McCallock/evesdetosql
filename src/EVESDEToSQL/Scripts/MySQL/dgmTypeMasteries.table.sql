﻿--
-- Table structure for table `dgmTypeMasteries`
--

DROP TABLE IF EXISTS `dgmTypeMasteries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmTypeMasteries` (
	`typeID` INT(10) unsigned NOT NULL,
	`masteryID` SMALLINT(6) unsigned NOT NULL,
	PRIMARY KEY (`typeID` ASC, `masteryID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmTypeMasteries`
--

LOCK TABLES `dgmTypeMasteries` WRITE;
/*!40000 ALTER TABLE `dgmTypeMasteries` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmTypeMasteries` ENABLE KEYS */;
UNLOCK TABLES;