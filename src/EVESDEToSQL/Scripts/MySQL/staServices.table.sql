﻿--
-- Table structure for table `staServices`
--

DROP TABLE IF EXISTS `staServices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staServices` (
	`serviceID` INT(10) unsigned NOT NULL,
	`serviceName` NVARCHAR(100) NULL,
	`description` NVARCHAR(1000) NULL,
	PRIMARY KEY (`serviceID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staServices`
--

LOCK TABLES `staServices` WRITE;
/*!40000 ALTER TABLE `staServices` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `staServices` ENABLE KEYS */;
UNLOCK TABLES;