﻿--
-- Table structure for table `ramActivities`
--

DROP TABLE IF EXISTS `ramActivities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramActivities` (
	`activityID` TINYINT(3) unsigned NOT NULL,
	`activityName` NVARCHAR(100) NULL,
	`iconNo` VARCHAR(5) NULL,
	`description` NVARCHAR(1000) NULL,
	`published` TINYINT(1) NULL,
	PRIMARY KEY (`activityID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ramActivities`
--

LOCK TABLES `ramActivities` WRITE;
/*!40000 ALTER TABLE `ramActivities` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `ramActivities` ENABLE KEYS */;
UNLOCK TABLES;