﻿--
-- Table structure for table `invCategories`
--

DROP TABLE IF EXISTS `invCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invCategories` (
	`categoryID` INT(10) unsigned NOT NULL,
	`categoryName` NVARCHAR(100) NULL,
	`description` NVARCHAR(3000) NULL,
	`iconID` INT(10) unsigned NULL,
	`published` TINYINT(1) NULL,
	PRIMARY KEY (`categoryID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invCategories`
--

LOCK TABLES `invCategories` WRITE;
/*!40000 ALTER TABLE `invCategories` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invCategories` ENABLE KEYS */;
UNLOCK TABLES;