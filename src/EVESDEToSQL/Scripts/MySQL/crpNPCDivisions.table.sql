﻿--
-- Table structure for table `crpNPCDivisions`
--

DROP TABLE IF EXISTS `crpNPCDivisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crpNPCDivisions` (
	`divisionID` TINYINT(3) unsigned NOT NULL,
	`divisionName` NVARCHAR(100) NULL,
	`description` NVARCHAR(1000) NULL,
	`leaderType` NVARCHAR(100) NULL,
	PRIMARY KEY (`divisionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crpNPCDivisions`
--

LOCK TABLES `crpNPCDivisions` WRITE;
/*!40000 ALTER TABLE `crpNPCDivisions` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crpNPCDivisions` ENABLE KEYS */;
UNLOCK TABLES;