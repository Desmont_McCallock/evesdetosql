﻿--
-- Table structure for table `invBlueprintTypes`
--

DROP TABLE IF EXISTS `invBlueprintTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invBlueprintTypes` (
	`blueprintTypeID` INT(10) unsigned NOT NULL,
	`parentBlueprintTypeID` INT(10) unsigned NULL,
	`productTypeID` INT(10) unsigned NULL,
	`productionTime` INT(10) NULL,
	`techLevel` SMALLINT(6) NULL,
	`researchProductivityTime` INT(10) NULL,
	`researchMaterialTime` INT(10) NULL,
	`researchCopyTime` INT(10) NULL,
	`researchTechTime` INT(10) NULL,
	`duplicatingTime` INT(10) NULL,
	`reverseEngineeringTime` INT(10) NULL,
	`inventionTime` INT(10) NULL,
	`productivityModifier` INT(10) NULL,
	`materialModifier` SMALLINT(6) NULL,
	`wasteFactor` SMALLINT(6) NULL,
	`maxProductionLimit` INT(10) NULL,
	PRIMARY KEY (`blueprintTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invBlueprintTypes`
--

LOCK TABLES `invBlueprintTypes` WRITE;
/*!40000 ALTER TABLE `invBlueprintTypes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invBlueprintTypes` ENABLE KEYS */;
UNLOCK TABLES;