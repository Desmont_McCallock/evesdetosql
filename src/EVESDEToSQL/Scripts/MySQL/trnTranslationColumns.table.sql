﻿--
-- Table structure for table `trnTranslationColumns`
--

DROP TABLE IF EXISTS `trnTranslationColumns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trnTranslationColumns` (
	`tcGroupID` SMALLINT(6) unsigned NULL,
	`tcID` SMALLINT(6) unsigned NOT NULL,
	`tableName` NVARCHAR(256) NOT NULL,
	`columnName` NVARCHAR(128) NOT NULL,
	`masterID` NVARCHAR(128) NULL,
	PRIMARY KEY (`tcID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trnTranslationColumns`
--

LOCK TABLES `trnTranslationColumns` WRITE;
/*!40000 ALTER TABLE `trnTranslationColumns` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `trnTranslationColumns` ENABLE KEYS */;
UNLOCK TABLES;