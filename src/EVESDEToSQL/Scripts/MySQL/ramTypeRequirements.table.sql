﻿--
-- Table structure for table `ramTypeRequirements`
--

DROP TABLE IF EXISTS `ramTypeRequirements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramTypeRequirements` (
	`typeID` INT(10) unsigned NOT NULL,
	`activityID` TINYINT(3) unsigned NOT NULL,
	`requiredTypeID` INT(10) unsigned NOT NULL,
	`quantity` INT(10) NULL,
	`level` INT(10) NULL,
	`damagePerJob` DOUBLE NULL,
	`recycle` TINYINT(1) NULL,
	`raceID` INT(10) unsigned NULL,
	`probability` DOUBLE NULL,
	`consume` TINYINT(1) NULL,
	PRIMARY KEY (`typeID` ASC, `activityID` ASC, `requiredTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ramTypeRequirements`
--

LOCK TABLES `ramTypeRequirements` WRITE;
/*!40000 ALTER TABLE `ramTypeRequirements` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `ramTypeRequirements` ENABLE KEYS */;
UNLOCK TABLES;