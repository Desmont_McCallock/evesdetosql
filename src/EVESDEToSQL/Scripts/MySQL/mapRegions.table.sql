﻿--
-- Table structure for table `mapRegions`
--

DROP TABLE IF EXISTS `mapRegions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapRegions` (
	`regionID` INT(10) unsigned NOT NULL,
	`regionName` NVARCHAR(100) NULL,
	`x` DOUBLE NULL,
	`y` DOUBLE NULL,
	`z` DOUBLE NULL,
	`xMin` DOUBLE NULL,
	`xMax` DOUBLE NULL,
	`yMin` DOUBLE NULL,
	`yMax` DOUBLE NULL,
	`zMin` DOUBLE NULL,
	`zMax` DOUBLE NULL,
	`factionID` INT(10) unsigned NULL,
	`radius` DOUBLE NULL,
	PRIMARY KEY (`regionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapRegions`
--

LOCK TABLES `mapRegions` WRITE;
/*!40000 ALTER TABLE `mapRegions` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapRegions` ENABLE KEYS */;
UNLOCK TABLES;