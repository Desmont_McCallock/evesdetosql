﻿--
-- Table structure for table `sknMaterials`
--

DROP TABLE IF EXISTS `sknMaterials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sknMaterials` (
	`skinMaterialID` INT(10) unsigned NOT NULL,
	`materialSetID` INT(10) unsigned NOT NULL DEFAULT 0,
	`displayNameID` INT(10) unsigned NOT NULL DEFAULT 0,
	-- Obsolete columns since Galatea 1.0
	`material` NVARCHAR(255) NOT NULL DEFAULT '',
	`colorHull` NVARCHAR(6) NULL,
	`colorWindow` NVARCHAR(6) NULL,
	`colorPrimary` NVARCHAR(6) NULL,
	`colorSecondary` NVARCHAR(6) NULL,
	PRIMARY KEY (`skinMaterialID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sknMaterials`
--

LOCK TABLES `sknMaterials` WRITE;
/*!40000 ALTER TABLE `sknMaterials` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `sknMaterials` ENABLE KEYS */;
UNLOCK TABLES;