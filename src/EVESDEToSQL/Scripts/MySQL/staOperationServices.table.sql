﻿--
-- Table structure for table `staOperationServices`
--

DROP TABLE IF EXISTS `staOperationServices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staOperationServices` (
	`operationID` TINYINT(3) unsigned NOT NULL,
	`serviceID` INT(10) unsigned NOT NULL,
	PRIMARY KEY (`operationID` ASC,	`serviceID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staOperationServices`
--

LOCK TABLES `staOperationServices` WRITE;
/*!40000 ALTER TABLE `staOperationServices` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `staOperationServices` ENABLE KEYS */;
UNLOCK TABLES;