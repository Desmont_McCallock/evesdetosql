﻿--
-- Table structure for table `mapLandmarks`
--

DROP TABLE IF EXISTS `mapLandmarks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapLandmarks` (
	`landmarkID` SMALLINT(6) unsigned NOT NULL,
	`landmarkName` VARCHAR(100) NULL,
	`description` VARCHAR(7000) NULL,
	`locationID` INT(10) unsigned NULL,
	`x` DOUBLE  NULL,
	`y` DOUBLE  NULL,
	`z` DOUBLE  NULL,
	`radius` DOUBLE NULL,
	`iconID` INT(10) unsigned NULL,
	`importance` TINYINT(3) NULL,
	PRIMARY KEY (`landmarkID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapLandmarks`
--

LOCK TABLES `mapLandmarks` WRITE;
/*!40000 ALTER TABLE `mapLandmarks` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapLandmarks` ENABLE KEYS */;
UNLOCK TABLES;