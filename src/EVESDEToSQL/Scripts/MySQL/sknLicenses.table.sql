﻿--
-- Table structure for table `sknLicenses`
--

DROP TABLE IF EXISTS `sknLicenses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sknLicenses` (
	`licenseTypeID` INT(10) unsigned NOT NULL,
	`skinID` INT(10) unsigned NOT NULL,
	`duration` INT(10) NOT NULL  DEFAULT -1,
	PRIMARY KEY (`licenseTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sknLicenses`
--

LOCK TABLES `sknLicenses` WRITE;
/*!40000 ALTER TABLE `sknLicenses` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `sknLicenses` ENABLE KEYS */;
UNLOCK TABLES;