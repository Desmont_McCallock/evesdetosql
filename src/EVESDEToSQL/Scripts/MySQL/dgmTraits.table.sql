﻿--
-- Table structure for table `dgmTraits`
--

DROP TABLE IF EXISTS `dgmTraits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dgmTraits` (
	`traitID` INT(10) unsigned NOT NULL,
	`bonusText` NVARCHAR(500) NOT NULL,
	`unitID` TINYINT(3) unsigned NULL,
	PRIMARY KEY (`traitID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dgmTraits`
--

LOCK TABLES `dgmTraits` WRITE;
/*!40000 ALTER TABLE `dgmTraits` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `dgmTraits` ENABLE KEYS */;
UNLOCK TABLES;