﻿--
-- Table structure for table `ramInstallationTypeContents`
--

DROP TABLE IF EXISTS `ramInstallationTypeContents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramInstallationTypeContents` (
	`installationTypeID` INT(10) unsigned NOT NULL,
	`assemblyLineTypeID` TINYINT(3) unsigned NOT NULL,
	`quantity` TINYINT(3) NULL,
	PRIMARY KEY (`installationTypeID` ASC, `assemblyLineTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ramInstallationTypeContents`
--

LOCK TABLES `ramInstallationTypeContents` WRITE;
/*!40000 ALTER TABLE `ramInstallationTypeContents` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `ramInstallationTypeContents` ENABLE KEYS */;
UNLOCK TABLES;