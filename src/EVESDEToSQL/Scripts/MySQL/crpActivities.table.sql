﻿--
-- Table structure for table `crpActivities`
--

DROP TABLE IF EXISTS `crpActivities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crpActivities` (
	`activityID` TINYINT(3) unsigned NOT NULL,
	`activityName` NVARCHAR(100) NULL,
	`description` NVARCHAR(1000) NULL,
	PRIMARY KEY (`activityID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crpActivities`
--

LOCK TABLES `crpActivities` WRITE;
/*!40000 ALTER TABLE `crpActivities` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crpActivities` ENABLE KEYS */;
UNLOCK TABLES;