﻿--
-- Table structure for table `invMarketGroups`
--

DROP TABLE IF EXISTS `invMarketGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invMarketGroups` (
	`marketGroupID` INT(10) unsigned NOT NULL,
	`parentGroupID` INT(10) unsigned NULL,
	`marketGroupName` NVARCHAR(100) NULL,
	`description` NVARCHAR(3000) NULL,
	`iconID` INT(10) unsigned NULL,
	`hasTypes` TINYINT(1) NULL,
	PRIMARY KEY (`marketGroupID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invMarketGroups`
--

LOCK TABLES `invMarketGroups` WRITE;
/*!40000 ALTER TABLE `invMarketGroups` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invMarketGroups` ENABLE KEYS */;
UNLOCK TABLES;