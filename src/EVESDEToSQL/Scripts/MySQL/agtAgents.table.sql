﻿--
-- Table structure for table `agtAgents`
--

DROP TABLE IF EXISTS `agtAgents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agtAgents` (
  `agentID` INT(10) unsigned NOT NULL,
  `divisionID` TINYINT(3) unsigned NULL,
  `corporationID` INT(10) unsigned NULL,
  `locationID` INT(10) unsigned NULL,
  `level` TINYINT(3) NULL,
  `quality` SMALLINT(6) NULL,
  `agentTypeID` INT(10) unsigned NULL,
  `isLocator` TINYINT(1) NULL,
  PRIMARY KEY (`agentID` ASC),
  KEY `agtAgents_IX_corporation` (`corporationID` ASC),
  KEY `agtAgents_IX_station` (`locationID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agtAgentTypes`
--

LOCK TABLES `agtAgents` WRITE;
/*!40000 ALTER TABLE `agtAgents` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `agtAgents` ENABLE KEYS */;
UNLOCK TABLES;