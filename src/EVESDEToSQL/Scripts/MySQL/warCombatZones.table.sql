﻿--
-- Table structure for table `warCombatZones`
--

DROP TABLE IF EXISTS `warCombatZones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `warCombatZones` (
	`combatZoneID` INT(10) NOT NULL DEFAULT -1,
	`combatZoneName` NVARCHAR(100) NULL,
	`factionID` INT(10) unsigned NULL,
	`centerSystemID` INT(10) unsigned NULL,
	`description` NVARCHAR(500) NULL,
	PRIMARY KEY (`combatZoneID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `warCombatZones`
--

LOCK TABLES `warCombatZones` WRITE;
/*!40000 ALTER TABLE `warCombatZones` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `warCombatZones` ENABLE KEYS */;
UNLOCK TABLES;