﻿--
-- Table structure for table `eveGraphics`
--

DROP TABLE IF EXISTS `eveGraphics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eveGraphics` (
	`graphicID` INT(10) unsigned NOT NULL,
	`graphicFile` VARCHAR(500) NOT NULL DEFAULT '',
	`description` NVARCHAR(8000) NOT NULL DEFAULT '',
	`obsolete` TINYINT(1) NOT NULL DEFAULT 0,
	`graphicType` VARCHAR(100) NULL,
	`collidable` TINYINT(1) NULL,
	`directoryID` INT(10) unsigned NULL,
	`graphicName` NVARCHAR(64) NOT NULL DEFAULT '',
	`gfxRaceID` VARCHAR(255) NULL,
	`colorScheme` VARCHAR(255) NULL,
	`sofHullName` VARCHAR(64) NULL,
	PRIMARY KEY (`graphicID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eveGraphics`
--

LOCK TABLES `eveGraphics` WRITE;
/*!40000 ALTER TABLE `eveGraphics` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `eveGraphics` ENABLE KEYS */;
UNLOCK TABLES;