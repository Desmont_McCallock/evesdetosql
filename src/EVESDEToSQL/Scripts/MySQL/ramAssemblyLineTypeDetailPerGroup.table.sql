﻿--
-- Table structure for table `ramAssemblyLineTypeDetailPerGroup`
--

DROP TABLE IF EXISTS `ramAssemblyLineTypeDetailPerGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramAssemblyLineTypeDetailPerGroup` (
	`assemblyLineTypeID` TINYINT(3) unsigned NOT NULL,
	`groupID` INT(10) unsigned NOT NULL,
	`timeMultiplier` DOUBLE NULL,
	`materialMultiplier` DOUBLE NULL,
	`costMultiplier` DOUBLE NULL,
	PRIMARY KEY (`assemblyLineTypeID` ASC, `groupID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ramAssemblyLineTypeDetailPerGroup`
--

LOCK TABLES `ramAssemblyLineTypeDetailPerGroup` WRITE;
/*!40000 ALTER TABLE `ramAssemblyLineTypeDetailPerGroup` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `ramAssemblyLineTypeDetailPerGroup` ENABLE KEYS */;
UNLOCK TABLES;