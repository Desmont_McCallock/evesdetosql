﻿--
-- Table structure for table `invTypes`
--

DROP TABLE IF EXISTS `invTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invTypes` (
	`typeID` INT(10) unsigned NOT NULL,
	`groupID` INT(10) unsigned NULL,
	`typeName` NVARCHAR(100) NULL,
	`description` NVARCHAR(3000) NULL,
	`mass` DOUBLE NULL,
	`volume` DOUBLE NULL,
	`capacity` DOUBLE NULL,
	`portionSize` INT(10) NULL,
	`raceID` TINYINT(3) unsigned NULL,
	`basePrice` DECIMAL(15,2) NULL,
	`published` TINYINT(1) NULL,
	`marketGroupID` INT(10) unsigned NULL,
	`chanceOfDuplicating` DOUBLE NULL,
	`factionID` INT(10) unsigned NULL,
	`graphicID` INT(10) unsigned NULL,
	`iconID` INT(10) unsigned NULL,
	`radius` DOUBLE NULL,
	`soundID` INT(10) unsigned NULL,
	PRIMARY KEY (`typeID` ASC),
	KEY `invTypes_IX_Group` (`groupID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invTypes`
--

LOCK TABLES `invTypes` WRITE;
/*!40000 ALTER TABLE `invTypes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invTypes` ENABLE KEYS */;
UNLOCK TABLES;