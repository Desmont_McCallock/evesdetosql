﻿--
-- Table structure for table `chrBloodlines`
--

DROP TABLE IF EXISTS `chrBloodlines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chrBloodlines` (
	`bloodlineID` TINYINT(3) unsigned NOT NULL,
	`bloodlineName` NVARCHAR(100) NULL,
	`raceID` TINYINT(3) unsigned NULL,
	`description` NVARCHAR(1000) NULL,
	`maleDescription` NVARCHAR(1000) NULL,
	`femaleDescription` NVARCHAR(1000) NULL,
	`shipTypeID` INT(10) unsigned NULL,
	`corporationID` INT(10) unsigned NULL,
	`perception` TINYINT(3) NULL,
	`willpower` TINYINT(3)  NULL,
	`charisma` TINYINT(3) NULL,
	`memory` TINYINT(3) NULL,
	`intelligence` TINYINT(3) NULL,
	`iconID` INT(10) unsigned NULL,
	`shortDescription` NVARCHAR(500) NULL,
	`shortMaleDescription` NVARCHAR(500) NULL,
	`shortFemaleDescription` NVARCHAR(500) NULL,
	PRIMARY KEY (`bloodlineID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chrBloodlines`
--

LOCK TABLES `chrBloodlines` WRITE;
/*!40000 ALTER TABLE `chrBloodlines` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `chrBloodlines` ENABLE KEYS */;
UNLOCK TABLES;