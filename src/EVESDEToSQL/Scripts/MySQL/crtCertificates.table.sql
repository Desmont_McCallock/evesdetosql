--
-- Table structure for table `crtCertificates`
--

DROP TABLE IF EXISTS `crtCertificates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `crtCertificates` (
	`certificateID` INT(10) unsigned NOT NULL,
	`groupID` SMALLINT(6) unsigned NULL,
	`classID` INT(10) unsigned NULL,
	`grade` TINYINT(3) NULL,
	`corpID` INT(10) unsigned NULL,
	`iconID` INT(10) unsigned NULL,
	`description` NVARCHAR(500) NULL,
	PRIMARY KEY (`certificateID` ASC),
	KEY crtCertificates_IX_group (`groupID` ASC),
	KEY crtCertificates_IX_class (`classID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `crtCertificates`
--

LOCK TABLES `crtCertificates` WRITE;
/*!40000 ALTER TABLE `crtCertificates` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `crtCertificates` ENABLE KEYS */;
UNLOCK TABLES;