﻿--
-- Table structure for table `sknSkins`
--

DROP TABLE IF EXISTS `sknSkins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sknSkins` (
	`skinID` INT(10) unsigned NOT NULL,
	`internalName` NVARCHAR(100) NOT NULL DEFAULT '',
	`skinMaterialID` INT(10) unsigned NULL,
	`typeID` INT(10) unsigned NULL,
	`allowCCPDevs` TINYINT(1) NOT NULL DEFAULT 0,
	`visibleSerenity` TINYINT(1) NOT NULL DEFAULT 0,
	`visibleTranquility` TINYINT(1) NOT NULL DEFAULT 0,
	PRIMARY KEY (`skinID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sknSkins`
--

LOCK TABLES `sknSkins` WRITE;
/*!40000 ALTER TABLE `sknSkins` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `sknSkins` ENABLE KEYS */;
UNLOCK TABLES;