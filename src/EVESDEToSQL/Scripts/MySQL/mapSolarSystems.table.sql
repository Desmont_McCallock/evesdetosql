﻿--
-- Table structure for table `mapSolarSystems`
--

DROP TABLE IF EXISTS `mapSolarSystems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapSolarSystems` (
	`regionID` INT(10) unsigned NULL,
	`constellationID` INT(10) unsigned NULL,
	`solarSystemID` INT(10) unsigned NOT NULL,
	`solarSystemName` NVARCHAR(100) NULL,
	`x` DOUBLE NULL,
	`y` DOUBLE NULL,
	`z` DOUBLE NULL,
	`xMin` DOUBLE NULL,
	`xMax` DOUBLE NULL,
	`yMin` DOUBLE NULL,
	`yMax` DOUBLE NULL,
	`zMin` DOUBLE NULL,
	`zMax` DOUBLE NULL,
	`luminosity` DOUBLE NULL,
	`border` TINYINT(1) NULL,
	`fringe` TINYINT(1) NULL,
	`corridor` TINYINT(1) NULL,
	`hub` TINYINT(1) NULL,
	`international` TINYINT(1) NULL,
	`regional` TINYINT(1) NULL,
	`constellation` TINYINT(1) NULL,
	`security` DOUBLE NULL,
	`factionID` INT(10) unsigned NULL,
	`radius` DOUBLE NULL,
	`sunTypeID` INT(10) unsigned NULL,
	`securityClass` VARCHAR(2) NULL,
	PRIMARY KEY (`solarSystemID` ASC),
	KEY `mapSolarSystems_IX_constellation` (`constellationID` ASC),
	KEY `mapSolarSystems_IX_region` (`regionID` ASC),
	KEY `mapSolarSystems_IX_security` (`security` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapSolarSystems`
--

LOCK TABLES `mapSolarSystems` WRITE;
/*!40000 ALTER TABLE `mapSolarSystems` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapSolarSystems` ENABLE KEYS */;
UNLOCK TABLES;