﻿--
-- Table structure for table `eveIcons`
--

DROP TABLE IF EXISTS `eveIcons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eveIcons` (
	`iconID` INT(10) unsigned NOT NULL,
	`iconFile` VARCHAR(500) NOT NULL DEFAULT '',
	`description` NVARCHAR(8000) NOT NULL DEFAULT '',
	PRIMARY KEY (`iconID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eveIcons`
--

LOCK TABLES `eveIcons` WRITE;
/*!40000 ALTER TABLE `eveIcons` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `eveIcons` ENABLE KEYS */;
UNLOCK TABLES;