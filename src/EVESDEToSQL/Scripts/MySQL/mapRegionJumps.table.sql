﻿--
-- Table structure for table `mapRegionJumps`
--

DROP TABLE IF EXISTS `mapRegionJumps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapRegionJumps` (
	`fromRegionID` INT(10) unsigned NOT NULL,
	`toRegionID` INT(10) unsigned NOT NULL,
	PRIMARY KEY (`fromRegionID` ASC, `toRegionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapRegionJumps`
--

LOCK TABLES `mapRegionJumps` WRITE;
/*!40000 ALTER TABLE `mapRegionJumps` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapRegionJumps` ENABLE KEYS */;
UNLOCK TABLES;