﻿--
-- Table structure for table `mapLocationWormholeClasses`
--

DROP TABLE IF EXISTS `mapLocationWormholeClasses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapLocationWormholeClasses` (
	`locationID` INT(10) unsigned NOT NULL,
	`wormholeClassID` TINYINT(3) unsigned NULL,
	PRIMARY KEY (`locationID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapLocationWormholeClasses`
--

LOCK TABLES `mapLocationWormholeClasses` WRITE;
/*!40000 ALTER TABLE `mapLocationWormholeClasses` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapLocationWormholeClasses` ENABLE KEYS */;
UNLOCK TABLES;