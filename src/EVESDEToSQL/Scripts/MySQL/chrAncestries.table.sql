﻿--
-- Table structure for table `chrAncestries`
--

DROP TABLE IF EXISTS `chrAncestries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chrAncestries` (
	`ancestryID` TINYINT(3) unsigned NOT NULL,
	`ancestryName` NVARCHAR(100) NULL,
	`bloodlineID` TINYINT(3) unsigned NULL,
	`description` NVARCHAR(1000) NULL,
	`perception` TINYINT(3) NULL,
	`willpower` TINYINT(3) NULL,
	`charisma` TINYINT(3) NULL,
	`memory` TINYINT(3) NULL,
	`intelligence` TINYINT(3) NULL,
	`iconID` INT(10) unsigned NULL,
	`shortDescription` NVARCHAR(500) NULL,
	PRIMARY KEY (`ancestryID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chrAncestries`
--

LOCK TABLES `chrAncestries` WRITE;
/*!40000 ALTER TABLE `chrAncestries` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `chrAncestries` ENABLE KEYS */;
UNLOCK TABLES;