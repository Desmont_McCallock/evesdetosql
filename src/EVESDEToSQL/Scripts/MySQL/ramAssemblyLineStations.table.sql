﻿--
-- Table structure for table `ramAssemblyLineStations`
--

DROP TABLE IF EXISTS `ramAssemblyLineStations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramAssemblyLineStations` (
	`stationID` INT(10) unsigned NOT NULL,
	`assemblyLineTypeID` TINYINT(3) unsigned NOT NULL,
	`quantity` TINYINT(3) NULL,
	`stationTypeID` INT(10) unsigned NULL,
	`ownerID` INT(10) unsigned NULL,
	`solarSystemID` INT(10) unsigned NULL,
	`regionID` INT(10) unsigned NULL,
	PRIMARY KEY (`stationID` ASC, `assemblyLineTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ramAssemblyLineStations`
--

LOCK TABLES `ramAssemblyLineStations` WRITE;
/*!40000 ALTER TABLE `ramAssemblyLineStations` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `ramAssemblyLineStations` ENABLE KEYS */;
UNLOCK TABLES;