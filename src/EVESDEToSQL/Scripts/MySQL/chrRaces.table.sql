﻿--
-- Table structure for table `chrRaces`
--

DROP TABLE IF EXISTS `chrRaces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chrRaces` (
	`raceID` TINYINT(3) unsigned NOT NULL,
	`raceName` VARCHAR(100) NULL,
	`description` VARCHAR(1000) NULL,
	`iconID` INT(10) unsigned NULL,
	`shortDescription` VARCHAR(500) NULL,
    PRIMARY KEY (`raceID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chrRaces`
--

LOCK TABLES `chrRaces` WRITE;
/*!40000 ALTER TABLE `chrRaces` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `chrRaces` ENABLE KEYS */;
UNLOCK TABLES;