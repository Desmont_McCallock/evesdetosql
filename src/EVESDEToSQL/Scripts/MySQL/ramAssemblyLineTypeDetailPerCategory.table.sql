﻿--
-- Table structure for table `ramAssemblyLineTypeDetailPerCategory`
--

DROP TABLE IF EXISTS `ramAssemblyLineTypeDetailPerCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ramAssemblyLineTypeDetailPerCategory` (
	`assemblyLineTypeID` TINYINT(3) unsigned NOT NULL,
	`categoryID` INT(10) unsigned NOT NULL,
	`timeMultiplier` DOUBLE NULL,
	`materialMultiplier` DOUBLE NULL,
	`costMultiplier` DOUBLE NULL,
	PRIMARY KEY (`assemblyLineTypeID` ASC, `categoryID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ramAssemblyLineTypeDetailPerCategory`
--

LOCK TABLES `ramAssemblyLineTypeDetailPerCategory` WRITE;
/*!40000 ALTER TABLE `ramAssemblyLineTypeDetailPerCategory` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `ramAssemblyLineTypeDetailPerCategory` ENABLE KEYS */;
UNLOCK TABLES;