﻿--
-- Table structure for table `trnTranslations`
--

DROP TABLE IF EXISTS `trnTranslations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trnTranslations` (
	`tcID` SMALLINT(6) unsigned NOT NULL,
	`keyID` INT(10) unsigned NOT NULL,
	`languageID` VARCHAR(50) NOT NULL,
	`text` NVARCHAR(8000) NOT NULL,
	PRIMARY KEY (`tcID` ASC, `keyID` ASC, `languageID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trnTranslations`
--

LOCK TABLES `trnTranslations` WRITE;
/*!40000 ALTER TABLE `trnTranslations` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `trnTranslations` ENABLE KEYS */;
UNLOCK TABLES;