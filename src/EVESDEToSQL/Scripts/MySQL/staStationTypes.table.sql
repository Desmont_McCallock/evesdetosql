﻿--
-- Table structure for table `staStationTypes`
--

DROP TABLE IF EXISTS `staStationTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staStationTypes` (
	`stationTypeID` INT(10) unsigned NOT NULL,
	`dockEntryX` DOUBLE NULL,
	`dockEntryY` DOUBLE NULL,
	`dockEntryZ` DOUBLE NULL,
	`dockOrientationX` DOUBLE NULL,
	`dockOrientationY` DOUBLE NULL,
	`dockOrientationZ` DOUBLE NULL,
	`operationID` TINYINT(3) unsigned NULL,
	`officeSlots` TINYINT(3) NULL,
	`reprocessingEfficiency` DOUBLE NULL,
	`conquerable` TINYINT(1) NULL,
	PRIMARY KEY (`stationTypeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staStationTypes`
--

LOCK TABLES `staStationTypes` WRITE;
/*!40000 ALTER TABLE `staStationTypes` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `staStationTypes` ENABLE KEYS */;
UNLOCK TABLES;