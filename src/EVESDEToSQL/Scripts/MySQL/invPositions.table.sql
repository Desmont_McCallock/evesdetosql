﻿--
-- Table structure for table `invPositions`
--

DROP TABLE IF EXISTS `invPositions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invPositions` (
	`itemID` BIGINT(20) unsigned NOT NULL,
	`x` DOUBLE NOT NULL DEFAULT 0.0,
	`y` DOUBLE NOT NULL DEFAULT 0.0,
	`z` DOUBLE NOT NULL DEFAULT 0.0,
	`yaw` DOUBLE NULL,
	`pitch` DOUBLE NULL,
	`roll` DOUBLE NULL,
	PRIMARY KEY (`itemID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invPositions`
--

LOCK TABLES `invPositions` WRITE;
/*!40000 ALTER TABLE `invPositions` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invPositions` ENABLE KEYS */;
UNLOCK TABLES;