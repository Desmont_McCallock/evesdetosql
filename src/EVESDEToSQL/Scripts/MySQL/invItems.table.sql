﻿--
-- Table structure for table `invItems`
--

DROP TABLE IF EXISTS `invItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invItems` (
	`itemID` BIGINT(20) unsigned NOT NULL,
	`typeID` INT(10) unsigned NOT NULL,
	`ownerID` INT(10) unsigned NOT NULL,
	`locationID` BIGINT(20) unsigned NOT NULL,
	`flagID` SMALLINT(6) unsigned NOT NULL,
	`quantity` INT(10) NOT NULL,
	PRIMARY KEY (`itemID` ASC),
	KEY `items_IX_Location` (`locationID` ASC),
	KEY `items_IX_OwnerLocation` (`ownerID` ASC, `locationID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invItems`
--

LOCK TABLES `invItems` WRITE;
/*!40000 ALTER TABLE `invItems` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `invItems` ENABLE KEYS */;
UNLOCK TABLES;