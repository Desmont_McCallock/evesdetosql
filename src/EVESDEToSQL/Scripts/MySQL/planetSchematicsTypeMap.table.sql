﻿--
-- Table structure for table `planetSchematicsTypeMap`
--

DROP TABLE IF EXISTS `planetSchematicsTypeMap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `planetSchematicsTypeMap` (
	`schematicID` SMALLINT(6) unsigned NOT NULL,
	`typeID` INT(10) unsigned NOT NULL,
	`quantity` SMALLINT(6) NULL,
	`isInput` TINYINT(1) NULL,
	PRIMARY KEY (`schematicID` ASC,	`typeID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `planetSchematicsTypeMap`
--

LOCK TABLES `planetSchematicsTypeMap` WRITE;
/*!40000 ALTER TABLE `planetSchematicsTypeMap` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `planetSchematicsTypeMap` ENABLE KEYS */;
UNLOCK TABLES;