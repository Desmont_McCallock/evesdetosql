﻿--
-- Table structure for table `mapConstellations`
--

DROP TABLE IF EXISTS `mapConstellations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mapConstellations` (
	`regionID` INT(10) unsigned NULL,
	`constellationID` INT(10) unsigned NOT NULL,
	`constellationName` NVARCHAR(100) NULL,
	`x` DOUBLE NULL,
	`y` DOUBLE NULL,
	`z` DOUBLE NULL,
	`xMin` DOUBLE NULL,
	`xMax` DOUBLE NULL,
	`yMin` DOUBLE NULL,
	`yMax` DOUBLE NULL,
	`zMin` DOUBLE NULL,
	`zMax` DOUBLE NULL,
	`factionID` INT(10) unsigned NULL,
	`radius` DOUBLE NULL,
	PRIMARY KEY (`constellationID` ASC),
	KEY `mapConstellations_IX_region` (`regionID` ASC)
) ENGINE InnoDB CHARSET utf8 COLLATE utf8_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mapConstellations`
--

LOCK TABLES `mapConstellations` WRITE;
/*!40000 ALTER TABLE `mapConstellations` DISABLE KEYS */;
{INSERTCOMMAND}
/*!40000 ALTER TABLE `mapConstellations` ENABLE KEYS */;
UNLOCK TABLES;