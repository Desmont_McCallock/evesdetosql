﻿-- ----------------------------
-- Table structure for table "invControlTowerResources"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invControlTowerResources";

CREATE TABLE "public"."invControlTowerResources"(
	"controlTowerTypeID" int NOT NULL,
	"resourceTypeID" int NOT NULL,
	purpose smallint NULL,
	quantity int NULL,
	"minSecurityLevel" double precision NULL,
	"factionID" int NULL,
	PRIMARY KEY ("controlTowerTypeID", "resourceTypeID")
);