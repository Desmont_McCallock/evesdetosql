﻿-- ----------------------------
-- Table structure for table "warCombatZones"
-- ----------------------------

DROP TABLE IF EXISTS "public"."warCombatZones";

CREATE TABLE "public"."warCombatZones"(
	"combatZoneID" int NOT NULL DEFAULT (-1),
	"combatZoneName" varchar(100) NULL,
	"factionID" int NULL,
	"centerSystemID" int NULL,
	description varchar(500) NULL,
	PRIMARY KEY ("combatZoneID")
);