﻿-- ----------------------------
-- Table structure for table "planetSchematicsTypeMap"
-- ----------------------------

DROP TABLE IF EXISTS "public"."planetSchematicsTypeMap";

CREATE TABLE "public"."planetSchematicsTypeMap"(
	"schematicID" smallint NOT NULL,
	"typeID" int NOT NULL,
	quantity smallint NULL,
	"isInput" bool NULL,
	PRIMARY KEY ("schematicID", "typeID")
);