﻿-- ----------------------------
-- Table structure for table "invMarketGroups"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invMarketGroups";

CREATE TABLE "public"."invMarketGroups"(
	"marketGroupID" int NOT NULL,
	"parentGroupID" int NULL,
	"marketGroupName" varchar(100) NULL,
	description varchar(3000) NULL,
	"iconID" int NULL,
	"hasTypes" bool NULL,
	PRIMARY KEY ("marketGroupID")
);