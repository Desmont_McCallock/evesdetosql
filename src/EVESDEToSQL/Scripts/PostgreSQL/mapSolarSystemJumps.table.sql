﻿-- ----------------------------
-- Table structure for table "mapSolarSystemJumps"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapSolarSystemJumps";

CREATE TABLE "public"."mapSolarSystemJumps"(
	"fromRegionID" int NULL,
	"fromConstellationID" int NULL,
	"fromSolarSystemID" int NOT NULL,
	"toSolarSystemID" int NOT NULL,
	"toConstellationID" int NULL,
	"toRegionID" int NULL,
	PRIMARY KEY ("fromSolarSystemID", "toSolarSystemID")
);

CREATE INDEX "mapSolarSystemJumps_IX_fromConstellation" ON "public"."mapSolarSystemJumps" ("fromConstellationID" ASC);

CREATE INDEX "mapSolarSystemJumps_IX_fromRegion" ON "public"."mapSolarSystemJumps" ("fromRegionID" ASC);
