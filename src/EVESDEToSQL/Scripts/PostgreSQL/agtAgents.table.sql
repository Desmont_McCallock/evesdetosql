﻿-- ----------------------------
-- Table structure for table "agtAgents"
-- ----------------------------

DROP TABLE IF EXISTS "public"."agtAgents";

CREATE TABLE "public"."agtAgents"(
	"agentID" int NOT NULL,
	"divisionID" smallint NULL,
	"corporationID" int NULL,
	"locationID" int NULL,
	level smallint NULL,
	quality smallint NULL,
	"agentTypeID" int NULL,
	"isLocator" bool NULL,
	PRIMARY KEY ("agentID")
);

CREATE INDEX "agtAgents_IX_corporation" ON "public"."agtAgents" ("corporationID" ASC);

CREATE INDEX "agtAgents_IX_station" ON "public"."agtAgents" ("locationID" ASC);
