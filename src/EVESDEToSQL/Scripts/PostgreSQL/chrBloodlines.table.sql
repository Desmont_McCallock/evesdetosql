﻿-- ----------------------------
-- Table structure for table "chrBloodlines"
-- ----------------------------

DROP TABLE IF EXISTS "public"."chrBloodlines";

CREATE TABLE "public"."chrBloodlines"(
	"bloodlineID" smallint NOT NULL,
	"bloodlineName" varchar(100) NULL,
	"raceID" smallint NULL,
	description varchar(1000) NULL,
	"maleDescription" varchar(1000) NULL,
	"femaleDescription" varchar(1000) NULL,
	"shipTypeID" int NULL,
	"corporationID" int NULL,
	perception smallint NULL,
	willpower smallint NULL,
	charisma smallint NULL,
	memory smallint NULL,
	intelligence smallint NULL,
	"iconID" int NULL,
	"shortDescription" varchar(500) NULL,
	"shortMaleDescription" varchar(500) NULL,
	"shortFemaleDescription" varchar(500) NULL,
	PRIMARY KEY ("bloodlineID")
);