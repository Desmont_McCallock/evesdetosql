-- ----------------------------
-- Table structure for table "crtClasses"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crtClasses";

CREATE TABLE "public"."crtClasses"(
	"classID" int NOT NULL,
	description varchar(500) NULL,
	"className" varchar(256) NULL,
	PRIMARY KEY ("classID")
);