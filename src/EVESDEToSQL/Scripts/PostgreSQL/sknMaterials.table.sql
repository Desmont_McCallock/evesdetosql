﻿-- ----------------------------
-- Table structure for table "sknMaterials"
-- ----------------------------

DROP TABLE IF EXISTS "public"."sknMaterials";

CREATE TABLE "public"."sknMaterials"(
	"skinMaterialID" int NOT NULL,
	"materialSetID" int NOT NULL DEFAULT (0),
	"displayNameID" int NOT NULL DEFAULT (0),
	-- Obsolete columns since Galatea 1.0
	material varchar(255) NOT NULL DEFAULT (''),
	"colorHull" varchar(6) NULL,
	"colorWindow" varchar(6) NULL,
	"colorPrimary" varchar(6) NULL,
	"colorSecondary" varchar(6) NULL,
	PRIMARY KEY ("skinMaterialID")
);