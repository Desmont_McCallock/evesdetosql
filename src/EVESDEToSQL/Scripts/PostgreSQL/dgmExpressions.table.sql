﻿-- ----------------------------
-- Table structure for table "dgmExpressions"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmExpressions";

CREATE TABLE "public"."dgmExpressions"(
	"expressionID" int NOT NULL,
	"operandID" int NULL,
	arg1 int NULL,
	arg2 int NULL,
	"expressionValue" varchar(100) NULL,
	description varchar(1000) NULL,
	"expressionName" varchar(500) NULL,
	"expressionTypeID" int NULL,
	"expressionGroupID" smallint NULL,
	"expressionAttributeID" smallint NULL,
	PRIMARY KEY ("expressionID")
);