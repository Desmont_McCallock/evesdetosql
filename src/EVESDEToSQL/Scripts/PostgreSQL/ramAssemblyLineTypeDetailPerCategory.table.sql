﻿-- ----------------------------
-- Table structure for table "ramAssemblyLineTypeDetailPerCategory"
-- ----------------------------

DROP TABLE IF EXISTS "public"."ramAssemblyLineTypeDetailPerCategory";

CREATE TABLE "public"."ramAssemblyLineTypeDetailPerCategory"(
	"assemblyLineTypeID" smallint NOT NULL,
	"categoryID" int NOT NULL,
	"timeMultiplier" double precision NULL,
	"materialMultiplier" double precision NULL,
	"costMultiplier" double precision NULL,
	PRIMARY KEY ("assemblyLineTypeID", "categoryID")
);