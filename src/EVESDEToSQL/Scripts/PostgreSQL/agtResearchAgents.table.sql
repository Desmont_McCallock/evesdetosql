﻿-- ----------------------------
-- Table structure for table "agtResearchAgents"
-- ----------------------------

DROP TABLE IF EXISTS "public"."agtResearchAgents";

CREATE TABLE "public"."agtResearchAgents"(
	"agentID" int NOT NULL,
	"typeID" int NOT NULL,
	PRIMARY KEY ("agentID", "typeID")
);

CREATE INDEX "agtResearchAgents_IX_type" ON "public"."agtResearchAgents" ("typeID" ASC);
