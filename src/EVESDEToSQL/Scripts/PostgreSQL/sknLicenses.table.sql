﻿-- ----------------------------
-- Table structure for table "sknLicenses"
-- ----------------------------

DROP TABLE IF EXISTS "public"."sknLicenses";

CREATE TABLE "public"."sknLicenses"(
	"licenseTypeID" int NOT NULL,
	"skinID" int NOT NULL,
	duration int NOT NULL  DEFAULT (-1),
	PRIMARY KEY ("licenseTypeID")
);

CREATE INDEX "sknLicenses_IX_skin" ON "public"."sknLicenses" ("skinID" ASC);
