﻿-- ----------------------------
-- Table structure for table "crpNPCCorporationTrades"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crpNPCCorporationTrades";

CREATE TABLE "public"."crpNPCCorporationTrades"(
	"corporationID" int NOT NULL,
	"typeID" int NOT NULL,
	PRIMARY KEY ("corporationID", "typeID")
);