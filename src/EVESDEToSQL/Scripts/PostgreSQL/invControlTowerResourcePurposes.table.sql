﻿-- ----------------------------
-- Table structure for table "invControlTowerResourcePurposes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invControlTowerResourcePurposes";

CREATE TABLE "public"."invControlTowerResourcePurposes"(
	purpose smallint NOT NULL,
	"purposeText" varchar(100) NULL,
	PRIMARY KEY (purpose)
);