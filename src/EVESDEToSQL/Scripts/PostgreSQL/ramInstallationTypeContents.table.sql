﻿-- ----------------------------
-- Table structure for table "ramInstallationTypeContents"
-- ----------------------------

DROP TABLE IF EXISTS "public"."ramInstallationTypeContents";

CREATE TABLE "public"."ramInstallationTypeContents"(
	"installationTypeID" int NOT NULL,
	"assemblyLineTypeID" smallint NOT NULL,
	quantity smallint NULL,
	PRIMARY KEY ("installationTypeID", "assemblyLineTypeID")
);