﻿-- ----------------------------
-- Table structure for table "invTypeReactions"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invTypeReactions";

CREATE TABLE "public"."invTypeReactions"(
	"reactionTypeID" int NOT NULL,
	input bool NOT NULL,
	"typeID" int NOT NULL,
	quantity smallint NULL,
	PRIMARY KEY ("reactionTypeID", input, "typeID")
);