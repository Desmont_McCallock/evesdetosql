-- ----------------------------
-- Table structure for table "crtCertificates"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crtCertificates";

CREATE TABLE "public"."crtCertificates"(
	"certificateID" int NOT NULL,
	"groupID" smallint NULL,
	"classID" int NULL,
	grade smallint NULL,
	"corpID" int NULL,
	"iconID" int NULL,
	description varchar(500) NULL,
	PRIMARY KEY ("certificateID")
);

CREATE INDEX "crtCertificates_IX_group" ON "public"."crtCertificates" ("groupID" ASC);

CREATE INDEX "crtCertificates_IX_class" ON "public"."crtCertificates" ("classID" ASC);
