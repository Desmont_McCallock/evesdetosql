﻿-- ----------------------------
-- Table structure for table "invMetaTypes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invMetaTypes";

CREATE TABLE "public"."invMetaTypes"(
	"typeID" int NOT NULL,
	"parentTypeID" int NULL,
	"metaGroupID" smallint NULL,
	PRIMARY KEY ("typeID")
);