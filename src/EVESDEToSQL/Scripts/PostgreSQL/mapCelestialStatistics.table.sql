﻿-- ----------------------------
-- Table structure for table "mapCelestialStatistics"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapCelestialStatistics";

CREATE TABLE "public"."mapCelestialStatistics"(
	"celestialID" int NOT NULL,
	temperature double precision NULL,
	"spectralClass" varchar(10) NULL,
	luminosity double precision NULL,
	age double precision NULL,
	life double precision NULL,
	"orbitRadius" double precision NULL,
	eccentricity double precision NULL,
	"massDust" double precision NULL,
	"massGas" double precision NULL,
	fragmented bool NULL,
	density double precision NULL,
	"surfaceGravity" double precision NULL,
	"escapeVelocity" double precision NULL,
	"orbitPeriod" double precision NULL,
	"rotationRate" double precision NULL,
	locked bool NULL,
	pressure double precision NULL,
	radius double precision NULL,
	mass double precision NULL,
	PRIMARY KEY ("celestialID")
);