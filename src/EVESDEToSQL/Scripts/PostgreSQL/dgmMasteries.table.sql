﻿-- ----------------------------
-- Table structure for table "crtCertificates"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmMasteries";

CREATE TABLE "public"."dgmMasteries"(
	"masteryID" int NOT NULL,
	"certificateID" int NOT NULL,
	grade smallint NOT NULL,
	PRIMARY KEY ("masteryID")
);

CREATE UNIQUE INDEX "dgmMasteries_UC_certificate_grade" ON "public"."dgmMasteries" ("certificateID" ASC, grade ASC);

CREATE INDEX "dgmMasteries_IX_certificate" ON "public"."dgmMasteries" ("certificateID" ASC);
