﻿-- ----------------------------
-- Table structure for table "ramTypeRequirements"
-- ----------------------------

DROP TABLE IF EXISTS "public"."ramTypeRequirements";

CREATE TABLE "public"."ramTypeRequirements"(
	"typeID" int NOT NULL,
	"activityID" smallint NOT NULL,
	"requiredTypeID" int NOT NULL,
	quantity int NULL,
	level int NULL,
	"damagePerJob" double precision NULL,
	recycle bool NULL,
	"raceID" int NULL,
	probability double precision NULL,
	consume bool NULL,
	PRIMARY KEY ("typeID", "activityID", "requiredTypeID")
);