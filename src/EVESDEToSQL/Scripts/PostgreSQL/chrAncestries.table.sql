﻿-- ----------------------------
-- Table structure for table "chrAncestries"
-- ----------------------------

DROP TABLE IF EXISTS "public"."chrAncestries";

CREATE TABLE "public"."chrAncestries"(
	"ancestryID" smallint NOT NULL,
	"ancestryName" varchar(100) NULL,
	"bloodlineID" smallint NULL,
	description varchar(1000) NULL,
	perception smallint NULL,
	willpower smallint NULL,
	charisma smallint NULL,
	memory smallint NULL,
	intelligence smallint NULL,
	"iconID" int NULL,
	"shortDescription" varchar(500) NULL,
	PRIMARY KEY ("ancestryID")
);