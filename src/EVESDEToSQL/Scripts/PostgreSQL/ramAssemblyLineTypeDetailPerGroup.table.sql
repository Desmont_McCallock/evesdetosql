﻿-- ----------------------------
-- Table structure for table "ramAssemblyLineTypeDetailPerGroup"
-- ----------------------------

DROP TABLE IF EXISTS "public"."ramAssemblyLineTypeDetailPerGroup";

CREATE TABLE "public"."ramAssemblyLineTypeDetailPerGroup"(
	"assemblyLineTypeID" smallint NOT NULL,
	"groupID" int NOT NULL,
	"timeMultiplier" double precision NULL,
	"materialMultiplier" double precision NULL,
	"costMultiplier" double precision NULL,
	PRIMARY KEY ("assemblyLineTypeID", "groupID")
);