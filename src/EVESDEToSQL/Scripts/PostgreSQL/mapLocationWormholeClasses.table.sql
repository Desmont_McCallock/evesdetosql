﻿-- ----------------------------
-- Table structure for table "mapLocationWormholeClasses"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapLocationWormholeClasses";

CREATE TABLE "public"."mapLocationWormholeClasses"(
	"locationID" int NOT NULL,
	"wormholeClassID" smallint NULL,
	PRIMARY KEY ("locationID")
);