﻿-- ----------------------------
-- Table structure for table "warCombatZoneSystems"
-- ----------------------------

DROP TABLE IF EXISTS "public"."warCombatZoneSystems";

CREATE TABLE "public"."warCombatZoneSystems"(
	"solarSystemID" int NOT NULL,
	"combatZoneID" int NULL,
	PRIMARY KEY ("solarSystemID")
);