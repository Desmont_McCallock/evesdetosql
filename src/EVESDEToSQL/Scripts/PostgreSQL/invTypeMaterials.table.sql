﻿-- ----------------------------
-- Table structure for table "invTypeMaterials"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invTypeMaterials";

CREATE TABLE "public"."invTypeMaterials"(
	"typeID" int NOT NULL,
	"materialTypeID" int NOT NULL,
	quantity int NOT NULL DEFAULT (0),
	PRIMARY KEY ("typeID", "materialTypeID")
);