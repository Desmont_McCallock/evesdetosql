﻿-- ----------------------------
-- Table structure for table "planetSchematics"
-- ----------------------------

DROP TABLE IF EXISTS "public"."planetSchematics";

CREATE TABLE "public"."planetSchematics"(
	"schematicID" smallint NOT NULL,
	"schematicName" varchar(255) NULL,
	"cycleTime" int NULL,
	PRIMARY KEY ("schematicID")
);