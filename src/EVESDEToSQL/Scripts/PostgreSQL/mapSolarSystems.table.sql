﻿-- ----------------------------
-- Table structure for table "mapSolarSystems"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapSolarSystems";

CREATE TABLE "public"."mapSolarSystems"(
	"regionID" int NULL,
	"constellationID" int NULL,
	"solarSystemID" int NOT NULL,
	"solarSystemName" varchar(100) NULL,
	x double precision NULL,
	y double precision NULL,
	z double precision NULL,
	"xMin" double precision NULL,
	"xMax" double precision NULL,
	"yMin" double precision NULL,
	"yMax" double precision NULL,
	"zMin" double precision NULL,
	"zMax" double precision NULL,
	luminosity double precision NULL,
	border bool NULL,
	fringe bool NULL,
	corridor bool NULL,
	hub bool NULL,
	international bool NULL,
	regional bool NULL,
	constellation bool NULL,
	security double precision NULL,
	"factionID" int NULL,
	radius double precision NULL,
	"sunTypeID" int NULL,
	"securityClass" varchar(2) NULL,
	PRIMARY KEY ("solarSystemID")
);

CREATE INDEX "mapSolarSystems_IX_constellation" ON "public"."mapSolarSystems" ("constellationID" ASC);

CREATE INDEX "mapSolarSystems_IX_region" ON "public"."mapSolarSystems" ("regionID" ASC);

CREATE INDEX "mapSolarSystems_IX_security" ON "public"."mapSolarSystems" ("security" ASC);
