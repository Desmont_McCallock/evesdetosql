﻿-- ----------------------------
-- Table structure for table "dgmEffects"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmEffects";

CREATE TABLE "public"."dgmEffects"(
	"effectID" smallint NOT NULL,
	"effectName" varchar(400) NULL,
	"effectCategory" smallint NULL,
	"preExpression" int NULL,
	"postExpression" int NULL,
	description varchar(1000) NULL,
	guid varchar(60) NULL,
	"iconID" int NULL,
	"isOffensive" bool NULL,
	"isAssistance" bool NULL,
	"durationAttributeID" smallint NULL,
	"trackingSpeedAttributeID" smallint NULL,
	"dischargeAttributeID" smallint NULL,
	"rangeAttributeID" smallint NULL,
	"falloffAttributeID" smallint NULL,
	"disallowAutoRepeat" bool NULL,
	published bool NULL,
	"displayName" varchar(100) NULL,
	"isWarpSafe" bool NULL,
	"rangeChance" bool NULL,
	"electronicChance" bool NULL,
	"propulsionChance" bool NULL,
	distribution smallint NULL,
	"sfxName" varchar(20) NULL,
	"npcUsageChanceAttributeID" smallint NULL,
	"npcActivationChanceAttributeID" smallint NULL,
	"fittingUsageChanceAttributeID" smallint NULL,
	"modifierInfo" varchar(8000) NULL,
	PRIMARY KEY ("effectID")
);