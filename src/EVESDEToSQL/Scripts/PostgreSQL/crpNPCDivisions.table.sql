﻿-- ----------------------------
-- Table structure for table "crpNPCDivisions"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crpNPCDivisions";

CREATE TABLE "public"."crpNPCDivisions"(
	"divisionID" smallint NOT NULL,
	"divisionName" varchar(100) NULL,
	description varchar(1000) NULL,
	"leaderType" varchar(100) NULL,
	PRIMARY KEY ("divisionID")
);