﻿-- ----------------------------
-- Table structure for table "eveUnits"
-- ----------------------------

DROP TABLE IF EXISTS "public"."eveUnits";

CREATE TABLE "public"."eveUnits"(
	"unitID" smallint NOT NULL,
	"unitName" varchar(100) NULL,
	"displayName" varchar(50) NULL,
	description varchar(1000) NULL,
	PRIMARY KEY ("unitID")
);