﻿-- ----------------------------
-- Table structure for table "agtAgentTypes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."agtAgentTypes";

CREATE TABLE "public"."agtAgentTypes"(
	"agentTypeID" int NOT NULL,
	"agentType" varchar(50) NULL,
	PRIMARY KEY ("agentTypeID")
);