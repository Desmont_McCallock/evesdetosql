﻿-- ----------------------------
-- Table structure for table "invTypes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invTypes";

CREATE TABLE "public"."invTypes"(
	"typeID" int NOT NULL,
	"groupID" int NULL,
	"typeName" varchar(100) NULL,
	description varchar(3000) NULL,
	mass double precision NULL,
	volume double precision NULL,
	capacity double precision NULL,
	"portionSize" int NULL,
	"raceID" smallint NULL,
	"basePrice" decimal(15,2) NULL,
	published bool NULL,
	"marketGroupID" int NULL,
	"chanceOfDuplicating" double precision NULL,
	"factionID" int NULL,
	"graphicID" int NULL,
	"iconID" int NULL,
	radius double precision NULL,
	"soundID" int NULL,
	PRIMARY KEY ("typeID")
);

CREATE INDEX "invTypes_IX_Group" ON "public"."invTypes" ("groupID" ASC);
