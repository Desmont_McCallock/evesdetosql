﻿-- ----------------------------
-- Table structure for table "eveGraphics"
-- ----------------------------

DROP TABLE IF EXISTS "public"."eveGraphics";

CREATE TABLE "public"."eveGraphics"(
	"graphicID" int NOT NULL,
	"graphicFile" varchar(500) NOT NULL DEFAULT (''),
	description varchar(8000) NOT NULL DEFAULT (''),
	obsolete bool NOT NULL DEFAULT (0)::bool,
	"graphicType" varchar(100) NULL,
	collidable bool NULL,
	"directoryID" int NULL,
	"graphicName" varchar(64) NOT NULL DEFAULT (''),
	"gfxRaceID" varchar(255) NULL,
	"colorScheme" varchar(255) NULL,
	"sofHullName" varchar(64) NULL,
	PRIMARY KEY ("graphicID")
);