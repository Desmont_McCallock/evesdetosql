﻿-- ----------------------------
-- Table structure for table "invItems"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invItems";

CREATE TABLE "public"."invItems"(
	"itemID" bigint NOT NULL,
	"typeID" int NOT NULL,
	"ownerID" int NOT NULL,
	"locationID" bigint NOT NULL,
	"flagID" smallint NOT NULL,
	quantity int NOT NULL,
	PRIMARY KEY ("itemID")
);

CREATE INDEX "items_IX_Location" ON "public"."invItems" ("locationID" ASC);

CREATE INDEX "items_IX_OwnerLocation" ON "public"."invItems" ("ownerID" ASC, "locationID" ASC);
