﻿-- ----------------------------
-- Table structure for table "crpNPCCorporationDivisions"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crpNPCCorporationDivisions";

CREATE TABLE "public"."crpNPCCorporationDivisions"(
	"corporationID" int NOT NULL,
	"divisionID" smallint NOT NULL,
	size smallint NULL,
	PRIMARY KEY ("corporationID", "divisionID")
);