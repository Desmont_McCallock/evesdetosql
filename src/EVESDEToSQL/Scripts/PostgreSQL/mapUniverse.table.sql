﻿-- ----------------------------
-- Table structure for table "mapUniverse"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapUniverse";

CREATE TABLE "public"."mapUniverse"(
	"universeID" int NOT NULL,
	"universeName" varchar(100) NULL,
	x double precision NULL,
	y double precision NULL,
	z double precision NULL,
	"xMin" double precision NULL,
	"xMax" double precision NULL,
	"yMin" double precision NULL,
	"yMax" double precision NULL,
	"zMin" double precision NULL,
	"zMax" double precision NULL,
	radius double precision NULL,
	PRIMARY KEY ("universeID")
);