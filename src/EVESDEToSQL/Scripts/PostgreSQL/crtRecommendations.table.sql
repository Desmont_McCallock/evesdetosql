-- ----------------------------
-- Table structure for table "crtRelationships"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crtRecommendations";

CREATE TABLE "public"."crtRecommendations"(
	"recommendationID" int NOT NULL,
	"shipTypeID" int NULL,
	"certificateID" int NULL,
	"recommendationLevel" smallint NOT NULL,
	PRIMARY KEY ("recommendationID")
);

CREATE INDEX "crtRecommendations_IX_certificate" ON "public"."crtRecommendations" ("certificateID" ASC);

CREATE INDEX "crtRecommendations_IX_shipType" ON "public"."crtRecommendations" ("shipTypeID" ASC);
