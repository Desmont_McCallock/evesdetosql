﻿-- ----------------------------
-- Table structure for table "chrRaces"
-- ----------------------------

DROP TABLE IF EXISTS "public"."chrRaces";

CREATE TABLE "public"."chrRaces"(
	"raceID" smallint NOT NULL,
	"raceName" varchar(100) NULL,
	description varchar(1000) NULL,
	"iconID" int NULL,
	"shortDescription" varchar(500) NULL,
    PRIMARY KEY ("raceID")
);