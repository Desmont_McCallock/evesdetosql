﻿-- ----------------------------
-- Table structure for table "crpNPCCorporations"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crpNPCCorporations";

CREATE TABLE "public"."crpNPCCorporations"(
	"corporationID" int NOT NULL,
	size char NULL,
	extent char NULL,
	"solarSystemID" int NULL,
	"investorID1" int NULL,
	"investorShares1" smallint NULL,
	"investorID2" int NULL,
	"investorShares2" smallint NULL,
	"investorID3" int NULL,
	"investorShares3" smallint NULL,
	"investorID4" int NULL,
	"investorShares4" smallint NULL,
	"friendID" int NULL,
	"enemyID" int NULL,
	"publicShares" bigint NULL,
	"initialPrice" int NULL,
	"minSecurity" double precision NULL,
	scattered bool NULL,
	fringe smallint NULL,
	corridor smallint NULL,
	hub smallint NULL,
	border smallint NULL,
	"factionID" int NULL,
	"sizeFactor" double precision NULL,
	"stationCount" smallint NULL,
	"stationSystemCount" smallint NULL,
	description varchar(4000) NULL,
	"iconID" int NULL,
	PRIMARY KEY ("corporationID")
);