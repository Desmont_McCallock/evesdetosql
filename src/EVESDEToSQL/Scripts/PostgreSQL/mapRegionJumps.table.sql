﻿-- ----------------------------
-- Table structure for table "mapRegionJumps"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapRegionJumps";

CREATE TABLE "public"."mapRegionJumps"(
	"fromRegionID" int NOT NULL,
	"toRegionID" int NOT NULL,
	PRIMARY KEY ("fromRegionID", "toRegionID")
);