﻿-- ----------------------------
-- Table structure for table "trnTranslations"
-- ----------------------------

DROP TABLE IF EXISTS "public"."trnTranslations";

CREATE TABLE "public"."trnTranslations"(
	"tcID" smallint NOT NULL,
	"keyID" int NOT NULL,
	"languageID" varchar(50) NOT NULL,
	text varchar(8000) NOT NULL,
	PRIMARY KEY ("tcID", "keyID", "languageID")
);