﻿-- ----------------------------
-- Table structure for table "mapLocationScenes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapLocationScenes";

CREATE TABLE "public"."mapLocationScenes"(
	"locationID" int NOT NULL,
	"graphicID" int NULL,
	PRIMARY KEY ("locationID")
);