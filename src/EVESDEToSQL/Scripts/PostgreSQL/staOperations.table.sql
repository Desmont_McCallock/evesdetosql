﻿-- ----------------------------
-- Table structure for table "staOperations"
-- ----------------------------

DROP TABLE IF EXISTS "public"."staOperations";

CREATE TABLE "public"."staOperations"(
	"activityID" smallint NULL,
	"operationID" smallint NOT NULL,
	"operationName" varchar(100) NULL,
	description varchar(1000) NULL,
	fringe smallint NULL,
	corridor smallint NULL,
	hub smallint NULL,
	border smallint NULL,
	ratio smallint NULL,
	"caldariStationTypeID" int NULL,
	"minmatarStationTypeID" int NULL,
	"amarrStationTypeID" int NULL,
	"gallenteStationTypeID" int NULL,
	"joveStationTypeID" int NULL,
	PRIMARY KEY ("operationID")
);