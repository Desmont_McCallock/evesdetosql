﻿-- ----------------------------
-- Table structure for table "staServices"
-- ----------------------------

DROP TABLE IF EXISTS "public"."staServices";

CREATE TABLE "public"."staServices"(
	"serviceID" int NOT NULL,
	"serviceName" varchar(100) NULL,
	description varchar(1000) NULL,
	PRIMARY KEY ("serviceID")
);