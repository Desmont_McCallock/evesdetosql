-- ----------------------------
-- Table structure for table "crtRelationships"
-- ----------------------------

DROP TABLE IF EXISTS "public"."crtRelationships";

CREATE TABLE "public"."crtRelationships"(
	"relationshipID" int NOT NULL,
	"parentID" int NULL,
	"parentTypeID" int NULL,
	"parentLevel" smallint NULL,
	"childID" int NULL,
	grade smallint NULL,
	PRIMARY KEY ("relationshipID")
);

CREATE INDEX "crtRelationships_IX_child" ON "public"."crtRelationships" ("childID" ASC);

CREATE INDEX "crtRelationships_IX_parent" ON "public"."crtRelationships" ("parentID" ASC);
