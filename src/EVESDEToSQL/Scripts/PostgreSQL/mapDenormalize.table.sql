﻿-- ----------------------------
-- Table structure for table "mapDenormalize"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapDenormalize";

CREATE TABLE "public"."mapDenormalize"(
	"itemID" int NOT NULL,
	"typeID" int NULL,
	"groupID" int NULL,
	"solarSystemID" int NULL,
	"constellationID" int NULL,
	"regionID" int NULL,
	"orbitID" int NULL,
	x double precision NULL,
	y double precision NULL,
	z double precision NULL,
	radius double precision NULL,
	"itemName" varchar(100) NULL,
	security double precision NULL,
	"celestialIndex" smallint NULL,
	"orbitIndex" smallint NULL,
	PRIMARY KEY ("itemID")
);

CREATE INDEX "mapDenormalize_IX_constellation" ON "public"."mapDenormalize" ("constellationID" ASC);

CREATE INDEX "mapDenormalize_IX_groupConstellation" ON "public"."mapDenormalize" ("groupID" ASC, "constellationID" ASC);

CREATE INDEX "mapDenormalize_IX_groupRegion" ON "public"."mapDenormalize" ("groupID" ASC, "regionID" ASC);

CREATE INDEX "mapDenormalize_IX_groupSystem" ON "public"."mapDenormalize" ("groupID" ASC, "solarSystemID" ASC);

CREATE INDEX "mapDenormalize_IX_orbit" ON "public"."mapDenormalize" ("orbitID" ASC);

CREATE INDEX "mapDenormalize_IX_region" ON "public"."mapDenormalize" ("regionID" ASC);

CREATE INDEX "mapDenormalize_IX_system" ON "public"."mapDenormalize" ("solarSystemID" ASC);
