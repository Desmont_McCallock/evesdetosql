﻿-- ----------------------------
-- Table structure for table "trnTranslationLanguages"
-- ----------------------------

DROP TABLE IF EXISTS "public"."trnTranslationLanguages";

CREATE TABLE "public"."trnTranslationLanguages"(
	"numericLanguageID" int NOT NULL,
	"languageID" varchar(50) NULL,
	"languageName" varchar(200) NULL,
	PRIMARY KEY ("numericLanguageID")
);