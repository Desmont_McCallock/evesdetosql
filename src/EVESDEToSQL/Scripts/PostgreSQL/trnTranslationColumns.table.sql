﻿-- ----------------------------
-- Table structure for table "trnTranslationColumns"
-- ----------------------------

DROP TABLE IF EXISTS "public"."trnTranslationColumns";

CREATE TABLE "public"."trnTranslationColumns"(
	"tcGroupID" smallint NULL,
	"tcID" smallint NOT NULL,
	"tableName" varchar(256) NOT NULL,
	"columnName" varchar(128) NOT NULL,
	"masterID" varchar(128) NULL,
	PRIMARY KEY ("tcID")
);