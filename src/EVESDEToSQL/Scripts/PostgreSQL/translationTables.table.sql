﻿-- ----------------------------
-- Table structure for table "translationTables"
-- ----------------------------

DROP TABLE IF EXISTS "public"."translationTables";

CREATE TABLE "public"."translationTables"(
	"sourceTable" varchar(200) NOT NULL,
	"destinationTable" varchar(200) NULL,
	"translatedKey" varchar(200) NOT NULL,
	"tcGroupID" int NULL,
	"tcID" int NULL,
	PRIMARY KEY ("sourceTable", "translatedKey")
);