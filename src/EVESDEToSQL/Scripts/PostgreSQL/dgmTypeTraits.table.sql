﻿-- ----------------------------
-- Table structure for table "dgmTypeTraits"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmTypeTraits";

CREATE TABLE "public"."dgmTypeTraits"(
	"typeID" int NOT NULL,
	"parentTypeID" int NOT NULL,
	"traitID" int NOT NULL,
	bonus double precision NULL,
	PRIMARY KEY ("typeID", "parentTypeID", "traitID")
);