﻿-- ----------------------------
-- Table structure for table "invContrabandTypes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invContrabandTypes";

CREATE TABLE "public"."invContrabandTypes"(
	"factionID" int NOT NULL,
	"typeID" int NOT NULL,
	"standingLoss" double precision NULL,
	"confiscateMinSec" double precision NULL,
	"fineByValue" double precision NULL,
	"attackMinSec" double precision NULL,
	PRIMARY KEY ("factionID", "typeID")
);

CREATE INDEX "invContrabandTypes_IX_type" ON "public"."invContrabandTypes" ("typeID" ASC);
