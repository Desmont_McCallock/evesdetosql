﻿-- ----------------------------
-- Table structure for table "eveIcons"
-- ----------------------------

DROP TABLE IF EXISTS "public"."eveIcons";

CREATE TABLE "public"."eveIcons"(
	"iconID" int NOT NULL,
	"iconFile" varchar(500) NOT NULL DEFAULT (''),
	description varchar(8000) NOT NULL DEFAULT (''),
	PRIMARY KEY ("iconID")
);