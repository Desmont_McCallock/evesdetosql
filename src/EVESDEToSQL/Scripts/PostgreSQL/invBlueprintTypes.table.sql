﻿-- ----------------------------
-- Table structure for table "invBlueprintTypes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invBlueprintTypes";

CREATE TABLE "public"."invBlueprintTypes"(
	"blueprintTypeID" int NOT NULL,
	"parentBlueprintTypeID" int NULL,
	"productTypeID" int NULL,
	"productionTime" int NULL,
	"techLevel" smallint NULL,
	"researchProductivityTime" int NULL,
	"researchMaterialTime" int NULL,
	"researchCopyTime" int NULL,
	"researchTechTime" int NULL,
	"duplicatingTime" int NULL,
	"reverseEngineeringTime" int NULL,
	"inventionTime" int NULL,
	"productivityModifier" int NULL,
	"materialModifier" smallint NULL,
	"wasteFactor" smallint NULL,
	"maxProductionLimit" int NULL,
	PRIMARY KEY ("blueprintTypeID")
);