﻿-- ----------------------------
-- Table structure for table "invNames"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invNames";

CREATE TABLE "public"."invNames"(
	"itemID" bigint NOT NULL,
	"itemName" varchar(200) NOT NULL,
	PRIMARY KEY ("itemID")
);