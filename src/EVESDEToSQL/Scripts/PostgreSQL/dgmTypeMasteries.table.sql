﻿-- ----------------------------
-- Table structure for table "dgmTypeMasteries"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmTypeMasteries";

CREATE TABLE "public"."dgmTypeMasteries"(
	"typeID" int NOT NULL,
	"masteryID" smallint NOT NULL,
	PRIMARY KEY ("typeID", "masteryID")
);