﻿-- ----------------------------
-- Table structure for table "dgmTraits"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmTraits";

CREATE TABLE "public"."dgmTraits"(
	"traitID" int NOT NULL,
	"bonusText" varchar(500) NOT NULL,
	"unitID" smallint NULL,
	PRIMARY KEY ("traitID")
);