﻿-- ----------------------------
-- Table structure for table "dgmAttributeCategories"
-- ----------------------------

DROP TABLE IF EXISTS "public"."dgmAttributeCategories";

CREATE TABLE "public"."dgmAttributeCategories"(
	"categoryID" smallint NOT NULL,
	"categoryName" varchar(50) NULL,
	"categoryDescription" varchar(200) NULL,
	PRIMARY KEY ("categoryID")
);