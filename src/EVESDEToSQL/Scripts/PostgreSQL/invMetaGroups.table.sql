﻿-- ----------------------------
-- Table structure for table "invMetaGroups"
-- ----------------------------

DROP TABLE IF EXISTS "public"."invMetaGroups";

CREATE TABLE "public"."invMetaGroups"(
	"metaGroupID" smallint NOT NULL,
	"metaGroupName" varchar(100) NULL,
	description varchar(1000) NULL,
	"iconID" int NULL,
	PRIMARY KEY ("metaGroupID")
);