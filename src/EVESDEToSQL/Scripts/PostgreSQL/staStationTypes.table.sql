﻿-- ----------------------------
-- Table structure for table "staStationTypes"
-- ----------------------------

DROP TABLE IF EXISTS "public"."staStationTypes";

CREATE TABLE "public"."staStationTypes"(
	"stationTypeID" int NOT NULL,
	"dockEntryX" double precision NULL,
	"dockEntryY" double precision NULL,
	"dockEntryZ" double precision NULL,
	"dockOrientationX" double precision NULL,
	"dockOrientationY" double precision NULL,
	"dockOrientationZ" double precision NULL,
	"operationID" smallint NULL,
	"officeSlots" smallint NULL,
	"reprocessingEfficiency" double precision NULL,
	conquerable bool NULL,
	PRIMARY KEY ("stationTypeID")
);