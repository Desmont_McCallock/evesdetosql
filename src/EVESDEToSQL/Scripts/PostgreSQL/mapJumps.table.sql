﻿-- ----------------------------
-- Table structure for table "mapJumps"
-- ----------------------------

DROP TABLE IF EXISTS "public"."mapJumps";

CREATE TABLE "public"."mapJumps"(
	"stargateID" int NOT NULL,
	"celestialID" int NULL,
	PRIMARY KEY ("stargateID")
);