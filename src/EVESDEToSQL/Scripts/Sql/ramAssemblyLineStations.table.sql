﻿IF OBJECT_ID('dbo.ramAssemblyLineStations', 'U') IS NOT NULL
DROP TABLE [dbo].[ramAssemblyLineStations]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[ramAssemblyLineStations](
	[stationID] [int] NOT NULL,
	[assemblyLineTypeID] [tinyint] NOT NULL,
	[quantity] [tinyint] NULL,
	[stationTypeID] [int] NULL,
	[ownerID] [int] NULL,
	[solarSystemID] [int] NULL,
	[regionID] [int] NULL,
 CONSTRAINT [ramAssemblyLineStations_PK] PRIMARY KEY CLUSTERED 
(
	[stationID] ASC,
	[assemblyLineTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [ramAssemblyLineStations_IX_owner] ON [dbo].[ramAssemblyLineStations]
(
	[ownerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

CREATE NONCLUSTERED INDEX [ramAssemblyLineStations_IX_region] ON [dbo].[ramAssemblyLineStations]
(
	[regionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)