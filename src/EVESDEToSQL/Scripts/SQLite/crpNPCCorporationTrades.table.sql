﻿DROP TABLE IF EXISTS sqlite_default_schema.crpNPCCorporationTrades;

CREATE TABLE sqlite_default_schema.crpNPCCorporationTrades(
	corporationID int NOT NULL,
	typeID int NOT NULL,
	PRIMARY KEY (corporationID ASC, typeID ASC)
);