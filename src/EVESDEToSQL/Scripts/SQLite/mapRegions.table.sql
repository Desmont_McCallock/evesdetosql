﻿DROP TABLE IF EXISTS sqlite_default_schema.mapRegions;

CREATE TABLE sqlite_default_schema.mapRegions(
	regionID int NOT NULL,
	regionName nvarchar(100) NULL,
	x float NULL,
	y float NULL,
	z float NULL,
	xMin float NULL,
	xMax float NULL,
	yMin float NULL,
	yMax float NULL,
	zMin float NULL,
	zMax float NULL,
	factionID int NULL,
	radius float NULL,
	PRIMARY KEY (regionID ASC)
);