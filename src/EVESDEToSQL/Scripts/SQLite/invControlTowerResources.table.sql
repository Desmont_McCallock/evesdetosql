﻿DROP TABLE IF EXISTS sqlite_default_schema.invControlTowerResources;

CREATE TABLE sqlite_default_schema.invControlTowerResources(
	controlTowerTypeID int NOT NULL,
	resourceTypeID int NOT NULL,
	purpose tinyint NULL,
	quantity int NULL,
	minSecurityLevel float NULL,
	factionID int NULL,
	PRIMARY KEY (controlTowerTypeID ASC, resourceTypeID ASC)
);