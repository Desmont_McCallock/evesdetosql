﻿DROP TABLE IF EXISTS sqlite_default_schema.invUniqueNames;

CREATE TABLE sqlite_default_schema.invUniqueNames(
	itemID int NOT NULL,
	itemName nvarchar(200) NOT NULL,
	groupID int NULL,
	PRIMARY KEY (itemID ASC)
);

CREATE INDEX invUniqueNames_IX_GroupName ON sqlite_default_schema.invUniqueNames (groupID ASC, itemName ASC);

CREATE INDEX invUniqueNames_UQ ON sqlite_default_schema.invUniqueNames (itemName ASC);