﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmMasteries;

CREATE TABLE sqlite_default_schema.dgmMasteries(
	masteryID int NOT NULL,
	certificateID int NOT NULL,
	grade tinyint NOT NULL,
	PRIMARY KEY (masteryID)
);

CREATE UNIQUE INDEX dgmMasteries_UC_certificate_grade ON sqlite_default_schema.dgmMasteries (certificateID ASC, grade ASC);

CREATE INDEX dgmMasteries_IX_certificate ON sqlite_default_schema.dgmMasteries (certificateID ASC);