﻿DROP TABLE IF EXISTS sqlite_default_schema.crpNPCDivisions;

CREATE TABLE sqlite_default_schema.crpNPCDivisions(
	divisionID tinyint NOT NULL,
	divisionName nvarchar(100) NULL,
	description nvarchar(1000) NULL,
	leaderType nvarchar(100) NULL,
	PRIMARY KEY (divisionID ASC)
);