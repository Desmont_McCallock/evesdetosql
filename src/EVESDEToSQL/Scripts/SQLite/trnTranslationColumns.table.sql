﻿DROP TABLE IF EXISTS sqlite_default_schema.trnTranslationColumns;

CREATE TABLE sqlite_default_schema.trnTranslationColumns(
	tcGroupID smallint NULL,
	tcID smallint NOT NULL,
	tableName nvarchar(256) NOT NULL,
	columnName nvarchar(128) NOT NULL,
	masterID nvarchar(128) NULL,
	PRIMARY KEY (tcID ASC)
);