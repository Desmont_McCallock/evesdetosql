﻿DROP TABLE IF EXISTS sqlite_default_schema.chrAncestries;

CREATE TABLE sqlite_default_schema.chrAncestries(
	ancestryID tinyint NOT NULL,
	ancestryName nvarchar(100) NULL,
	bloodlineID tinyint NULL,
	description nvarchar(1000) NULL,
	perception tinyint NULL,
	willpower tinyint NULL,
	charisma tinyint NULL,
	memory tinyint NULL,
	intelligence tinyint NULL,
	iconID int NULL,
	shortDescription nvarchar(500) NULL,
	PRIMARY KEY (ancestryID ASC)
);