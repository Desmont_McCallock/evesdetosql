﻿DROP TABLE IF EXISTS sqlite_default_schema.staServices;

CREATE TABLE sqlite_default_schema.staServices(
	serviceID int NOT NULL,
	serviceName nvarchar(100) NULL,
	description nvarchar(1000) NULL,
	PRIMARY KEY (serviceID ASC)
);