﻿DROP TABLE IF EXISTS sqlite_default_schema.chrFactions;

CREATE TABLE sqlite_default_schema.chrFactions(
	factionID int NOT NULL,
	factionName varchar(100) NULL,
	description varchar(1000) NULL,
	raceIDs int NULL,
	solarSystemID int NULL,
	corporationID int NULL,
	sizeFactor float NULL,
	stationCount smallint NULL,
	stationSystemCount smallint NULL,
	militiaCorporationID int NULL,
	iconID int NULL,
    PRIMARY KEY (factionID ASC)
);