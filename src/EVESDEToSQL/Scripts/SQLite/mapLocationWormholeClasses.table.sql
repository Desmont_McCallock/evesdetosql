﻿DROP TABLE IF EXISTS sqlite_default_schema.mapLocationWormholeClasses;

CREATE TABLE sqlite_default_schema.mapLocationWormholeClasses(
	locationID int NOT NULL,
	wormholeClassID tinyint NULL,
	PRIMARY KEY (locationID ASC)
);