﻿DROP TABLE IF EXISTS sqlite_default_schema.sknMaterials;

CREATE TABLE sqlite_default_schema.sknMaterials(
	skinMaterialID int NOT NULL,
	materialSetID int NOT NULL DEFAULT 0,
	displayNameID int NOT NULL DEFAULT 0,
	-- Obsolete columns since Galatea 1.0
	material nvarchar(255) NOT NULL DEFAULT '',
	colorHull nvarchar(6) NULL,
	colorWindow nvarchar(6) NULL,
	colorPrimary nvarchar(6) NULL,
	colorSecondary nvarchar(6) NULL,
	PRIMARY KEY (skinMaterialID ASC)
);