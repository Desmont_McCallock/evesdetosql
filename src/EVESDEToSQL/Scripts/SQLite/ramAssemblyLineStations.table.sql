﻿DROP TABLE IF EXISTS sqlite_default_schema.ramAssemblyLineStations;

CREATE TABLE sqlite_default_schema.ramAssemblyLineStations(
	stationID int NOT NULL,
	assemblyLineTypeID tinyint NOT NULL,
	quantity tinyint NULL,
	stationTypeID int NULL,
	ownerID int NULL,
	solarSystemID int NULL,
	regionID int NULL,
	PRIMARY KEY (stationID ASC, assemblyLineTypeID ASC)
);

CREATE INDEX ramAssemblyLineStations_IX_owner ON sqlite_default_schema.ramAssemblyLineStations (ownerID ASC);

CREATE INDEX ramAssemblyLineStations_IX_region ON sqlite_default_schema.ramAssemblyLineStations (regionID ASC);