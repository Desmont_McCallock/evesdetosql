﻿DROP TABLE IF EXISTS sqlite_default_schema.invTypeReactions;

CREATE TABLE sqlite_default_schema.invTypeReactions(
	reactionTypeID int NOT NULL,
	input bit NOT NULL,
	typeID int NOT NULL,
	quantity smallint NULL,
	PRIMARY KEY (reactionTypeID ASC, input ASC,	typeID ASC)
);