﻿DROP TABLE IF EXISTS sqlite_default_schema.mapUniverse;

CREATE TABLE sqlite_default_schema.mapUniverse(
	universeID int NOT NULL,
	universeName varchar(100) NULL,
	x float NULL,
	y float NULL,
	z float NULL,
	xMin float NULL,
	xMax float NULL,
	yMin float NULL,
	yMax float NULL,
	zMin float NULL,
	zMax float NULL,
	radius float NULL,
	PRIMARY KEY (universeID ASC)
);