﻿DROP TABLE IF EXISTS sqlite_default_schema.invCategories;

CREATE TABLE sqlite_default_schema.invCategories(
	categoryID int NOT NULL,
	categoryName nvarchar(100) NULL,
	description nvarchar(3000) NULL,
	iconID int NULL,
	published bit NULL,
	PRIMARY KEY (categoryID ASC)
);