﻿DROP TABLE IF EXISTS sqlite_default_schema.staOperationServices;

CREATE TABLE sqlite_default_schema.staOperationServices(
	operationID tinyint NOT NULL,
	serviceID int NOT NULL,
	PRIMARY KEY (operationID ASC, serviceID ASC)
);