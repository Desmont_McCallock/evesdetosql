﻿DROP TABLE IF EXISTS sqlite_default_schema.staOperations;

CREATE TABLE sqlite_default_schema.staOperations(
	activityID tinyint NULL,
	operationID tinyint NOT NULL,
	operationName nvarchar(100) NULL,
	description nvarchar(1000) NULL,
	fringe tinyint NULL,
	corridor tinyint NULL,
	hub tinyint NULL,
	border tinyint NULL,
	ratio tinyint NULL,
	caldariStationTypeID int NULL,
	minmatarStationTypeID int NULL,
	amarrStationTypeID int NULL,
	gallenteStationTypeID int NULL,
	joveStationTypeID int NULL,
	PRIMARY KEY (operationID ASC)
);