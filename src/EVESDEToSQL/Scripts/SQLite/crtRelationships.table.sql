DROP TABLE IF EXISTS sqlite_default_schema.crtRelationships;

CREATE TABLE sqlite_default_schema.crtRelationships(
	relationshipID int NOT NULL,
	parentID int NULL,
	parentTypeID int NULL,
	parentLevel tinyint NULL,
	childID int NULL,
	grade tinyint NULL,
	PRIMARY KEY (relationshipID ASC)
);

CREATE INDEX crtRelationships_IX_child ON sqlite_default_schema.crtRelationships (childID ASC);

CREATE INDEX crtRelationships_IX_parent ON sqlite_default_schema.crtRelationships (parentID ASC);