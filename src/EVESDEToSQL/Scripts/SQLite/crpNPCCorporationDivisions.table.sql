﻿DROP TABLE IF EXISTS sqlite_default_schema.crpNPCCorporationDivisions;

CREATE TABLE sqlite_default_schema.crpNPCCorporationDivisions(
	corporationID int NOT NULL,
	divisionID tinyint NOT NULL,
	size tinyint NULL,
	PRIMARY KEY (corporationID ASC, divisionID ASC)
);