﻿DROP TABLE IF EXISTS sqlite_default_schema.mapConstellationJumps;

CREATE TABLE sqlite_default_schema.mapConstellationJumps(
	fromRegionID int NULL,
	fromConstellationID int NOT NULL,
	toConstellationID int NOT NULL,
	toRegionID int NULL,
	PRIMARY KEY (fromConstellationID ASC, toConstellationID ASC)
);

CREATE INDEX mapConstellationJumps_IX_fromRegion ON sqlite_default_schema.mapConstellationJumps (fromRegionID ASC);