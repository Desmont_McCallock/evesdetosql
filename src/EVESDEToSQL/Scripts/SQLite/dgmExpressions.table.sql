﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmExpressions;

CREATE TABLE sqlite_default_schema.dgmExpressions(
	expressionID int NOT NULL,
	operandID int NULL,
	arg1 int NULL,
	arg2 int NULL,
	expressionValue varchar(100) NULL,
	description varchar(1000) NULL,
	expressionName varchar(500) NULL,
	expressionTypeID int NULL,
	expressionGroupID smallint NULL,
	expressionAttributeID smallint NULL,
	PRIMARY KEY (expressionID ASC)
);