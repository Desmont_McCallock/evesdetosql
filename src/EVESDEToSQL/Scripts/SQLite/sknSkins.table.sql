﻿DROP TABLE IF EXISTS sqlite_default_schema.sknSkins;

CREATE TABLE sqlite_default_schema.sknSkins(
	skinID int NOT NULL,
	internalName nvarchar(100) NOT NULL DEFAULT '',
	skinMaterialID int NULL,
	typeID int NULL,
	allowCCPDevs bit NOT NULL DEFAULT 0,
	visibleSerenity bit NOT NULL DEFAULT 0,
	visibleTranquility bit NOT NULL DEFAULT 0,
	PRIMARY KEY (skinID ASC)
);

CREATE INDEX sknSkins_IX_skinMaterial ON sqlite_default_schema.sknSkins (skinMaterialID ASC);