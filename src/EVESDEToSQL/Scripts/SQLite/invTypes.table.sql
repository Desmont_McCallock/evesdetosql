﻿DROP TABLE IF EXISTS sqlite_default_schema.invTypes;

CREATE TABLE sqlite_default_schema.invTypes(
	typeID int NOT NULL,
	groupID int NULL,
	typeName nvarchar(100) NULL,
	description nvarchar(3000) NULL,
	mass float NULL,
	volume float NULL,
	capacity float NULL,
	portionSize int NULL,
	raceID tinyint NULL,
	basePrice money NULL,
	published bit NULL,
	marketGroupID int NULL,
	chanceOfDuplicating float NULL,
	factionID int NULL,
	graphicID int NULL,
	iconID int NULL,
	radius float NULL,
	soundID int NULL,
	PRIMARY KEY (typeID ASC)
);

CREATE INDEX invTypes_IX_Group ON sqlite_default_schema.invTypes (groupID ASC);