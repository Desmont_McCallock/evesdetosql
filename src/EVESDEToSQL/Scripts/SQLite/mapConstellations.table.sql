﻿DROP TABLE IF EXISTS sqlite_default_schema.mapConstellations;

CREATE TABLE sqlite_default_schema.mapConstellations(
	regionID int NULL,
	constellationID int NOT NULL,
	constellationName nvarchar(100) NULL,
	x float NULL,
	y float NULL,
	z float NULL,
	xMin float NULL,
	xMax float NULL,
	yMin float NULL,
	yMax float NULL,
	zMin float NULL,
	zMax float NULL,
	factionID int NULL,
	radius float NULL,
	PRIMARY KEY (constellationID ASC)
);

CREATE INDEX mapConstellations_IX_region ON sqlite_default_schema.mapConstellations (regionID ASC);