﻿DROP TABLE IF EXISTS sqlite_default_schema.sknLicenses;

CREATE TABLE sqlite_default_schema.sknLicenses(
	licenseTypeID int NOT NULL,
	skinID int NOT NULL,
	duration int NOT NULL DEFAULT -1,
	PRIMARY KEY (licenseTypeID ASC)
);

CREATE INDEX sknLicenses_IX_skin ON sqlite_default_schema.sknLicenses (skinID ASC);