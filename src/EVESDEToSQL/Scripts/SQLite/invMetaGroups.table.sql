﻿DROP TABLE IF EXISTS sqlite_default_schema.invMetaGroups;

CREATE TABLE sqlite_default_schema.invMetaGroups(
	metaGroupID smallint NOT NULL,
	metaGroupName nvarchar(100) NULL,
	description nvarchar(1000) NULL,
	iconID int NULL,
	PRIMARY KEY (metaGroupID ASC)
);