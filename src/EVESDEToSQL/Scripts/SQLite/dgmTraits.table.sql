﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmTraits;

CREATE TABLE sqlite_default_schema.dgmTraits(
	traitID int NOT NULL,
	bonusText nvarchar(500) NOT NULL,
	unitID tinyint NULL,
	PRIMARY KEY (traitID ASC)
);