DROP TABLE IF EXISTS sqlite_default_schema.crtCertificates;

CREATE TABLE sqlite_default_schema.crtCertificates(
	certificateID int NOT NULL,
	groupID smallint NULL,
	classID int NULL,
	grade tinyint NULL,
	corpID int NULL,
	iconID int NULL,
	description nvarchar(500) NULL,
	PRIMARY KEY (certificateID ASC)
);

CREATE INDEX crtCertificates_IX_group ON sqlite_default_schema.crtCertificates (groupID ASC);

CREATE INDEX crtCertificates_IX_class ON sqlite_default_schema.crtCertificates (classID ASC);