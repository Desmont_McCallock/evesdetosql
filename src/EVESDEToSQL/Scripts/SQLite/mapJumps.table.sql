﻿DROP TABLE IF EXISTS sqlite_default_schema.mapJumps;

CREATE TABLE sqlite_default_schema.mapJumps(
	stargateID int NOT NULL,
	celestialID int NULL,
	PRIMARY KEY (stargateID ASC)
);