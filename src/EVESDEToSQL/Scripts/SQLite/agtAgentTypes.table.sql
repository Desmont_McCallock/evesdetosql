﻿DROP TABLE IF EXISTS sqlite_default_schema.agtAgentTypes;

CREATE TABLE sqlite_default_schema.agtAgentTypes(
	agentTypeID int NOT NULL,
	agentType varchar(50) NULL,
	PRIMARY KEY (agentTypeID ASC)
);