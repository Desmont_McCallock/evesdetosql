﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmTypeTraits;

CREATE TABLE sqlite_default_schema.dgmTypeTraits(
	typeID int NOT NULL,
	parentTypeID int NOT NULL,
	traitID int NOT NULL,
	bonus float NULL,
	PRIMARY KEY (typeID ASC, parentTypeID ASC, traitID ASC)
);