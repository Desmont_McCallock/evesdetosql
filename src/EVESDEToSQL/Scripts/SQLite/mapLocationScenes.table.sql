﻿DROP TABLE IF EXISTS sqlite_default_schema.mapLocationScenes;

CREATE TABLE sqlite_default_schema.mapLocationScenes(
	locationID int NOT NULL,
	graphicID int NULL,
	PRIMARY KEY (locationID ASC)
);