﻿DROP TABLE IF EXISTS sqlite_default_schema.invContrabandTypes;

CREATE TABLE sqlite_default_schema.invContrabandTypes(
	factionID int NOT NULL,
	typeID int NOT NULL,
	standingLoss float NULL,
	confiscateMinSec float NULL,
	fineByValue float NULL,
	attackMinSec float NULL,
	PRIMARY KEY (factionID ASC, typeID ASC)
);

CREATE INDEX invContrabandTypes_IX_type ON sqlite_default_schema.invContrabandTypes (typeID ASC);