﻿DROP TABLE IF EXISTS sqlite_default_schema.agtAgents;

CREATE TABLE sqlite_default_schema.agtAgents(
	agentID int NOT NULL,
	divisionID tinyint NULL,
	corporationID int NULL,
	locationID int NULL,
	level tinyint NULL,
	quality smallint NULL,
	agentTypeID int NULL,
	isLocator bit NULL,
	PRIMARY KEY (agentID ASC)
);

CREATE INDEX agtAgents_IX_corporation ON sqlite_default_schema.agtAgents (corporationID ASC);

CREATE INDEX agtAgents_IX_station ON sqlite_default_schema.agtAgents (locationID ASC);