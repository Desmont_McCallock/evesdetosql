﻿DROP TABLE IF EXISTS sqlite_default_schema.eveUnits;

CREATE TABLE sqlite_default_schema.eveUnits(
	unitID tinyint NOT NULL,
	unitName varchar(100) NULL,
	displayName varchar(50) NULL,
	description varchar(1000) NULL,
	PRIMARY KEY (unitID ASC)
);