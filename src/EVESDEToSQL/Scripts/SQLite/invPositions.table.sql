﻿DROP TABLE IF EXISTS sqlite_default_schema.invPositions;

CREATE TABLE sqlite_default_schema.invPositions(
	itemID bigint NOT NULL,
	x float NOT NULL DEFAULT 0.0,
	y float NOT NULL DEFAULT 0.0,
	z float NOT NULL DEFAULT 0.0,
	yaw real NULL,
	pitch real NULL,
	roll real NULL,
	PRIMARY KEY (itemID ASC)
);