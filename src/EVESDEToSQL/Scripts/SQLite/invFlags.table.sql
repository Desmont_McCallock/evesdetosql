﻿DROP TABLE IF EXISTS sqlite_default_schema.invFlags;

CREATE TABLE sqlite_default_schema.invFlags(
	flagID smallint NOT NULL,
	flagName varchar(200) NULL,
	flagText varchar(100) NULL,
	orderID int NULL,
	PRIMARY KEY (flagID ASC)
);