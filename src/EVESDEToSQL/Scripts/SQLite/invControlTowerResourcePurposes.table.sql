﻿DROP TABLE IF EXISTS sqlite_default_schema.invControlTowerResourcePurposes;

CREATE TABLE sqlite_default_schema.invControlTowerResourcePurposes(
	purpose tinyint NOT NULL,
	purposeText varchar(100) NULL,
	PRIMARY KEY (purpose ASC)
);