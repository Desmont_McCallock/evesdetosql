﻿DROP TABLE IF EXISTS sqlite_default_schema.trnTranslations;

CREATE TABLE sqlite_default_schema.trnTranslations(
	tcID smallint NOT NULL,
	keyID int NOT NULL,
	languageID varchar(50) NOT NULL,
	text nvarchar(8000) NOT NULL,
	PRIMARY KEY (tcID ASC, keyID ASC, languageID ASC)
);