﻿DROP TABLE IF EXISTS sqlite_default_schema.ramAssemblyLineTypeDetailPerCategory;

CREATE TABLE sqlite_default_schema.ramAssemblyLineTypeDetailPerCategory(
	assemblyLineTypeID tinyint NOT NULL,
	categoryID int NOT NULL,
	timeMultiplier float NULL,
	materialMultiplier float NULL,
	costMultiplier float NULL,
	PRIMARY KEY (assemblyLineTypeID ASC, categoryID ASC)
);