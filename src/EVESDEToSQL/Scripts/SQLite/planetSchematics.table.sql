﻿DROP TABLE IF EXISTS sqlite_default_schema.planetSchematics;

CREATE TABLE sqlite_default_schema.planetSchematics(
	schematicID smallint NOT NULL,
	schematicName nvarchar(255) NULL,
	cycleTime int NULL,
	PRIMARY KEY (schematicID ASC)
);