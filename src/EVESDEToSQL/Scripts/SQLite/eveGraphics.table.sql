﻿DROP TABLE IF EXISTS sqlite_default_schema.eveGraphics;

CREATE TABLE sqlite_default_schema.eveGraphics(
	graphicID int NOT NULL,
	graphicFile varchar(500) NOT NULL DEFAULT '',
	description nvarchar(8000) NOT NULL DEFAULT '',
	obsolete bit NOT NULL DEFAULT 0,
	graphicType varchar(100) NULL,
	collidable bit NULL,
	directoryID int NULL,
	graphicName nvarchar(64) NOT NULL DEFAULT '',
	gfxRaceID varchar(255) NULL,
	colorScheme varchar(255) NULL,
	sofHullName varchar(64) NULL,
	PRIMARY KEY (graphicID ASC)
);