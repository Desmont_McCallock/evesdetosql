﻿DROP TABLE IF EXISTS sqlite_default_schema.invMetaTypes;

CREATE TABLE sqlite_default_schema.invMetaTypes(
	typeID int NOT NULL,
	parentTypeID int NULL,
	metaGroupID smallint NULL,
	PRIMARY KEY (typeID ASC)
);