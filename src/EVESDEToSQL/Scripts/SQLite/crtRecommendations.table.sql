DROP TABLE IF EXISTS sqlite_default_schema.crtRecommendations;

CREATE TABLE sqlite_default_schema.crtRecommendations(
	recommendationID int NOT NULL,
	shipTypeID int NULL,
	certificateID int NULL,
	recommendationLevel tinyint NOT NULL,
	PRIMARY KEY (recommendationID ASC)
);

CREATE INDEX crtRecommendations_IX_certificate ON sqlite_default_schema.crtRecommendations (certificateID ASC);

CREATE INDEX crtRecommendations_IX_shipType ON sqlite_default_schema.crtRecommendations (shipTypeID ASC);