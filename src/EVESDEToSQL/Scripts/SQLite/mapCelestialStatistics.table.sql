﻿DROP TABLE IF EXISTS sqlite_default_schema.mapCelestialStatistics;

CREATE TABLE sqlite_default_schema.mapCelestialStatistics(
	celestialID int NOT NULL,
	temperature float NULL,
	spectralClass varchar(10) NULL,
	luminosity float NULL,
	age float NULL,
	life float NULL,
	orbitRadius float NULL,
	eccentricity float NULL,
	massDust float NULL,
	massGas float NULL,
	fragmented bit NULL,
	density float NULL,
	surfaceGravity float NULL,
	escapeVelocity float NULL,
	orbitPeriod float NULL,
	rotationRate float NULL,
	locked bit NULL,
	pressure float NULL,
	radius float NULL,
	mass float NULL,
	PRIMARY KEY (celestialID ASC)
);