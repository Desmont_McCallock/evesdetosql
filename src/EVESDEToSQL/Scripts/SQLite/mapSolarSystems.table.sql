﻿DROP TABLE IF EXISTS sqlite_default_schema.mapSolarSystems;

CREATE TABLE sqlite_default_schema.mapSolarSystems(
	regionID int NULL,
	constellationID int NULL,
	solarSystemID int NOT NULL,
	solarSystemName nvarchar(100) NULL,
	x float NULL,
	y float NULL,
	z float NULL,
	xMin float NULL,
	xMax float NULL,
	yMin float NULL,
	yMax float NULL,
	zMin float NULL,
	zMax float NULL,
	luminosity float NULL,
	border bit NULL,
	fringe bit NULL,
	corridor bit NULL,
	hub bit NULL,
	international bit NULL,
	regional bit NULL,
	constellation bit NULL,
	security float NULL,
	factionID int NULL,
	radius float NULL,
	sunTypeID int NULL,
	securityClass varchar(2) NULL,
	PRIMARY KEY (solarSystemID ASC)
);

CREATE INDEX mapSolarSystems_IX_constellation ON sqlite_default_schema.mapSolarSystems (constellationID ASC);

CREATE INDEX mapSolarSystems_IX_region ON sqlite_default_schema.mapSolarSystems (regionID ASC);

CREATE INDEX mapSolarSystems_IX_security ON sqlite_default_schema.mapSolarSystems (security ASC);