﻿DROP TABLE IF EXISTS sqlite_default_schema.mapRegionJumps;

CREATE TABLE sqlite_default_schema.mapRegionJumps(
	fromRegionID int NOT NULL,
	toRegionID int NOT NULL,
	PRIMARY KEY (fromRegionID ASC, toRegionID ASC)
);