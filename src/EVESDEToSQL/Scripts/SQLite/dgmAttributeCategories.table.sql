﻿DROP TABLE IF EXISTS sqlite_default_schema.dgmAttributeCategories;

CREATE TABLE sqlite_default_schema.dgmAttributeCategories(
	categoryID tinyint NOT NULL,
	categoryName nvarchar(50) NULL,
	categoryDescription nvarchar(200) NULL,
	PRIMARY KEY (categoryID ASC)
);