﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using ADOX;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using JRO;
using Microsoft.SqlServer.Management.Smo;
using Table = Microsoft.SqlServer.Management.Smo.Table;

namespace EVESDEToSQL.Exporters.SQLToAccess
{
    internal sealed class AccessExporter : IExporter
    {
        private readonly OleDbConnectionProvider m_oleDbConnectionProvider;
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccessExporter" /> class.
        /// </summary>
        /// <exception cref="ArgumentNullException">oleDbConnectionProvider</exception>
        internal AccessExporter(DbConnectionProvider oleDbConnectionProvider, DbConnectionProvider sqlConnectionProvider)
        {
            if (oleDbConnectionProvider == null)
                throw new ArgumentNullException(nameof(oleDbConnectionProvider));

            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            m_oleDbConnectionProvider = (OleDbConnectionProvider)oleDbConnectionProvider;
            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        public List<string> TablesToExport { get; set; }

        /// <summary>
        /// Exports the data.
        /// </summary>
        public void ExportData()
        {
            if (m_isClosing)
                return;

            string actionText =
                $"Exporting database '{m_sqlConnectionProvider.Connection.Database}' to MS Access database file... ";

            Console.WriteLine(actionText);
            Console.WriteLine();

            try
            {
                Database database = m_sqlConnectionProvider.GetDatabase();

                Dictionary<string, Table> dbTables = TablesToExport.Any()
                    ? TablesToExport.ToDictionary(key => key, value => database.Tables[value])
                    : database.Tables.Cast<Table>().ToDictionary(key => key.Name, value => value);

                foreach (KeyValuePair<string, Table> table in dbTables)
                {
                    if (table.Value == null)
                    {
                        Util.ResetConsoleCursorPosition(actionText + Util.DisplayedText);
                        Console.WriteLine($"Table '{table.Key}' does not exists!");
                        continue;
                    }

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    Util.ResetCounters();

                    actionText = $"Exporting table '{table.Key}'... ";

                    Console.Write(actionText);

                    m_oleDbConnectionProvider.DropAndCreateTable(table.Value.Name);

                    DataTable dataTable = m_sqlConnectionProvider.GetDataTable(table.Value);

                    m_oleDbConnectionProvider.ImportDataBulk(dataTable);

                    Util.DisplayEndTime(stopwatch);

                    if (database.Tables.Count % 10 == 0)
                        CompactDatabase();
                }

                CompactDatabase();

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string failText =
                    $"Exporting database '{m_sqlConnectionProvider.Connection.Database}' to MS Access database file: Failed\n{ex.Message}";

                actionText += Util.DisplayedText;

                Util.HandleException(actionText, failText);
            }
        }

        /// <summary>
        /// Compacts the database.
        /// </summary>
        private void CompactDatabase()
        {
            string sourceFile = m_oleDbConnectionProvider.Connection.DataSource;

            string path = Path.GetDirectoryName(sourceFile);
            if (path == null)
                return;

            string tempFile = Path.Combine(path, "Database.mdb");


            string destination =
                $"Provider={((OleDbConnection)m_oleDbConnectionProvider.Connection).Provider};Jet OLEDB:Engine Type=5;Data Source={tempFile}";

            m_oleDbConnectionProvider.Connection.Close();

            JetEngine engine = new JetEngine();
            engine.CompactDatabase(m_oleDbConnectionProvider.Connection.ConnectionString, destination);
            Marshal.ReleaseComObject(engine);

            File.Copy(tempFile, sourceFile, true);
            File.Delete(tempFile);

            m_oleDbConnectionProvider.Connection.Open();
        }

        /// <summary>
        /// Creates the database file.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        internal static void CreateFile(string connectionString)
        {
            Catalog catalog = new Catalog();
            catalog.Create(connectionString);
            Marshal.ReleaseComObject(catalog);
        }
    }
}
