﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL.Exporters.SQLToPostgreSQL
{
    internal sealed class PostgreSqlExporter : IExporter
    {
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        public PostgreSqlExporter(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        public List<string> TablesToExport { get; set; }

        /// <summary>
        /// Exports the data.
        /// </summary>
        public void ExportData()
        {
            if (m_isClosing)
                return;

            if (m_sqlConnectionProvider.Connection.State == ConnectionState.Closed)
                m_sqlConnectionProvider.OpenConnection();

            string actionText = $"Exporting database '{m_sqlConnectionProvider.Connection.Database}' to PostgreSQL dump file... ";

            Console.WriteLine(actionText);
            Console.WriteLine();

            string filePath =
                Path.Combine(new DirectoryInfo(Util.DataDumpExportsDirectory.FullName + "\\PostgreSQL").CreateIfAbsent(),
                    $"postgresql94-{m_sqlConnectionProvider.Connection.Database}-{DateTimeOffset.Now:yyyyMMddHHmm}.sql");

            if (File.Exists(filePath))
                File.Delete(filePath);

            try
            {
                Database database = m_sqlConnectionProvider.GetDatabase();

                File.AppendAllText(filePath, GetHeader());

                Dictionary<string, Table> dbTables = TablesToExport.Any()
                    ? TablesToExport.ToDictionary(key => key, value => database.Tables[value])
                    : database.Tables.Cast<Table>().ToDictionary(key => key.Name, value => value);

                foreach (KeyValuePair<string, Table> table in dbTables)
                {
                    if (table.Value == null)
                    {
                        Util.ResetConsoleCursorPosition(actionText + Util.DisplayedText);
                        Console.WriteLine($"Table '{table.Key}' does not exists!");
                        continue;
                    }

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    Util.ResetCounters();

                    actionText = $"Exporting table '{table.Key}'... ";

                    Console.Write(actionText);

                    DataTable dataTable = m_sqlConnectionProvider.GetDataTable(table.Value);

                    File.AppendAllText(filePath, GetTableScript(dataTable));

                    Util.DisplayEndTime(stopwatch);
                }

                File.AppendAllText(filePath, GetFooter());

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string failText =
                    $"Exporting database '{m_sqlConnectionProvider.Connection.Database}' to PostgreSQL dump file: Failed\n{ex.Message}";

                actionText += Util.DisplayedText;

                Util.HandleException(actionText, failText);
            }
        }

        /// <summary>
        /// Gets the header.
        /// </summary>
        /// <returns></returns>
        private string GetHeader()
        {
            StringBuilder sb = new StringBuilder();

            AssemblyName assemblyName = typeof(Program).Assembly.GetName();
            sb.AppendLine("-- ------------------------------------------------------")
                .AppendLine("-- PostgreSQL database dump")
                .AppendLine("--")
                .Append(
                    $"-- Dumped by {assemblyName.Name} {assemblyName.Version.Major}.{assemblyName.Version.Minor}.{assemblyName.Version.Build}")
                .AppendLine()
                .Append($"-- Dumped from database: {m_sqlConnectionProvider.Connection.Database}")
                .AppendLine()
                .AppendLine("-- ------------------------------------------------------")
                .AppendLine()
                .AppendLine(Util.GetScriptFor<PostgreSQLCommand>("header"));

            return sb.ToString();
        }

        /// <summary>
        /// Gets the footer.
        /// </summary>
        /// <returns></returns>
        private static string GetFooter()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append($"-- Completed on {DateTimeOffset.Now:yyyy-MM-dd HH:mm:ss}")
                .AppendLine();

            return sb.ToString();
        }

        /// <summary>
        /// Gets the table script.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <returns></returns>
        private static string GetTableScript(DataTable dataTable)
        {
            StringBuilder sb = new StringBuilder();

            string script = Util.GetScriptFor<PostgreSQLCommand>(dataTable.TableName);

            if (!script.EndsWith(Environment.NewLine, StringComparison.OrdinalIgnoreCase))
                script = script.Insert(script.Length, Environment.NewLine);

            sb.AppendLine(script)
                .AppendLine();

            GetInsertCommand(dataTable, sb);

            sb.AppendLine();

            return sb.ToString();
        }

        /// <summary>
        /// Gets the insert command.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="sb">The sb.</param>
        private static void GetInsertCommand(DataTable dataTable, StringBuilder sb)
        {
            List<DataRow> rows = dataTable.Rows.Cast<DataRow>().ToList();
            double rowCount = rows.Count * 2;
            int storedCapacity = 0;

            const int MaxCapacity = 1024 * 1024;
            const string InsertCommand = "INSERT INTO \"{0}\" VALUES ";

            sb.AppendFormat(InsertCommand, dataTable.TableName);

            foreach (DataRow row in dataTable.Rows.Cast<DataRow>())
            {
                Util.UpdatePercentDone(rowCount);

                // We need to split commands in case data exceeds mysql max package size
                if (sb.Capacity - storedCapacity > MaxCapacity)
                {
                    storedCapacity = sb.Capacity;

                    // We have reached the max package size
                    // Remove trailing comma and replace with semi-colon
                    sb.Remove(sb.Length - 1, 1).Append(";");

                    sb.AppendLine()
                        .AppendFormat(InsertCommand, dataTable.TableName);
                }

                sb.Append("(");
                foreach (object item in row.ItemArray)
                {
                    if (item.GetType().UnderlyingSystemType == typeof(string))
                        sb.Append($"'{item.ToString().Replace("\'", "\'\'")}'");
                    else
                        sb.Append(item is DBNull ? SqlString.Null : item);

                    sb.Append(",");
                }

                // Unorthodox way to remove trailing comma,
                // Array.IndexOf() or Last() comparison is not a viable solution
                // because in case any item in the row equals with the last one
                // it will give false result
                sb.Remove(sb.Length - 1, 1);

                sb.Append(")")
                    .Append(rows.Last() != row ? "," : ";");
            }

            sb.AppendLine();
        }

        private sealed class PostgreSQLCommand : DbCommand
        {
            public override void Prepare()
            {
                throw new NotImplementedException();
            }

            public override string CommandText { get; set; }

            public override int CommandTimeout { get; set; }

            public override CommandType CommandType { get; set; }

            public override UpdateRowSource UpdatedRowSource { get; set; }

            protected override DbConnection DbConnection { get; set; }

            protected override DbParameterCollection DbParameterCollection { get; }

            protected override DbTransaction DbTransaction { get; set; }

            public override bool DesignTimeVisible { get; set; }

            public override void Cancel()
            {
                throw new NotImplementedException();
            }

            protected override DbParameter CreateDbParameter()
            {
                throw new NotImplementedException();
            }

            protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior)
            {
                throw new NotImplementedException();
            }

            public override int ExecuteNonQuery()
            {
                throw new NotImplementedException();
            }

            public override object ExecuteScalar()
            {
                throw new NotImplementedException();
            }
        }
    }
}
