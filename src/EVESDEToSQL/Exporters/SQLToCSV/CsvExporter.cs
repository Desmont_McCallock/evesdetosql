﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL.Exporters.SQLToCSV
{
    internal sealed class CsvExporter : IExporter
    {
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        public CsvExporter(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets or sets the tables to export.
        /// </summary>
        /// <value>
        /// The tables to export.
        /// </value>
        public List<string> TablesToExport { get; set; }

        /// <summary>
        /// Exports the data.
        /// </summary>
        public void ExportData()
        {
            if (m_isClosing)
                return;

            if (m_sqlConnectionProvider.Connection.State == ConnectionState.Closed)
                m_sqlConnectionProvider.OpenConnection();

            string actionText = $"Exporting database '{m_sqlConnectionProvider.Connection.Database}' to CSV files... ";

            Console.WriteLine(actionText);
            Console.WriteLine();

            try
            {
                Database database = m_sqlConnectionProvider.GetDatabase();

                Dictionary<string, Table> dbTables = TablesToExport.Any()
                    ? TablesToExport.ToDictionary(key => key, value => database.Tables[value])
                    : database.Tables.Cast<Table>().ToDictionary(key => key.Name, value => value);

                foreach (KeyValuePair<string, Table> table in dbTables)
                {
                    if (table.Value == null)
                    {
                        Util.ResetConsoleCursorPosition(actionText + Util.DisplayedText);
                        Console.WriteLine($"Table '{table.Key}' does not exists!");
                        continue;
                    }

                    Stopwatch stopwatch = Stopwatch.StartNew();
                    Util.ResetCounters();

                    actionText = $"Exporting table '{table.Key}'... ";

                    Console.Write(actionText);

                    DataTable dataTable = m_sqlConnectionProvider.GetDataTable(table.Value);

                    ExportToCsv(dataTable);

                    Util.DisplayEndTime(stopwatch);
                }

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string failText =
                    $"Exporting database '{m_sqlConnectionProvider.Connection.Database}' to SQLite database file: Failed\n{ex.Message}";

                actionText += Util.DisplayedText;

                Util.HandleException(actionText, failText);
            }
        }

        /// <summary>
        /// Exports the data table to a CSV file.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        private static void ExportToCsv(DataTable dataTable)
        {
            StringBuilder sb = new StringBuilder();

            double rowCount = dataTable.Rows.Count * 2;

            foreach (DataColumn column in dataTable.Columns.Cast<DataColumn>())
            {
                sb.Append($"{column.ColumnName};");
            }
            sb.AppendLine();

            foreach (DataRow row in dataTable.Rows.Cast<DataRow>())
            {
                Util.UpdatePercentDone(rowCount);

                foreach (object item in row.ItemArray)
                {
                    if (item is string)
                        sb.Append($"\"{item}\";");
                    else
                        sb.Append($"{item};");
                }
                sb.AppendLine();
            }

            string path = Path.Combine(new DirectoryInfo(Util.DataDumpExportsDirectory.FullName + "\\CSV").CreateIfAbsent(),
                $"{dataTable.TableName}.csv");

            File.WriteAllText(path, sb.ToString());
        }
    }
}
