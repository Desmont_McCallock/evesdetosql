﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;

namespace EVESDEToSQL.Importers.SQLiteToSQL
{
    internal class SqliteImporter : IImporter
    {
        private readonly SqliteConnectionProvider m_sqliteConnectionProvider;
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqliteImporter" /> class.
        /// </summary>
        /// <param name="sqliteConnectionProvider">The sqlite provider.</param>
        /// <param name="sqlConnectionProvider">The SQL provider.</param>
        /// <exception cref="System.ArgumentNullException">sqliteConnectionProvider
        /// or
        /// sqlConnectionProvider</exception>
        internal SqliteImporter(DbConnectionProvider sqliteConnectionProvider, DbConnectionProvider sqlConnectionProvider)
        {
            if (sqliteConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqliteConnectionProvider));

            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            m_sqliteConnectionProvider = (SqliteConnectionProvider)sqliteConnectionProvider;
            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Imports the files.
        /// </summary>
        public void ImportData()
        {
            if (m_isClosing)
                return;

            if (m_sqlConnectionProvider.Connection == null || m_sqliteConnectionProvider.Connection == null)
                return;

            List<string> tables = m_sqliteConnectionProvider.Connection.GetSchema("Tables")
                .Rows.Cast<DataRow>()
                .Select(row => row.ItemArray[2])
                .Cast<string>()
                .OrderBy(tableName => tableName).ToList();

            if (Debugger.IsAttached)
                Import(tables.First());
            else
                tables.ForEach(Import);

            Console.WriteLine();
            m_sqliteConnectionProvider.CloseConnection();
        }

        /// <summary>
        /// Imports the specified table name.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <exception cref="System.ArgumentNullException">tableName</exception>
        private void Import(string tableName)
        {
            if (m_isClosing)
                return;

            if (string.IsNullOrWhiteSpace(tableName))
                throw new ArgumentNullException(nameof(tableName));

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            Console.Write(@"Importing {0}... ", tableName);

            m_sqlConnectionProvider.DropAndCreateTable(tableName);

            DataTable dataTable = m_sqliteConnectionProvider.GetDataTable(tableName);

            m_sqlConnectionProvider.ImportDataBulk(dataTable);

            Util.UpdatePercentDone(dataTable.Rows.Count * 2);

            Util.DisplayEndTime(stopwatch);
        }
    }
}
