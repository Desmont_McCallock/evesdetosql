﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    internal static class Blueprints
    {
        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Blueprints"/> class.
        /// </summary>
        static Blueprints()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the blueprints.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        public static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string yamlFile = YamlFilesConstants.Blueprints;
            string filePath = Util.CheckYamlFileExists(yamlFile);

            if (string.IsNullOrEmpty(filePath))
                return;

            string text = $"Parsing {yamlFile}... ";
            Console.Write(text);
            YamlMappingNode rNode = Util.ParseYamlFile(filePath);

            if (s_isClosing)
                return;

            Util.ResetConsoleCursorPosition(text);

            if (rNode == null)
            {
                Console.WriteLine($"Unable to parse {yamlFile}.");
                return;
            }

            Console.Write($"Importing {yamlFile}... ");

            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.InvBlueprintTypesTableName);
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.RamTypeRequirementsTableName);

            ImportDataBulk(rNode);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportDataBulk(YamlMappingNode rNode)
        {
            if (s_isClosing)
                return;

            Util.UpdatePercentDone(0);

            DataTable invBlueprintTypes = GetInvBlueprintTypesDataTable();
            DataTable ramTypeRequirements = GetRamTypeRequirementsDataTable();

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            YamlNode manActivity = new YamlScalarNode(Activity.Manufacturing.GetDescription());
            YamlNode rteActivity = new YamlScalarNode(Activity.ResearchingTechnology.GetDescription());
            YamlNode rtpActivity = new YamlScalarNode(Activity.ResearchingTimeEfficiency.GetDescription());
            YamlNode rmpActivity = new YamlScalarNode(Activity.ResearchingMaterialEfficiency.GetDescription());
            YamlNode copActivity = new YamlScalarNode(Activity.Copying.GetDescription());
            YamlNode dupActivity = new YamlScalarNode(Activity.Duplicating.GetDescription());
            YamlNode renActivity = new YamlScalarNode(Activity.ReverseEngineering.GetDescription());
            YamlNode invActivity = new YamlScalarNode(Activity.Invention.GetDescription());

            foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
            {
                Util.UpdatePercentDone(rowCount);

                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                SqlInt32 blueprintTypeID = SqlInt32.Parse(pair.Key.ToString());
                YamlNode blueprintTypeIDNode = cNode.Children[new YamlScalarNode(GenericConstant.BlueprintTypeIDText)];

                if (blueprintTypeID.ToString() != blueprintTypeIDNode.ToString())
                {
                    throw new ConstraintException($"Key [{blueprintTypeID}] differs from {GenericConstant.BlueprintTypeIDText}");
                }

                SqlInt32 productTypeID = SqlInt32.Null;
                SqlInt32 productionTime = SqlInt32.Null;
                SqlInt32 researchTechTime = SqlInt32.Null;
                SqlInt32 researchProductivityTime = SqlInt32.Null;
                SqlInt32 researchMaterialTime = SqlInt32.Null;
                SqlInt32 researchCopyTime = SqlInt32.Null;
                SqlInt32 duplicatingTime = SqlInt32.Null;
                SqlInt32 reverseEngeneeringTime = SqlInt32.Null;
                SqlInt32 inventionTime = SqlInt32.Null;

                YamlNode activitiesNode = new YamlScalarNode(GenericConstant.ActivitiesText);
                if (cNode.Children.ContainsKey(activitiesNode))
                {
                    YamlMappingNode activityNode = cNode.Children[activitiesNode] as YamlMappingNode;

                    if (activityNode == null)
                        continue;

                    if (activityNode.Children.ContainsKey(manActivity))
                    {
                        YamlMappingNode actNode = (YamlMappingNode)activityNode.Children[manActivity];

                        if (actNode.Children.Keys.Any(key => key.ToString() == GenericConstant.ProductsText))
                        {
                            productTypeID = SqlInt32.Parse(
                                ((YamlMappingNode)
                                    ((YamlSequenceNode)actNode.Children[new YamlScalarNode(GenericConstant.ProductsText)]).Children
                                        .First()).Children[new YamlScalarNode(GenericConstant.TypeIDText)].ToString());
                        }
                    }

                    productionTime = activityNode.Children.ContainsKey(manActivity)
                        ? ((YamlMappingNode)activityNode.Children[manActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : productionTime;

                    researchTechTime = activityNode.Children.ContainsKey(rteActivity)
                        ? ((YamlMappingNode)activityNode.Children[rteActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : researchTechTime;

                    researchProductivityTime = activityNode.Children.ContainsKey(rtpActivity)
                        ? ((YamlMappingNode)activityNode.Children[rtpActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : researchProductivityTime;

                    researchMaterialTime = activityNode.Children.ContainsKey(rmpActivity)
                        ? ((YamlMappingNode)activityNode.Children[rmpActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : researchMaterialTime;

                    researchCopyTime = activityNode.Children.ContainsKey(copActivity)
                        ? ((YamlMappingNode)activityNode.Children[copActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : researchCopyTime;

                    duplicatingTime = activityNode.Children.ContainsKey(dupActivity)
                        ? ((YamlMappingNode)activityNode.Children[dupActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : duplicatingTime;

                    reverseEngeneeringTime = activityNode.Children.ContainsKey(renActivity)
                        ? ((YamlMappingNode)activityNode.Children[renActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : reverseEngeneeringTime;

                    inventionTime = activityNode.Children.ContainsKey(invActivity)
                        ? ((YamlMappingNode)activityNode.Children[invActivity]).Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TimeText)
                        : inventionTime;

                    foreach (KeyValuePair<YamlNode, YamlNode> activity in activityNode)
                    {
                        if (!activity.Key.Equals(manActivity))
                            ImportProducts(activity, blueprintTypeID, ramTypeRequirements);

                        ImportMaterials(activity, blueprintTypeID, ramTypeRequirements);
                        ImportSkills(activity, blueprintTypeID, ramTypeRequirements);
                    }
                }

                DataRow row = invBlueprintTypes.NewRow();
                invBlueprintTypes.Rows.Add(row);

                row[GenericConstant.BlueprintTypeIDText] = blueprintTypeID;
                row[GenericConstant.ProductTypeIDText] = productTypeID;
                row[GenericConstant.ProductionTimeText] = productionTime;
                row[GenericConstant.ResearchProductivityTimeText] = researchProductivityTime;
                row[GenericConstant.ResearchMaterialTimeText] = researchMaterialTime;
                row[GenericConstant.ResearchCopyTimeText] = researchCopyTime;
                row[GenericConstant.ResearchTechTimeText] = researchTechTime;
                row[GenericConstant.DuplicatingTimeText] = duplicatingTime;
                row[GenericConstant.ReverseEngineeringTimeText] = reverseEngeneeringTime;
                row[GenericConstant.InventionTimeText] = inventionTime;
                row[GenericConstant.MaxProductionLimitText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.MaxProductionLimitText);
            }

            List<IGrouping<dynamic, DataRow>> duplicates = ramTypeRequirements.AsEnumerable()
                .GroupBy(r =>
                    (dynamic)new
                    {
                        typeID = r[GenericConstant.TypeIDText],
                        activityID = r[GenericConstant.ActivityIDText],
                        requiredTypeID = r[GenericConstant.RequiredTypeIDText]
                    })
                .Where(gr => gr.Count() > 1).ToList();

            HandledDuplicateQuantityEntries(ramTypeRequirements, duplicates);

            s_sqlConnectionProvider.ImportDataBulk(invBlueprintTypes);
            s_sqlConnectionProvider.ImportDataBulk(ramTypeRequirements, false);

            Util.UpdatePercentDone(rowCount);
        }

        /// <summary>
        /// Handleds the duplicate quantity entries.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="duplicates">The duplicates.</param>
        private static void HandledDuplicateQuantityEntries(DataTable dataTable, IEnumerable<IGrouping<dynamic, DataRow>> duplicates)
        {
            foreach (var duplicate in duplicates)
            {
                List<DataRow> duplicateRows = duplicate.Select(r => r).ToList();
                DataRow firstRow = duplicateRows.First();
                SqlInt32 quantity = (SqlInt32)firstRow["quantity"];

                foreach (DataRow duplicateRow in duplicateRows.Where(row => row != firstRow))
                {
                    quantity += (SqlInt32)duplicateRow["quantity"];
                    dataTable.Rows.Remove(duplicateRow);
                }

                firstRow["quantity"] = quantity;
            }
        }

        /// <summary>
        /// Imports the products.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <param name="productTypeID">The product type identifier.</param>
        /// <param name="ramTypeRequirements">The ram type requirements.</param>
        private static void ImportProducts(KeyValuePair<YamlNode, YamlNode> activity, SqlInt32 productTypeID,
            DataTable ramTypeRequirements)
        {
            if (productTypeID == SqlInt32.Null)
                return;

            YamlMappingNode actNode = activity.Value as YamlMappingNode;

            if (actNode == null)
                return;

            YamlNode productsNode = new YamlScalarNode(GenericConstant.ProductsText);
            if (!actNode.Children.ContainsKey(productsNode))
                return;

            YamlSequenceNode prodsNode = actNode.Children[productsNode] as YamlSequenceNode;

            if (prodsNode == null)
                return;

            foreach (YamlNode product in prodsNode.Distinct())
            {
                YamlMappingNode prodNode = product as YamlMappingNode;

                if (prodNode == null)
                    continue;

                DataRow row = ramTypeRequirements.NewRow();
                ramTypeRequirements.Rows.Add(row);

                row[GenericConstant.TypeIDText] = productTypeID;
                row[GenericConstant.ActivityIDText] = activity.Key.GetSqlTypeOrDefaultOfEnum<SqlByte, Activity>();
                row[GenericConstant.RequiredTypeIDText] = prodNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText);
                row[GenericConstant.QuantityText] = prodNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.QuantityText);
                row[GenericConstant.ProbabilityText] = prodNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.ProbabilityText);
                row[GenericConstant.RaceIDText] = prodNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.RaceIDText);
            }
        }

        /// <summary>
        /// Imports the materials.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <param name="productTypeID">The product type identifier.</param>
        /// <param name="ramTypeRequirements">The ram type requirements.</param>
        private static void ImportMaterials(KeyValuePair<YamlNode, YamlNode> activity, SqlInt32 productTypeID,
            DataTable ramTypeRequirements)
        {
            if (productTypeID == SqlInt32.Null)
                return;

            YamlMappingNode actNode = activity.Value as YamlMappingNode;

            if (actNode == null)
                return;

            YamlNode materialsNode = new YamlScalarNode(GenericConstant.MaterialsText);
            if (!actNode.Children.ContainsKey(materialsNode))
                return;

            YamlSequenceNode matsNode = actNode.Children[materialsNode] as YamlSequenceNode;

            if (matsNode == null)
                return;

            foreach (YamlNode material in matsNode.Distinct())
            {
                YamlMappingNode matNode = material as YamlMappingNode;

                if (matNode == null)
                    continue;

                DataRow row = ramTypeRequirements.NewRow();
                ramTypeRequirements.Rows.Add(row);

                row[GenericConstant.TypeIDText] = productTypeID;
                row[GenericConstant.ActivityIDText] = activity.Key.GetSqlTypeOrDefaultOfEnum<SqlByte, Activity>();
                row[GenericConstant.RequiredTypeIDText] = matNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText);
                row[GenericConstant.QuantityText] = matNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.QuantityText);
                row[GenericConstant.ConsumeText] = matNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.ConsumeText);
            }
        }

        /// <summary>
        /// Imports the skills.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <param name="productTypeID">The product type identifier.</param>
        /// <param name="ramTypeRequirements">The ram type requirements.</param>
        private static void ImportSkills(KeyValuePair<YamlNode, YamlNode> activity, SqlInt32 productTypeID,
            DataTable ramTypeRequirements)
        {
            if (productTypeID == SqlInt32.Null)
                return;

            YamlMappingNode actNode = activity.Value as YamlMappingNode;

            if (actNode == null)
                return;

            YamlNode skillsNode = new YamlScalarNode(GenericConstant.SkillsText);
            if (!actNode.Children.ContainsKey(skillsNode))
                return;

            YamlSequenceNode sksNode = actNode.Children[skillsNode] as YamlSequenceNode;

            if (sksNode == null)
                return;

            foreach (YamlNode skill in sksNode.Distinct())
            {
                YamlMappingNode skillNode = skill as YamlMappingNode;

                if (skillNode == null)
                    continue;

                DataRow row = ramTypeRequirements.NewRow();
                ramTypeRequirements.Rows.Add(row);

                row[GenericConstant.TypeIDText] = productTypeID;
                row[GenericConstant.ActivityIDText] = activity.Key.GetSqlTypeOrDefaultOfEnum<SqlByte, Activity>();
                row[GenericConstant.RequiredTypeIDText] = skillNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText);
                row[GenericConstant.LevelText] = skillNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.LevelText);
            }
        }

        /// <summary>
        /// Gets the data table for the ramTypeRequirements table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetRamTypeRequirementsDataTable()
        {
            using (DataTable ramTypeRequirements = new DataTable(TableNamesConstant.RamTypeRequirementsTableName))
            {
                ramTypeRequirements.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.TypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ActivityIDText, typeof(SqlByte)),
                        new DataColumn(GenericConstant.RequiredTypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.QuantityText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.LevelText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.DamagePerJobText, typeof(SqlDouble)),
                        new DataColumn(GenericConstant.RecycleText, typeof(SqlBoolean)),
                        new DataColumn(GenericConstant.RaceIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ProbabilityText, typeof(SqlDouble)),
                        new DataColumn(GenericConstant.ConsumeText, typeof(SqlBoolean)),
                    });

                return ramTypeRequirements;
            }
        }

        /// <summary>
        /// Gets the data table for the invBlueprintTypes table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetInvBlueprintTypesDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.InvBlueprintTypesTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.BlueprintTypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ParentBlueprintTypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ProductTypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ProductionTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.TechLevelText, typeof(SqlInt16)),
                        new DataColumn(GenericConstant.ResearchProductivityTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ResearchMaterialTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ResearchCopyTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ResearchTechTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.DuplicatingTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ReverseEngineeringTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.InventionTimeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ProductivityModifierText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.MaterialModifierText, typeof(SqlInt16)),
                        new DataColumn(GenericConstant.WasteFactorText, typeof(SqlInt16)),
                        new DataColumn(GenericConstant.MaxProductionLimitText, typeof(SqlInt32)),
                    });
                return dataTable;
            }
        }


        #region Helper Enumeration

        private enum Activity
        {
            None = 0,

            [Description("manufacturing")]
            Manufacturing = 1,

            [Description("research_technology")]
            ResearchingTechnology = 2,

            [Description("research_time")]
            ResearchingTimeEfficiency = 3,

            [Description("research_material")]
            ResearchingMaterialEfficiency = 4,

            [Description("copying")]
            Copying = 5,

            [Description("duplicating")]
            Duplicating = 6,

            [Description("reverse_engineering")]
            ReverseEngineering = 7,

            [Description("invention")]
            Invention = 8
        }

        #endregion
    }
}
