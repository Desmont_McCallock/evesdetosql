﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    internal static class Types
    {
        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Types"/> class.
        /// </summary>
        static Types()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the types.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        internal static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string yamlFile = YamlFilesConstants.TypeIDs;
            string filePath = Util.CheckYamlFileExists(yamlFile);

            if (string.IsNullOrEmpty(filePath))
                return;

            string text = $"Parsing {yamlFile}... ";
            Console.Write(text);
            YamlMappingNode rNode = Util.ParseYamlFile(filePath);

            if (s_isClosing)
                return;

            Util.ResetConsoleCursorPosition(text);

            if (rNode == null)
            {
                Console.WriteLine(@"Unable to parse {0}.", yamlFile);
                return;
            }

            Console.Write(@"Importing {0}... ", yamlFile);

            var columns = new Dictionary<string, string>
            {
                { GenericConstant.FactionIDText, "int" },
                { GenericConstant.GraphicIDText, "int" },
                { GenericConstant.IconIDText, "int" },
                { GenericConstant.RadiusText, "float" },
                { GenericConstant.SoundIDText, "int" }
            };

            bool tableIsEmpty = s_sqlConnectionProvider.CreateTableOrColumns(rNode, GenericConstant.NameText,
                TableNamesConstant.InvTypesTableName, columns);

            s_sqlConnectionProvider.DropAndCreateTable(rNode, GenericConstant.MasteriesText,
                TableNamesConstant.DgmMasteriesTableName);
            s_sqlConnectionProvider.DropAndCreateTable(rNode, GenericConstant.MasteriesText,
                TableNamesConstant.DgmTypeMasteriesTableName);

            bool traitsAdded = s_sqlConnectionProvider.DropAndCreateTable(rNode, GenericConstant.TraitsText,
                TableNamesConstant.DgmTraitsTableName);
            s_sqlConnectionProvider.DropAndCreateTable(rNode, GenericConstant.TraitsText,
                TableNamesConstant.DgmTypeTraitsTableName);

            if (tableIsEmpty)
            {
                ImportTranslationsStaticData(traitsAdded);
                ImportDataBulk(rNode);
            }
            else
                ImportData(rNode);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportDataBulk(YamlMappingNode rNode)
        {
            Util.UpdatePercentDone(0);

            DataTable invTypesTable = GetInvTypesDataTable();
            DataTable dgmMasteriesTable = GetDgmMasteriesDataTable();
            DataTable dgmTypeMasteriesTable = GetDgmTypeMasteriesDataTable();
            DataTable dgmTraitsTable = GetDgmTraitsDataTable();
            DataTable dgmTypeTraitsTable = GetDgmTypeTraitsDataTable();
            DataTable trnTranslationsTable = Translations.GetTrnTranslationsDataTable();

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            int masteryId = 0;
            int traitId = 0;
            Dictionary<int, Dictionary<string, string>> masteriesDict = new Dictionary<int, Dictionary<string, string>>();
            Dictionary<int, string> traitsDict = new Dictionary<int, string>();

            foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
            {
                Util.UpdatePercentDone(rowCount);

                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                // Masteries
                ImportMasteries(cNode, dgmMasteriesTable, dgmTypeMasteriesTable, pair, masteriesDict, ref masteryId);

                //Traits
                ImportTraits(cNode, dgmTraitsTable, dgmTypeTraitsTable, trnTranslationsTable, pair, traitsDict, ref traitId);

                // Types
                ImportTypes(cNode, invTypesTable, pair, trnTranslationsTable);
            }

            Translations.DeleteTranslations(s_sqlConnectionProvider, GenericConstant.TranslationTypesDescriptionID);
            Translations.DeleteTranslations(s_sqlConnectionProvider, GenericConstant.TranslationTypesTypeNameID);
            Translations.DeleteTranslations(s_sqlConnectionProvider, GenericConstant.TranslationTraitsBonusTextID);
            Translations.ImportDataBulk(s_sqlConnectionProvider, trnTranslationsTable);

            s_sqlConnectionProvider.ImportDataBulk(invTypesTable);
            s_sqlConnectionProvider.ImportDataBulk(dgmMasteriesTable, false);
            s_sqlConnectionProvider.ImportDataBulk(dgmTypeMasteriesTable, false);
            s_sqlConnectionProvider.ImportDataBulk(dgmTraitsTable, false);
            s_sqlConnectionProvider.ImportDataBulk(dgmTypeTraitsTable, false);

            Util.UpdatePercentDone(rowCount);
        }

        /// <summary>
        /// Imports the data.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportData(YamlMappingNode rNode)
        {
            SqlConnection sqlConnection = (SqlConnection)s_sqlConnectionProvider.Connection;

            DataTable dgmMasteriesTable = GetDgmMasteriesDataTable();
            DataTable dgmTypeMasteriesTable = GetDgmTypeMasteriesDataTable();
            DataTable dgmTraitsTable = GetDgmTraitsDataTable();
            DataTable dgmTypeTraitsTable = GetDgmTypeTraitsDataTable();
            DataTable trnTranslationsTable = Translations.GetTrnTranslationsDataTable();

            int masteryId = 0;
            int traitId = 0;
            Dictionary<int, Dictionary<string, string>> masteriesDict = new Dictionary<int, Dictionary<string, string>>();
            Dictionary<int, string> traitsDict = new Dictionary<int, string>();

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            using (DbCommand command = new SqlCommand(
                string.Empty,
                sqlConnection,
                sqlConnection.BeginTransaction()))
            {
                Util.SetCommandTimeout(command);

                try
                {
                    foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
                    {
                        Util.UpdatePercentDone(rowCount);

                        YamlMappingNode cNode = rNode.Children[pair.Key] as YamlMappingNode;

                        if (cNode == null)
                            continue;

                        ImportMasteries(cNode, dgmMasteriesTable, dgmTypeMasteriesTable, pair, masteriesDict, ref masteryId);

                        ImportTraits(cNode, dgmTraitsTable, dgmTypeTraitsTable, trnTranslationsTable, pair, traitsDict,
                            ref traitId);

                        Dictionary<string, string> parameters = new Dictionary<string, string>
                        {
                            ["columnFilter1"] = GenericConstant.TypeIDText,
                            ["id1"] = pair.Key.ToString(),
                            [GenericConstant.FactionIDText] = cNode.Children.GetTextOrDefaultString(GenericConstant.FactionIDText),
                            [GenericConstant.GraphicIDText] = cNode.Children.GetTextOrDefaultString(GenericConstant.GraphicIDText),
                            [GenericConstant.IconIDText] = cNode.Children.GetTextOrDefaultString(GenericConstant.IconIDText),
                            [GenericConstant.RadiusText] = cNode.Children.GetTextOrDefaultString(GenericConstant.RadiusText),
                            [GenericConstant.SoundIDText] = cNode.Children.GetTextOrDefaultString(GenericConstant.SoundIDText)
                        };


                        command.CommandText = DbConnectionProvider.UpdateCommandText(TableNamesConstant.InvTypesTableName,
                            parameters);
                        command.ExecuteNonQuery();
                    }

                    command.Transaction.Commit();

                    if (trnTranslationsTable.Rows.Count > 0)
                    {
                        Translations.DeleteTranslations(s_sqlConnectionProvider, GenericConstant.TranslationTraitsBonusTextID);
                        Translations.ImportDataBulk(s_sqlConnectionProvider, trnTranslationsTable);
                    }

                    s_sqlConnectionProvider.ImportDataBulk(dgmMasteriesTable);
                    s_sqlConnectionProvider.ImportDataBulk(dgmTypeMasteriesTable);
                    s_sqlConnectionProvider.ImportDataBulk(dgmTraitsTable);
                    s_sqlConnectionProvider.ImportDataBulk(dgmTypeTraitsTable);
                }
                catch (SqlException e)
                {
                    Util.HandleExceptionForCommand(command, e);

                    command.Transaction?.Rollback();
                }
            }
        }

        /// <summary>
        /// Imports the types.
        /// </summary>
        /// <param name="cNode">The yaml node.</param>
        /// <param name="invTypesTable">The invTypes datatable.</param>
        /// <param name="pair">The pair.</param>
        /// <param name="trnTranslationsTable">The trnTranslations datatable.</param>
        private static void ImportTypes(YamlMappingNode cNode, DataTable invTypesTable, KeyValuePair<YamlNode, YamlNode> pair,
            DataTable trnTranslationsTable)
        {
            YamlMappingNode typeNameNodes = cNode.Children.Keys.Any(key => key.ToString() == GenericConstant.NameText)
                ? cNode.Children[new YamlScalarNode(GenericConstant.NameText)] as YamlMappingNode
                : null;
            YamlMappingNode descriptionNodes = cNode.Children.Keys.Any(key => key.ToString() == GenericConstant.DescriptionText)
                ? cNode.Children[new YamlScalarNode(GenericConstant.DescriptionText)] as YamlMappingNode
                : null;

            DataRow row = invTypesTable.NewRow();
            invTypesTable.Rows.Add(row);

            row[GenericConstant.TypeIDText] = SqlInt32.Parse(pair.Key.ToString());
            row[GenericConstant.GroupIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.GroupIDText);
            row[GenericConstant.TypeNameText] = typeNameNodes?.Children.GetSqlTypeOrDefault<SqlString>(
                GenericConstant.EnglishLanguageIDText, defaultValue: "")
                                                ??
                                                cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.NameText,
                                                    defaultValue: "");
            row[GenericConstant.DescriptionText] = descriptionNodes?.Children.GetSqlTypeOrDefault<SqlString>(
                GenericConstant.EnglishLanguageIDText,
                defaultValue: "") ??
                                                   cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.DescriptionText,
                                                       defaultValue: "");
            row[GenericConstant.MassText] = cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.MassText,
                defaultValue: "0");
            row[GenericConstant.VolumeText] = cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.VolumeText,
                defaultValue: "0");
            row[GenericConstant.CapacityText] = cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.CapacityText,
                defaultValue: "0");
            row[GenericConstant.PortionSizeText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.PortionSizeText);
            row[GenericConstant.RaceIDText] = cNode.Children.GetSqlTypeOrDefault<SqlByte>(GenericConstant.RaceIDText);
            row[GenericConstant.BasePriceText] = cNode.Children.GetSqlTypeOrDefault<SqlMoney>(GenericConstant.BasePriceText,
                defaultValue: "0.00");
            row[GenericConstant.PublishedText] = cNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.PublishedText,
                defaultValue: "0");
            row[GenericConstant.MarketGroupIDText] =
                cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.MarketGroupIDText);
            row[GenericConstant.ChanceOfDuplicatingText] =
                cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.ChanceOfDuplicatingText,
                    defaultValue: "0");
            row[GenericConstant.FactionIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.FactionIDText);
            row[GenericConstant.GraphicIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.GraphicIDText);
            row[GenericConstant.IconIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.IconIDText);
            row[GenericConstant.RadiusText] = cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText);
            row[GenericConstant.SoundIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.SoundIDText);

            if (typeNameNodes != null)
            {
                Translations.AddTranslationsParameters(GenericConstant.TranslationTypesTypeNameID, pair.Key, typeNameNodes,
                    trnTranslationsTable);
            }

            if (descriptionNodes != null)
            {
                Translations.AddTranslationsParameters(GenericConstant.TranslationTypesDescriptionID, pair.Key, descriptionNodes,
                    trnTranslationsTable);
            }
        }

        /// <summary>
        /// Imports the traits.
        /// </summary>
        /// <param name="cNode">The yaml node.</param>
        /// <param name="dgmTraitsTable">The dgmTraits datatable.</param>
        /// <param name="dgmTypeTraitsTable">The dgmTypeTraits datatable.</param>
        /// <param name="trnTranslationsTable">The trnTranslations datatable.</param>
        /// <param name="pair">The pair.</param>
        /// <param name="traitsDict">The traits dictionary.</param>
        /// <param name="traitId">The trait identifier.</param>
        private static void ImportTraits(YamlMappingNode cNode, DataTable dgmTraitsTable, DataTable dgmTypeTraitsTable,
            DataTable trnTranslationsTable, KeyValuePair<YamlNode, YamlNode> pair, Dictionary<int, string> traitsDict,
            ref int traitId)
        {
            YamlNode traitsNode = new YamlScalarNode(GenericConstant.TraitsText);

            if (!cNode.Children.ContainsKey(traitsNode))
                return;

            YamlMappingNode traitNode = cNode.Children[traitsNode] as YamlMappingNode;

            if (traitNode == null)
                return;

            foreach (KeyValuePair<YamlNode, YamlNode> trait in traitNode)
            {
                if (trait.Key.ToString() == "miscBonuses")
                    continue;

                // roleBonuses
                if (trait.Key.ToString() == "roleBonuses")
                {
                    YamlSequenceNode seqNode = traitNode.Children[trait.Key] as YamlSequenceNode;

                    if (seqNode == null)
                        continue;

                    // Order data by importance
                    foreach (YamlNode node in seqNode.Children.Where(bonusNode => bonusNode is YamlMappingNode)
                        .OrderBy(
                            bonusNode => ((YamlMappingNode)bonusNode).Children[new YamlScalarNode(GenericConstant.ImportanceText)],
                            new YamlNodeComparer()))
                    {
                        ImportTraits(dgmTraitsTable, dgmTypeTraitsTable, trnTranslationsTable, pair, traitsDict,
                            node, SqlInt32.Parse("-1"), ref traitId);
                    }

                    continue;
                }

                // types
                if (trait.Key.ToString() == "types")
                {
                    YamlMappingNode mapNode = traitNode.Children[trait.Key] as YamlMappingNode;

                    if (mapNode == null)
                        continue;

                    foreach (KeyValuePair<YamlNode, YamlNode> node in mapNode)
                    {
                        YamlSequenceNode seqNode = mapNode.Children[node.Key] as YamlSequenceNode;

                        if (seqNode == null)
                            continue;

                        // Order data by importance
                        foreach (YamlNode sNode in seqNode.Children.Where(bonusNode => bonusNode is YamlMappingNode)
                            .OrderBy(
                                bonusNode =>
                                        ((YamlMappingNode)bonusNode).Children[new YamlScalarNode(GenericConstant.ImportanceText)],
                                new YamlNodeComparer()))
                        {
                            ImportTraits(dgmTraitsTable, dgmTypeTraitsTable, trnTranslationsTable, pair, traitsDict,
                                sNode, SqlInt32.Parse(node.Key.ToString()), ref traitId);
                        }
                    }

                    continue;
                }

                // Old traits format
                YamlMappingNode bonusesNode = traitNode.Children[trait.Key] as YamlMappingNode;

                if (bonusesNode == null)
                    continue;

                foreach (YamlMappingNode bonusNode in bonusesNode
                    .Select(bonuses => bonusesNode.Children[bonuses.Key])
                    .OfType<YamlMappingNode>())
                {
                    ImportTraits(dgmTraitsTable, dgmTypeTraitsTable, trnTranslationsTable, pair, traitsDict,
                        bonusNode, SqlInt32.Parse(trait.Key.ToString()), ref traitId);
                }
            }
        }

        /// <summary>
        /// Imports the traits.
        /// </summary>
        /// <param name="dgmTraitsTable">The DGM traits table.</param>
        /// <param name="dgmTypeTraitsTable">The DGM type traits table.</param>
        /// <param name="trnTranslationsTable">The TRN translations table.</param>
        /// <param name="pair">The pair.</param>
        /// <param name="traitsDict">The traits dictionary.</param>
        /// <param name="node">The node.</param>
        /// <param name="parentTypeId">The parent type identifier.</param>
        /// <param name="traitId">The trait identifier.</param>
        private static void ImportTraits(DataTable dgmTraitsTable, DataTable dgmTypeTraitsTable, DataTable trnTranslationsTable,
            KeyValuePair<YamlNode, YamlNode> pair, Dictionary<int, string> traitsDict, YamlNode node, SqlInt32 parentTypeId,
            ref int traitId)
        {
            YamlMappingNode bonusNode = node as YamlMappingNode;

            if (bonusNode == null)
                return;

            ImportTraits(dgmTraitsTable, dgmTypeTraitsTable, trnTranslationsTable, pair, traitsDict,
                bonusNode, parentTypeId, ref traitId);
        }

        /// <summary>
        /// Imports the traits.
        /// </summary>
        /// <param name="dgmTraitsTable">The DGM traits table.</param>
        /// <param name="dgmTypeTraitsTable">The DGM type traits table.</param>
        /// <param name="trnTranslationsTable">The TRN translations table.</param>
        /// <param name="pair">The pair.</param>
        /// <param name="traitsDict">The traits dictionary.</param>
        /// <param name="bonusNode">The bonus node.</param>
        /// <param name="parentTypeId">The parent type identifier.</param>
        /// <param name="traitId">The trait identifier.</param>
        private static void ImportTraits(DataTable dgmTraitsTable, DataTable dgmTypeTraitsTable, DataTable trnTranslationsTable,
            KeyValuePair<YamlNode, YamlNode> pair, Dictionary<int, string> traitsDict, YamlMappingNode bonusNode, SqlInt32 parentTypeId,
            ref int traitId)
        {
            YamlMappingNode bonusTextNodes =
                bonusNode.Children.Keys.Any(key => key.ToString() == GenericConstant.BonusTextText)
                    ? bonusNode.Children[new YamlScalarNode(GenericConstant.BonusTextText)] as YamlMappingNode
                    : null;

            // dgmTraits
            traitId++;
            DataRow row = dgmTraitsTable.NewRow();

            row[GenericConstant.TraitIDText] = SqlInt32.Parse(traitId.ToString());
            row[GenericConstant.BonusTextText] = bonusTextNodes?.Children
                                                     .GetSqlTypeOrDefault<SqlString>(GenericConstant.EnglishLanguageIDText)
                                                 ??
                                                 bonusNode.Children
                                                     .GetSqlTypeOrDefault<SqlString>(GenericConstant.BonusTextText);

            row[GenericConstant.UnitIDText] =
                bonusNode.Children.GetSqlTypeOrDefault<SqlByte>(GenericConstant.UnitIDText);

            if (!dgmTraitsTable.Rows.OfType<DataRow>()
                .Select(x => x.ItemArray)
                .Any(x => x[1].ToString() == row[GenericConstant.BonusTextText].ToString() &&
                          x[2].ToString() == row[GenericConstant.UnitIDText].ToString()
                ))
            {
                dgmTraitsTable.Rows.Add(row);

                if (bonusTextNodes != null)
                {
                    Translations.AddTranslationsParameters(GenericConstant.TranslationTraitsBonusTextID,
                        new YamlScalarNode(traitId.ToString()),
                        bonusTextNodes, trnTranslationsTable);
                }
            }

            // dgmTypeTraits
            string value = row[GenericConstant.BonusTextText].ToString();
            row = dgmTypeTraitsTable.NewRow();

            row[GenericConstant.TypeIDText] = SqlInt32.Parse(pair.Key.ToString());
            row[GenericConstant.ParentTypeIDText] = parentTypeId;

            if (traitsDict.ContainsValue(value))
            {
                row[GenericConstant.TraitIDText] = SqlInt32.Parse(
                    traitsDict.First(x => x.Value == value).Key.ToString());
                row[GenericConstant.BonusText] =
                    bonusNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.BonusText);

                if (!dgmTypeTraitsTable.Rows.OfType<DataRow>()
                    .Select(x => x.ItemArray)
                    .Any(x =>
                        x[0].ToString() == row[GenericConstant.TypeIDText].ToString() &&
                        x[1].ToString() == row[GenericConstant.ParentTypeIDText].ToString() &&
                        x[2].ToString() == row[GenericConstant.TraitIDText].ToString()))
                {
                    dgmTypeTraitsTable.Rows.Add(row);
                }

                return;
            }

            traitsDict[traitId] = value;

            row[GenericConstant.TraitIDText] = SqlInt32.Parse(traitId.ToString());
            row[GenericConstant.BonusText] =
                bonusNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.BonusText);

            dgmTypeTraitsTable.Rows.Add(row);
        }

        /// <summary>
        /// Imports the masteries.
        /// </summary>
        /// <param name="cNode">The yaml node.</param>
        /// <param name="dgmMasteriesTable">The dgmMasteries datatable.</param>
        /// <param name="dgmTypeMasteriesTable">The dgmTypeMasteries datatable.</param>
        /// <param name="pair">The pair.</param>
        /// <param name="masteriesDict">The masteries dictionary.</param>
        /// <param name="masteryId">The mastery identifier.</param>
        private static void ImportMasteries(YamlMappingNode cNode, DataTable dgmMasteriesTable, DataTable dgmTypeMasteriesTable,
            KeyValuePair<YamlNode, YamlNode> pair, Dictionary<int, Dictionary<string, string>> masteriesDict, ref int masteryId)
        {
            YamlNode masteriesNode = new YamlScalarNode(GenericConstant.MasteriesText);

            if (!cNode.Children.ContainsKey(masteriesNode))
                return;

            YamlMappingNode mastNode = cNode.Children[masteriesNode] as YamlMappingNode;

            if (mastNode == null)
                return;

            foreach (KeyValuePair<YamlNode, YamlNode> mastery in mastNode)
            {
                YamlSequenceNode certNode = mastNode.Children[mastery.Key] as YamlSequenceNode;

                if (certNode == null)
                    continue;

                foreach (YamlNode certificate in certNode.Distinct())
                {
                    // dgmMasteries
                    masteryId++;
                    DataRow row = dgmMasteriesTable.NewRow();
                    row[GenericConstant.MasteryIDText] = SqlInt32.Parse(masteryId.ToString());
                    row[GenericConstant.CertificateIDText] = SqlInt32.Parse(certificate.ToString());
                    row[GenericConstant.GradeText] = SqlByte.Parse(mastery.Key.ToString());

                    if (!dgmMasteriesTable.Rows.OfType<DataRow>()
                        .Select(x => x.ItemArray)
                        .Any(x => x[1].ToString() == row[GenericConstant.CertificateIDText].ToString() &&
                                  x[2].ToString() == row[GenericConstant.GradeText].ToString()
                        ))
                    {
                        dgmMasteriesTable.Rows.Add(row);
                    }

                    // dgmTypeMasteries
                    row = dgmTypeMasteriesTable.NewRow();
                    row[GenericConstant.TypeIDText] = SqlInt32.Parse(pair.Key.ToString());

                    Dictionary<string, string> value = new Dictionary<string, string>
                    {
                        { mastery.Key.ToString(), certificate.ToString() }
                    };

                    if (masteriesDict.Values.Any(
                        x => x.Any(y => y.Key == mastery.Key.ToString()
                                        && y.Value == certificate.ToString())))
                    {
                        row[GenericConstant.MasteryIDText] = SqlInt16.Parse(
                            masteriesDict.First(
                                    x => value.Any(y => x.Value.ContainsKey(y.Key) && x.Value.ContainsValue(y.Value)))
                                .Key.ToString());

                        dgmTypeMasteriesTable.Rows.Add(row);

                        continue;
                    }

                    masteriesDict[masteryId] = value;

                    row[GenericConstant.MasteryIDText] = SqlInt16.Parse(masteryId.ToString());

                    dgmTypeMasteriesTable.Rows.Add(row);
                }
            }
        }

        /// <summary>
        /// Gets the data table for the dgmTypeTraits table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetDgmTypeTraitsDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.DgmTypeTraitsTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.TypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ParentTypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.TraitIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.BonusText, typeof(SqlDouble)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the data table for the dgmTraits table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetDgmTraitsDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.DgmTraitsTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.TraitIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.BonusTextText, typeof(SqlString)),
                        new DataColumn(GenericConstant.UnitIDText, typeof(SqlByte)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the data table for the dgmTypeMasteries table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetDgmTypeMasteriesDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.DgmTypeMasteriesTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.TypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.MasteryIDText, typeof(SqlInt16)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the data table for the dgmMsteries table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetDgmMasteriesDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.DgmMasteriesTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.MasteryIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.CertificateIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.GradeText, typeof(SqlByte)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the data table for the invTypes table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetInvTypesDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.InvTypesTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.TypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.GroupIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.TypeNameText, typeof(SqlString)),
                        new DataColumn(GenericConstant.DescriptionText, typeof(SqlString)),
                        new DataColumn(GenericConstant.MassText, typeof(SqlDouble)),
                        new DataColumn(GenericConstant.VolumeText, typeof(SqlDouble)),
                        new DataColumn(GenericConstant.CapacityText, typeof(SqlDouble)),
                        new DataColumn(GenericConstant.PortionSizeText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.RaceIDText, typeof(SqlByte)),
                        new DataColumn(GenericConstant.BasePriceText, typeof(SqlMoney)),
                        new DataColumn(GenericConstant.PublishedText, typeof(SqlBoolean)),
                        new DataColumn(GenericConstant.MarketGroupIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ChanceOfDuplicatingText, typeof(SqlDouble)),
                        new DataColumn(GenericConstant.FactionIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.GraphicIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.IconIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.RadiusText, typeof(SqlDouble)),
                        new DataColumn(GenericConstant.SoundIDText, typeof(SqlInt32)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Imports the translations static data.
        /// </summary>
        /// <param name="addTraitsRecords">if set to <c>true</c> [add traits records].</param>
        private static void ImportTranslationsStaticData(bool addTraitsRecords)
        {
            Translations.InsertTranslationsStaticData(s_sqlConnectionProvider, new TranslationsParameters
            {
                TableName = TableNamesConstant.InvTypesTableName,
                SourceTable = "inventory.typesTx",
                ColumnName = GenericConstant.DescriptionText,
                MasterID = GenericConstant.TypeIDText,
                TcGroupID = GenericConstant.TranslationTypesGroupID,
                TcID = GenericConstant.TranslationTypesDescriptionID
            });

            Translations.InsertTranslationsStaticData(s_sqlConnectionProvider, new TranslationsParameters
            {
                TableName = TableNamesConstant.InvTypesTableName,
                SourceTable = "inventory.typesTx",
                ColumnName = GenericConstant.TypeNameText,
                MasterID = GenericConstant.TypeIDText,
                TcGroupID = GenericConstant.TranslationTypesGroupID,
                TcID = GenericConstant.TranslationTypesTypeNameID
            });

            if (!addTraitsRecords)
                return;

            Translations.InsertTranslationsStaticData(s_sqlConnectionProvider, new TranslationsParameters
            {
                TableName = TableNamesConstant.DgmTraitsTableName,
                SourceTable = "dogma.traitsTx",
                ColumnName = GenericConstant.BonusTextText,
                MasterID = GenericConstant.TraitIDText,
                TcGroupID = GenericConstant.TranslationTraitsGroupID,
                TcID = GenericConstant.TranslationTraitsBonusTextID
            });
        }
    }
}
