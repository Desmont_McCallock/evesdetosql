﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    internal static class DataTablePerYamlFile
    {
        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="DataTablePerYamlFile"/> class.
        /// </summary>
        static DataTablePerYamlFile()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the blueprints.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        public static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            IList<string> yamlFiles = Util.GetYamlFilesInBsdDirectory().ToList();

            if (!yamlFiles.Any())
            {
                Console.WriteLine(@"No per table yaml files where found. Ensure that the folder 'bsd' exists under folder 'sde'.");
                Console.WriteLine(@"Per table yaml files importation skipped.");
                Console.WriteLine();
                return;
            }

            foreach (string yamlFile in yamlFiles)
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                Util.ResetCounters();

                string filename = Path.GetFileName(yamlFile);
                if (string.IsNullOrEmpty(filename))
                    return;

                string text = $"Parsing {filename}... ";
                Console.Write(text);
                YamlSequenceNode rNode = Util.ParsePerTableFileYamlFile(yamlFile);

                if (s_isClosing)
                    return;

                Util.ResetConsoleCursorPosition(text);

                if (rNode == null)
                {
                    Console.WriteLine($"Unable to parse {filename}.");
                    return;
                }

                Console.Write($"Importing {filename}... ");

                string tableName = Path.GetFileNameWithoutExtension(yamlFile);
                s_sqlConnectionProvider.DropAndCreateTable(tableName);

                ImportDataBulk(tableName, rNode);

                Util.DisplayEndTime(stopwatch);
            }
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="rNode">The root node.</param>
        private static void ImportDataBulk(string tableName, YamlSequenceNode rNode)
        {
            if (s_isClosing)
                return;

            Util.UpdatePercentDone(0);

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            DataTable dataTable = Util.GetDataTable(tableName, rNode, rowCount);

            s_sqlConnectionProvider.ImportDataBulk(dataTable);

            Util.UpdatePercentDone(rowCount);
        }
    }
}
