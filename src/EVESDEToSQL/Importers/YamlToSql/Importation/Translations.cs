﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    internal static class Translations
    {
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Translations"/> class.
        /// </summary>
        static Translations()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Gets the data table.
        /// </summary>
        /// <returns></returns>
        internal static DataTable GetTrnTranslationsDataTable()
        {
            using (DataTable trnTranslationsTable = new DataTable(TableNamesConstant.TrnTranslationsTableName))
            {
                trnTranslationsTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.TcIDText, typeof(SqlInt16)),
                        new DataColumn(GenericConstant.KeyIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.LanguageIDText, typeof(SqlString)),
                        new DataColumn(GenericConstant.TextText, typeof(SqlString)),
                    });

                return trnTranslationsTable;
            }
        }

        /// <summary>
        /// Deletes the translations of the specifies tcID.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="tcID">The tc identifier.</param>
        internal static void DeleteTranslations(DbConnectionProvider provider, string tcID)
        {
            if (s_isClosing)
                return;

            SqlConnectionProvider sqlConnectionProvider = provider as SqlConnectionProvider;

            if (sqlConnectionProvider == null)
                return;

            SqlConnection sqlConnection = sqlConnectionProvider.Connection as SqlConnection;

            if (sqlConnection == null)
                return;

            using (DbCommand command = new SqlCommand(
                string.Empty,
                sqlConnection,
                sqlConnection.BeginTransaction()))
            {
                Util.SetCommandTimeout(command);

                try
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters["columnFilter1"] = GenericConstant.TcIDText;
                    parameters["id1"] = tcID;

                    command.CommandText = DbConnectionProvider.DeleteCommandText(TableNamesConstant.TrnTranslationsTableName, parameters);
                    command.ExecuteNonQuery();
                    command.Transaction.Commit();
                }
                catch (SqlException e)
                {
                    Util.HandleExceptionForCommand(command, e);

                    if (command.Transaction != null)
                        command.Transaction.Rollback();
                }
            }
        }

        /// <summary>
        /// Inserts the translations static data.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="trnParameters">The translations parameters.</param>
        internal static void InsertTranslationsStaticData(DbConnectionProvider provider, TranslationsParameters trnParameters)
        {
            string tableText = "dbo." + trnParameters.TableName;
            var baseParameters = new Dictionary<string, string>
            {
                [GenericConstant.TcGroupIDText] = trnParameters.TcGroupID,
                [GenericConstant.TcIDText] = trnParameters.TcID
            };

            var parameters = new Dictionary<string, string>(baseParameters)
            {
                [GenericConstant.SourceTableText] = trnParameters.SourceTable.GetTextOrDefaultString(),
                [GenericConstant.DestinationTableText] = tableText.GetTextOrDefaultString(),
                [GenericConstant.TranslatedKeyText] = trnParameters.ColumnName.GetTextOrDefaultString(),
                ["columnFilter1"] = GenericConstant.SourceTableText
            };
            parameters["id1"] = parameters[GenericConstant.SourceTableText];
            parameters["columnFilter2"] = GenericConstant.TranslatedKeyText;
            parameters["id2"] = parameters[GenericConstant.TranslatedKeyText];

            InsertStaticData(provider, TableNamesConstant.TranslationTableName, parameters);

            parameters = new Dictionary<string, string>(baseParameters)
            {
                [GenericConstant.TableNameText] = tableText.GetTextOrDefaultString(),
                [GenericConstant.ColumnNameText] = trnParameters.ColumnName.GetTextOrDefaultString(),
                [GenericConstant.MasterIDText] = trnParameters.MasterID.GetTextOrDefaultString(),
                ["columnFilter1"] = GenericConstant.TcIDText
            };
            parameters["id1"] = parameters[GenericConstant.TcIDText];

            InsertStaticData(provider, TableNamesConstant.TrnTranslationColumnsTableName, parameters);
        }

        /// <summary>
        /// Inserts the static data.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        private static void InsertStaticData(DbConnectionProvider provider, string tableName,
            IDictionary<string, string> parameters)
        {
            if (s_isClosing)
                return;

            SqlConnectionProvider sqlConnectionProvider = provider as SqlConnectionProvider;

            if (sqlConnectionProvider == null)
                return;

            // Quit if table doesn't exists
            if (!sqlConnectionProvider.CheckTableExists(tableName))
                return;

            SqlConnection sqlConnection = sqlConnectionProvider.Connection as SqlConnection;

            if (sqlConnection == null)
                return;

            using (DbCommand command = new SqlCommand(
                string.Empty,
                sqlConnection,
                sqlConnection.BeginTransaction()))
            {
                Util.SetCommandTimeout(command);

                try
                {
                    command.CommandText = DbConnectionProvider.UpdateCommandText(tableName, parameters);
                    if (command.ExecuteNonQuery() == 0)
                    {
                        foreach (KeyValuePair<string, string> parameter in parameters
                            .Where(par => par.Key.StartsWith("id", StringComparison.OrdinalIgnoreCase) ||
                                          par.Key.StartsWith("columnFilter", StringComparison.OrdinalIgnoreCase))
                            .ToList())
                        {
                            parameters.Remove(parameter);
                        }

                        command.CommandText = DbConnectionProvider.InsertCommandText(tableName, parameters);
                        command.ExecuteNonQuery();
                    }

                    command.Transaction.Commit();
                }
                catch (SqlException e)
                {
                    Util.HandleExceptionForCommand(command, e);

                    command.Transaction?.Rollback();
                }
            }
        }

        /// <summary>
        /// Adds the translations parameters.
        /// </summary>
        /// <param name="tcID">The tc identifier.</param>
        /// <param name="key">The key.</param>
        /// <param name="nameNodes">The name nodes.</param>
        /// <param name="table">The table.</param>
        internal static void AddTranslationsParameters(string tcID, YamlNode key, YamlMappingNode nameNodes, DataTable table)
        {
            foreach (KeyValuePair<YamlNode, YamlNode> pair in nameNodes)
            {
                DataRow row = table.NewRow();
                table.Rows.Add(row);

                row[GenericConstant.TcIDText] = SqlInt16.Parse(tcID);
                row[GenericConstant.KeyIDText] = SqlInt32.Parse(key.ToString());
                row[GenericConstant.LanguageIDText] = GetProperLanguageID(pair.Key);
                row[GenericConstant.TextText] = !nameNodes.Children.ContainsKey(new YamlScalarNode(pair.Key.ToString())) ||
                                nameNodes.Children[new YamlScalarNode(pair.Key.ToString())] == null
                    ? Convert.DBNull.ToString()
                    : nameNodes.Children[new YamlScalarNode(pair.Key.ToString())].ToString();
            }
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="provider">The provider.</param>
        /// <param name="data">The data.</param>
        internal static void ImportDataBulk(DbConnectionProvider provider, DataTable data)
        {
            SqlConnectionProvider sqlConnectionProvider = provider as SqlConnectionProvider;

            sqlConnectionProvider?.ImportDataBulk(data, false);
        }

        /// <summary>
        /// Gets the proper language identifier.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private static string GetProperLanguageID(YamlNode key)
        {
            string languageIDText = key.ToString();
            if (string.Equals(languageIDText, GenericConstant.EnglishLanguageIDText, StringComparison.OrdinalIgnoreCase))
                languageIDText = languageIDText + "-US";

            return languageIDText.ToUpperInvariant();
        }
    }
}
