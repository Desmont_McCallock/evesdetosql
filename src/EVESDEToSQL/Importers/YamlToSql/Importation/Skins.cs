﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    internal static class Skins
    {
        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Skins"/> class.
        /// </summary>
        static Skins()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the skins.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        internal static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string yamlFile = YamlFilesConstants.Skins;
            string filePath = Util.CheckYamlFileExists(yamlFile);

            if (string.IsNullOrEmpty(filePath))
                return;

            string text = $"Parsing {yamlFile}... ";
            Console.Write(text);
            YamlMappingNode rNode = Util.ParseYamlFile(filePath);

            if (s_isClosing)
                return;

            Util.ResetConsoleCursorPosition(text);

            if (rNode == null)
            {
                Console.WriteLine($"Unable to parse {yamlFile}.");
                return;
            }

            Console.Write($"Importing {yamlFile}... ");

            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.SknSkinsTableName);

            ImportDataBulk(rNode);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportDataBulk(YamlMappingNode rNode)
        {
            if (s_isClosing)
                return;

            Util.UpdatePercentDone(0);

            DataTable sknSkinsTable = GetSknSkinsDataTable();

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
            {
                Util.UpdatePercentDone(rowCount);

                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                DataRow row = sknSkinsTable.NewRow();
                sknSkinsTable.Rows.Add(row);

                row[GenericConstant.SkinIDText] = SqlInt32.Parse(pair.Key.ToString());
                row[GenericConstant.InternalNameText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.InternalNameText, defaultValue: "");
                row[GenericConstant.SkinMaterialIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.SkinMaterialIDText);
                row[GenericConstant.AllowCCPDevsText] = cNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.AllowCCPDevsText, defaultValue: "0");
                row[GenericConstant.VisibleSerenityText] = cNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.VisibleSerenityText, defaultValue: "0");
                row[GenericConstant.VisibleTranquilityText] = cNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.VisibleTranquilityText,
                    defaultValue: "0");

                YamlNode typesNode = new YamlScalarNode(GenericConstant.TypesText);
                if (!cNode.Children.ContainsKey(typesNode))
                    continue;

                YamlSequenceNode typeIDsNode = cNode.Children[typesNode] as YamlSequenceNode;

                row[GenericConstant.TypeIDText] = typeIDsNode != null
                    ? typeIDsNode.Count() == 1
                        ? SqlInt32.Parse(typeIDsNode.Children.First().ToString())
                        : SqlInt32.Null
                    : SqlInt32.Null;
            }

            s_sqlConnectionProvider.ImportDataBulk(sknSkinsTable);

            Util.UpdatePercentDone(rowCount);
        }

        /// <summary>
        /// Gets the data table for the sknSkins table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetSknSkinsDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.SknSkinsTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.SkinIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.InternalNameText, typeof(SqlString)),
                        new DataColumn(GenericConstant.SkinMaterialIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.TypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.AllowCCPDevsText, typeof(SqlBoolean)),
                        new DataColumn(GenericConstant.VisibleSerenityText, typeof(SqlBoolean)),
                        new DataColumn(GenericConstant.VisibleTranquilityText, typeof(SqlBoolean)),
                    });
                return dataTable;
            }
        }
    }
}
