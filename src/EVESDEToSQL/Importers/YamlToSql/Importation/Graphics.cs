﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    internal static class Graphics
    {
        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Graphics"/> class.
        /// </summary>
        static Graphics()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the graphics.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        internal static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string yamlFile = YamlFilesConstants.GraphicsIDs;
            string filePath = Util.CheckYamlFileExists(yamlFile);

            if (string.IsNullOrEmpty(filePath))
                return;

            string text = $"Parsing {yamlFile}... ";
            Console.Write(text);
            YamlMappingNode rNode = Util.ParseYamlFile(filePath);

            if (s_isClosing)
                return;

            Util.ResetConsoleCursorPosition(text);

            if (rNode == null)
            {
                Console.WriteLine($"Unable to parse {yamlFile}.");
                return;
            }

            Console.Write($"Importing {yamlFile}... ");

            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.EveGraphicsTableName);

            ImportDataBulk(rNode);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportDataBulk(YamlMappingNode rNode)
        {
            if (s_isClosing)
                return;

            Util.UpdatePercentDone(0);

            DataTable eveGraphicsTable = GetEveGraphicsDataTable();

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
            {
                Util.UpdatePercentDone(rowCount);

                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                DataRow row = eveGraphicsTable.NewRow();
                eveGraphicsTable.Rows.Add(row);

                row[GenericConstant.GraphicIDText] = SqlInt32.Parse(pair.Key.ToString());
                row[GenericConstant.GraphicFileText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.GraphicFileText, defaultValue: "");
                row[GenericConstant.DescriptionText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.DescriptionText, defaultValue: "");
                row[GenericConstant.ObsoleteText] = cNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.ObsoleteText,
                    defaultValue: "0");
                row[GenericConstant.GraphicTypeText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.GraphicTypeText);
                row[GenericConstant.CollidableText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.CollidableText);
                row[GenericConstant.DirectoryIDText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.DirectoryIDText);
                row[GenericConstant.GraphicNameText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.GraphicNameText, defaultValue: "");
                row[GenericConstant.GfxRaceIDText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.GfxRaceIDText);
                row[GenericConstant.ColorSchemeText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.ColorSchemeText);
                row[GenericConstant.SofHullNameText] =
                    cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.SofHullNameText);
            }

            s_sqlConnectionProvider.ImportDataBulk(eveGraphicsTable);

            Util.UpdatePercentDone(rowCount);
        }

        /// <summary>
        /// Gets the data table for the eveGraphics table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetEveGraphicsDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.EveGraphicsTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.GraphicIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.GraphicFileText, typeof(SqlString)),
                        new DataColumn(GenericConstant.DescriptionText, typeof(SqlString)),
                        new DataColumn(GenericConstant.ObsoleteText, typeof(SqlBoolean)),
                        new DataColumn(GenericConstant.GraphicTypeText, typeof(SqlString)),
                        new DataColumn(GenericConstant.CollidableText, typeof(SqlBoolean)),
                        new DataColumn(GenericConstant.DirectoryIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.GraphicNameText, typeof(SqlString)),
                        new DataColumn(GenericConstant.GfxRaceIDText, typeof(SqlString)),
                        new DataColumn(GenericConstant.ColorSchemeText, typeof(SqlString)),
                        new DataColumn(GenericConstant.SofHullNameText, typeof(SqlString)),
                    });
                return dataTable;
            }
        }
    }
}
