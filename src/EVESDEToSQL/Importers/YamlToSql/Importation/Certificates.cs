﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    /// <summary>
    /// Represents a certificate grade.
    /// </summary>
    internal enum CertificateGrade
    {
        None = 0,
        Basic = 1,
        Standard = 2,
        Improved = 3,
        Advanced = 4,
        Elite = 5
    }

    internal static class Certificates
    {
        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Certificates"/> class.
        /// </summary>
        static Certificates()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the certificates.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        public static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string yamlFile = YamlFilesConstants.Certificates;
            string filePath = Util.CheckYamlFileExists(yamlFile);

            if (string.IsNullOrEmpty(filePath))
                return;

            string text = $"Parsing {yamlFile}... ";
            Console.Write(text);
            YamlMappingNode rNode = Util.ParseYamlFile(filePath);

            if (s_isClosing)
                return;

            Util.ResetConsoleCursorPosition(text);

            if (rNode == null)
            {
                Console.WriteLine($"Unable to parse {yamlFile}.");
                return;
            }

            Console.Write($"Importing {yamlFile}... ");

            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.CrtClassesTableName);
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.CrtCertificateTableName);
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.CrtRecommendationsTableName);
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.CrtRelationshipsTableName);

            ImportDataBulk(rNode);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportDataBulk(YamlMappingNode rNode)
        {
            if (s_isClosing)
                return;

            Util.UpdatePercentDone(0);

            DataTable crtCertificatesTable = GetCrtCertificatesDataTable();
            DataTable crtClassesTable = GetCrtClassesDataTable();
            DataTable crtRecommendationsTable = GetCrtRecommendationsDataTable();
            DataTable crtRelationshipsTable = GetCrtRelationshipsDataTable();

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            int classId = 0;

            foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
            {
                Util.UpdatePercentDone(rowCount);

                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                ImportClasses(cNode, crtClassesTable, ref classId);

                ImportRecommendations(cNode, crtRecommendationsTable, pair);

                ImportRelationships(cNode, crtRelationshipsTable, pair);

                DataRow row = crtCertificatesTable.NewRow();
                crtCertificatesTable.Rows.Add(row);

                row[GenericConstant.CertificateIDText] = SqlInt32.Parse(pair.Key.ToString());
                row[GenericConstant.GroupIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt16>(GenericConstant.GroupIDText);
                row[GenericConstant.ClassIDText] = classId;
                row[GenericConstant.DescriptionText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.DescriptionText, defaultValue: "");
            }

            s_sqlConnectionProvider.ImportDataBulk(crtClassesTable);
            s_sqlConnectionProvider.ImportDataBulk(crtCertificatesTable, false);
            s_sqlConnectionProvider.ImportDataBulk(crtRecommendationsTable, false);
            s_sqlConnectionProvider.ImportDataBulk(crtRelationshipsTable, false);

            Util.UpdatePercentDone(rowCount);
        }

        /// <summary>
        /// Imports the relationships.
        /// </summary>
        /// <param name="cNode">The c node.</param>
        /// <param name="crtRelationshipsTable">The crtRelationships table.</param>
        /// <param name="pair">The pair.</param>
        private static void ImportRelationships(YamlMappingNode cNode, DataTable crtRelationshipsTable,
            KeyValuePair<YamlNode, YamlNode> pair)
        {
            YamlNode skillTypesNode = new YamlScalarNode(GenericConstant.SkillTypesText);
            if (!cNode.Children.ContainsKey(skillTypesNode))
                return;

            YamlMappingNode stNode = cNode.Children[skillTypesNode] as YamlMappingNode;

            if (stNode == null)
                return;

            foreach (KeyValuePair<YamlNode, YamlNode> skillType in stNode)
            {
                YamlMappingNode grNode = skillType.Value as YamlMappingNode;

                if (grNode == null)
                    continue;

                foreach (KeyValuePair<YamlNode, YamlNode> grade in grNode)
                {
                    DataRow row = crtRelationshipsTable.NewRow();
                    crtRelationshipsTable.Rows.Add(row);

                    row[GenericConstant.RelationshipIDText] = crtRelationshipsTable.Rows.Count;
                    row[GenericConstant.ParentTypeIDText] = SqlInt32.Parse(skillType.Key.ToString());
                    row[GenericConstant.ParentLevelText] = SqlByte.Parse(grade.Value.ToString());
                    row[GenericConstant.ChildIDText] = SqlInt32.Parse(pair.Key.ToString());
                    row[GenericConstant.GradeText] = SqlByte.Parse(
                        ((byte)(int)Enum.Parse(typeof(CertificateGrade), grade.Key.ToString(), true))
                            .ToString());
                }
            }
        }

        /// <summary>
        /// Imports the recommendations.
        /// </summary>
        /// <param name="cNode">The c node.</param>
        /// <param name="crtRecommendationsTable">The crtRecommendations table.</param>
        /// <param name="pair">The pair.</param>
        private static void ImportRecommendations(YamlMappingNode cNode, DataTable crtRecommendationsTable,
            KeyValuePair<YamlNode, YamlNode> pair)
        {
            YamlNode recommendedForNode = new YamlScalarNode(GenericConstant.RecommendedForText);
            if (!cNode.Children.ContainsKey(recommendedForNode))
                return;

            YamlSequenceNode recNode = cNode.Children[recommendedForNode] as YamlSequenceNode;

            if (recNode == null)
                return;

            foreach (YamlNode recommendation in recNode.Distinct())
            {
                DataRow row = crtRecommendationsTable.NewRow();
                crtRecommendationsTable.Rows.Add(row);

                row[GenericConstant.RecommendationIDText] = crtRecommendationsTable.Rows.Count;
                row[GenericConstant.ShipTypeIDText] = SqlInt32.Parse(recommendation.ToString());
                row[GenericConstant.CertificateIDText] = SqlInt32.Parse(pair.Key.ToString());
            }
        }

        /// <summary>
        /// Imports the classes.
        /// </summary>
        /// <param name="cNode">The c node.</param>
        /// <param name="crtClassesTable">The CRT classes table.</param>
        /// <param name="classId">The class identifier.</param>
        private static void ImportClasses(YamlMappingNode cNode, DataTable crtClassesTable, ref int classId)
        {
            YamlNode nameNode = new YamlScalarNode(GenericConstant.NameText);
            if (!cNode.Children.ContainsKey(nameNode))
                return;

            classId++;
            DataRow row = crtClassesTable.NewRow();
            crtClassesTable.Rows.Add(row);

            row[GenericConstant.ClassIDText] = classId;
            row[GenericConstant.DescriptionText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.NameText, defaultValue: "");
            row[GenericConstant.ClassNameText] = cNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.NameText, defaultValue: "");
        }

        /// <summary>
        /// Gets the data table for the crtRelationships table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetCrtRelationshipsDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.CrtRelationshipsTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn("relationshipID", typeof(SqlInt32)),
                        new DataColumn("parentID", typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ParentTypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ParentLevelText, typeof(SqlByte)),
                        new DataColumn(GenericConstant.ChildIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.GradeText, typeof(SqlByte)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the data table for the crtRecommendations table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetCrtRecommendationsDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.CrtRecommendationsTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn("recommendationID", typeof(SqlInt32)),
                        new DataColumn(GenericConstant.ShipTypeIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.CertificateIDText, typeof(SqlInt32)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the data table for the crtClasses table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetCrtClassesDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.CrtClassesTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.ClassIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.DescriptionText, typeof(SqlString)),
                        new DataColumn(GenericConstant.ClassNameText, typeof(SqlString)),
                    });
                return dataTable;
            }
        }

        /// <summary>
        /// Gets the data table for the crtCertificates table.
        /// </summary>
        /// <returns></returns>
        private static DataTable GetCrtCertificatesDataTable()
        {
            using (DataTable dataTable = new DataTable(TableNamesConstant.CrtCertificateTableName))
            {
                dataTable.Columns.AddRange(
                    new[]
                    {
                        new DataColumn(GenericConstant.CertificateIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.GroupIDText, typeof(SqlInt16)),
                        new DataColumn(GenericConstant.ClassIDText, typeof(SqlInt32)),

                        // Not used columns
                        new DataColumn(GenericConstant.GradeText, typeof(SqlByte)),
                        new DataColumn("corpID", typeof(SqlInt32)),
                        new DataColumn(GenericConstant.IconIDText, typeof(SqlInt32)),
                        new DataColumn(GenericConstant.DescriptionText, typeof(SqlString)),
                    });
                return dataTable;
            }
        }
    }
}
