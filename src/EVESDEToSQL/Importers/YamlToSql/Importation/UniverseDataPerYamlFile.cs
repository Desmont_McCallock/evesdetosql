﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using YamlDotNet.RepresentationModel;

namespace EVESDEToSQL.Importers.YamlToSQL.Importation
{
    internal static class UniverseDataPerYamlFile
    {
        private const string ImportText = "Importing universe data... ";

        private static SqlConnectionProvider s_sqlConnectionProvider;
        private static bool s_isClosing;
        private static bool s_showMissingDataWarning;
        private static bool s_showItemNameFromNamesTableWarning;
        private static bool s_showLandmarkNameFromTranslationTableWarning;

        private static DataTable s_mapRegionsDataTable;
        private static DataTable s_mapConstellationsDataTable;
        private static DataTable s_mapSolarSystemsDataTable;
        private static DataTable s_mapJumpsDataTable;
        private static DataTable s_mapLocationScenesDataTable;
        private static DataTable s_mapLocationWormholeClassesDataTable;
        private static DataTable s_mapLandmarksDataTable;
        private static DataTable s_mapCelestialStatisticsDataTable;
        private static DataTable s_mapDenormalizeDataTable;
        private static DataTable s_mapSolarSystemJumpsDataTable;
        private static DataTable s_mapConstellationJumpsDataTable;
        private static DataTable s_mapRegionJumpsDataTable;
        private static DataTable s_trnTranslationsDataTable;
        private static DataTable s_invNamesDataTable;

        /// <summary>
        /// Initializes the <see cref="UniverseDataPerYamlFile"/> class.
        /// </summary>
        static UniverseDataPerYamlFile()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;
        }

        /// <summary>
        /// Imports the blueprints.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <exception cref="System.ArgumentNullException">sqlConnectionProvider</exception>
        public static void Import(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            s_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;

            if (s_isClosing)
                return;

            ImportUniverseData();

            // mapLandmarks
            ImportLandmarksData();

            if (s_showMissingDataWarning)
            {
                Console.WriteLine();
                Console.WriteLine(@"WARNING! IMPORTATION OF UNIVERSE DATA RESULTED SOME TABLES MISSING DATA");
            }

            if (s_showItemNameFromNamesTableWarning)
            {
                Console.WriteLine();
                Console.WriteLine(@"WARNING! ITEM NAMES IN MAPDENORMALIZE TABLE WHERE IMPORTED FROM INVNAMES TABLE");
            }

            if (s_showLandmarkNameFromTranslationTableWarning)
            {
                Console.WriteLine();
                Console.WriteLine(@"WARNING! LANDMARK NAMES AND DESCRIPTIONS WHERE IMPORTED FROM TRNTRANSLATION TABLE");
            }

            // Always add another line to separate the warnings
            if (s_showMissingDataWarning || s_showItemNameFromNamesTableWarning || s_showLandmarkNameFromTranslationTableWarning)
                Console.WriteLine();
        }

        /// <summary>
        /// Imports the landmarks data.
        /// </summary>
        private static void ImportLandmarksData()
        {
            if (!Util.CheckLandmarksDirectoryExists())
            {
                Console.WriteLine(
                    @"No landmarks yaml file where found. Ensure that the folder 'landmarks' exists under folder 'fsd'.");
                return;
            }

            if (!CheckLandmarkDataExists())
            {
                Console.WriteLine(
                    $@"{TableNamesConstant.TrnTranslationsTableName} table or yaml file was not found. Landmarks data will not be imported.");
                Console.WriteLine(@"Landmarks data importation skipped.");
                Console.WriteLine();
                return;
            }

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string filePath = Util.CheckStaticDataYamlFileExists(Util.GetLandmarksDirectory(), "landmarks");
            if (!File.Exists(filePath))
                return;

            string filename = Path.GetFileName(filePath);

            string text = $"Parsing {filename}... ";

            Console.Write(text);
            YamlMappingNode rNode = Util.ParseYamlFile(filePath);

            if (s_isClosing)
                return;

            Util.ResetConsoleCursorPosition(text);

            if (rNode == null)
            {
                Console.WriteLine($@"Unable to parse {filename}.");
                return;
            }
            
            Console.Write(@"Importing landmarks data... ");

            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapLandmarksTableName);

            Util.UpdatePercentDone(0);

            s_mapLandmarksDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapLandmarksTableName);

            double rowCount = Math.Round((double)rNode.Count() * 2, 0, MidpointRounding.AwayFromZero);

            foreach (KeyValuePair<YamlNode, YamlNode> pair in rNode.Children)
            {
                Util.UpdatePercentDone(rowCount);

                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                SqlInt16 landmarkID = SqlInt16.Parse(pair.Key.ToString());

                DataRow row = s_mapLandmarksDataTable.NewRow();
                s_mapLandmarksDataTable.Rows.Add(row);

                row[GenericConstant.LandmarkIDText] = landmarkID;
                row[GenericConstant.LandmarkNameText] = GetTextFor(GenericConstant.LandmarkNameText, "1000", cNode, landmarkID);
                row[GenericConstant.DescriptionText] = GetTextFor(GenericConstant.DescriptionText, "1001", cNode, landmarkID);
                row[GenericConstant.LocationIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.LocationIDText);
                row[GenericConstant.RadiusText] = SqlDouble.Null;
                row[GenericConstant.IconIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.IconIDText);

                AssignPosition(cNode, row, GenericConstant.PositionText, new[] { "x", "y", "z" });
            }

            s_sqlConnectionProvider.ImportDataBulk(s_mapLandmarksDataTable);

            Util.UpdatePercentDone(rowCount);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Gets the text for the specified field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="translationColumnID">The translation column identifier.</param>
        /// <param name="cNode">The child node.</param>
        /// <param name="landmarkID">The landmark identifier.</param>
        /// <returns></returns>
        private static SqlString GetTextFor(string field, string translationColumnID, YamlMappingNode cNode, SqlInt16 landmarkID)
        {
            SqlString landmarkName = cNode.Children.GetSqlTypeOrDefault<SqlString>(field);
            return landmarkName.IsNull
                ? GetTextFromTranslationsTable(translationColumnID, landmarkID)
                : landmarkName;
        }

        /// <summary>
        /// Gets the text from translations table.
        /// </summary>
        /// <param name="translationColumnID">The translation column identifier.</param>
        /// <param name="landmarkID">The landmark identifier.</param>
        /// <returns></returns>
        private static SqlString GetTextFromTranslationsTable(string translationColumnID, SqlInt16 landmarkID)
        {
            if (s_trnTranslationsDataTable == null)
            {
                string filename = Util.GetYamlFilesInBsdDirectory()
                    .FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == TableNamesConstant.TrnTranslationsTableName);

                if (string.IsNullOrEmpty(filename))
                    return SqlString.Null;

                YamlSequenceNode rNode = Util.ParsePerTableFileYamlFile(filename);

                if (rNode == null)
                    return SqlString.Null;

                string tableName = Path.GetFileNameWithoutExtension(filename);
                s_trnTranslationsDataTable = Util.GetDataTable(tableName, rNode, 0);
            }

            s_showLandmarkNameFromTranslationTableWarning = true;

            string filterExpression = $"languageID = 'EN-US' and tcID = {translationColumnID} and keyID = {landmarkID}";
            DataRow[] rows = s_trnTranslationsDataTable.Select(filterExpression);

            if (rows.Length > 1)
                return SqlString.Null;

            SqlString text = SqlString.Null;

            object val = rows.FirstOrDefault()[GenericConstant.TextText];
            if (val != null)
                return (SqlString)val;

            return text;
        }

        /// <summary>
        /// Checks the landmark data exists.
        /// </summary>
        /// <returns></returns>
        private static bool CheckLandmarkDataExists()
        {
            return s_sqlConnectionProvider.CheckTableExists(TableNamesConstant.TrnTranslationsTableName) ||
                   !string.IsNullOrEmpty(Util.GetYamlFilesInBsdDirectory()
                       .FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == TableNamesConstant.TrnTranslationsTableName));
        }

        /// <summary>
        /// Imports the universe data.
        /// </summary>
        private static void ImportUniverseData()
        {
            if (!Util.CheckUniverseDirectoryExists())
            {
                Console.WriteLine(
                    @"No universe yaml files where found. Ensure that the folder 'universe' exists under folder 'fsd'.");
                return;
            }

            if (!CheckNamesDataExists())
            {
                Console.WriteLine(
                    $@"{TableNamesConstant.InvNamesTableName} table or yaml file was not found. Universe data will not be imported.");
                Console.WriteLine(@"Universe data importation skipped.");
                Console.WriteLine();
                return;
            }

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            List<string> regions = Util.GetUniverseDirectorySubDirectories().ToList();

            if (!regions.Any())
            {
                Console.WriteLine(@"No region folders where found. Ensure that the folder 'universe' is not empty.");
                return;
            }

            Console.Write(ImportText);

            int tableCount = 0;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapRegionsTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapConstellationsTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapSolarSystemsTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapJumpsTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapLocationScenesTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapLocationWormholeClassesTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapCelestialStatisticsTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapDenormalizeTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapSolarSystemJumpsTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapConstellationJumpsTableName);
            tableCount++;
            s_sqlConnectionProvider.DropAndCreateTable(TableNamesConstant.MapRegionJumpsTableName);
            tableCount++;

            s_mapRegionsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapRegionsTableName);
            s_mapConstellationsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapConstellationsTableName);
            s_mapSolarSystemsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapSolarSystemsTableName);
            s_mapJumpsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapJumpsTableName);
            s_mapLocationScenesDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapLocationScenesTableName);
            s_mapLocationWormholeClassesDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapLocationWormholeClassesTableName);
            s_mapCelestialStatisticsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapCelestialStatisticsTableName);
            s_mapDenormalizeDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapDenormalizeTableName);
            s_mapSolarSystemJumpsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapSolarSystemJumpsTableName);
            s_mapConstellationJumpsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapConstellationJumpsTableName);
            s_mapRegionJumpsDataTable = Util.GetEmptyDataTable(TableNamesConstant.MapRegionJumpsTableName);
            
            double rowCount = Math.Round((double)regions.Count + tableCount, 0, MidpointRounding.AwayFromZero);

            if (!ImportRegions(regions, rowCount))
                return;

            // mapSolarSystemJumps
            ImportSolarSystemJumps();

            // mapConstellationJumps
            ImportConstellationJumps();

            // mapRegionJumps
            ImportRegionJumps();

            s_sqlConnectionProvider.ImportDataBulk(s_mapCelestialStatisticsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapConstellationJumpsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapConstellationsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapDenormalizeDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapJumpsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapLocationScenesDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapLocationWormholeClassesDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapRegionJumpsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapRegionsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapSolarSystemJumpsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);
            s_sqlConnectionProvider.ImportDataBulk(s_mapSolarSystemsDataTable, notify: false);
            Util.UpdatePercentDone(rowCount);

            Util.DisplayEndTime(stopwatch);
        }

        /// <summary>
        /// Imports the regions.
        /// </summary>
        /// <param name="regions">The regions.</param>
        /// <param name="rowCount">The row count.</param>
        /// <returns></returns>
        private static bool ImportRegions(IEnumerable<string> regions, double rowCount)
        {
            string importTextWithProgress = ImportText + Util.UpdatePercentDone(0);
            string previousText = string.Empty;

            foreach (string region in regions)
            {
                string filePath = Util.CheckStaticDataYamlFileExists(region, "region");
                if (!File.Exists(filePath))
                    continue;

                string filename = Path.GetFileName(filePath);
                DirectoryInfo parentDirectory = Directory.GetParent(filePath);

                string text = $"Parsing {filename} of '{parentDirectory.Name.ToSpaceSeparatedString()}'";

                Console.WriteLine();

                // Delete previous text
                Util.DeleteText(previousText);
                previousText = text;

                Console.Write(text);

                YamlMappingNode rNode = Util.ParseYamlFile(filePath);

                if (s_isClosing)
                    return false;

                Util.ResetConsoleCursorPosition(text);

                if (rNode == null)
                {
                    Console.WriteLine($@"Unable to parse {filename} of '{parentDirectory.Name.ToSpaceSeparatedString()}'");
                    return false;
                }

                text = $"Importing data of region '{parentDirectory.Name.ToSpaceSeparatedString()}'";
                
                // Delete previous text
                Util.DeleteText(previousText);
                previousText = text;

                Console.Write(text);

                Util.ResetConsoleCursorPosition(text);
                Console.SetCursorPosition(Console.CursorLeft + importTextWithProgress.Length, Console.CursorTop - 1);

                string progress = Util.UpdatePercentDone(rowCount);
                if (progress != string.Empty)
                    importTextWithProgress = ImportText + progress;

                ImportRegion(parentDirectory, rNode);
            }

            // Delete previous text
            Console.WriteLine();
            Util.DeleteText(previousText);
            Console.SetCursorPosition(importTextWithProgress.Length, Console.CursorTop - 1);
            return true;
        }

        /// <summary>
        /// Imports the solar system jumps.
        /// </summary>
        private static void ImportSolarSystemJumps()
        {
            IEnumerable solarSystemJumps = s_mapJumpsDataTable.Rows.Cast<DataRow>()
                .Join(s_mapDenormalizeDataTable.Rows.Cast<DataRow>(),
                    row => row[GenericConstant.StargateIDText],
                    denorm1 => denorm1[GenericConstant.ItemIDText],
                    (row, denorm1) => new { row, denorm1 })
                .Join(s_mapDenormalizeDataTable.Rows.Cast<DataRow>(),
                    a => a.row[GenericConstant.CelestialIDText],
                    denorm2 => denorm2[GenericConstant.ItemIDText],
                    (row, denorm2) => new { row, denorm2 })
                .OrderBy(a => a.row.denorm1[GenericConstant.SolarSystemIDText])
                .ThenBy(b => b.denorm2[GenericConstant.SolarSystemIDText])
                .Select(c => new
                {
                    FromRegionID = c.row.denorm1[GenericConstant.RegionIDText],
                    FromConstellationID = c.row.denorm1[GenericConstant.ConstellationIDText],
                    FromSolarSystemID = c.row.denorm1[GenericConstant.SolarSystemIDText],
                    ToSolarSystemID = c.denorm2[GenericConstant.SolarSystemIDText],
                    ToConstellationID = c.denorm2[GenericConstant.ConstellationIDText],
                    ToRegionID = c.denorm2[GenericConstant.RegionIDText]
                });

            foreach (dynamic jump in solarSystemJumps)
            {
                DataRow row = s_mapSolarSystemJumpsDataTable.NewRow();
                s_mapSolarSystemJumpsDataTable.Rows.Add(row);

                row[GenericConstant.FromRegionIDText] = jump.FromRegionID;
                row[GenericConstant.FromConstellationIDText] = jump.FromConstellationID;
                row[GenericConstant.FromSolarSystemIDText] = jump.FromSolarSystemID;
                row[GenericConstant.ToSolarSystemIDText] = jump.ToSolarSystemID;
                row[GenericConstant.ToConstellationIDText] = jump.ToConstellationID;
                row[GenericConstant.ToRegionIDText] = jump.ToRegionID;
            }
        }

        /// <summary>
        /// Imports the constellation jumps.
        /// </summary>
        private static void ImportConstellationJumps()
        {
            IEnumerable constellationJumps = s_mapJumpsDataTable.Rows.Cast<DataRow>()
                .Join(s_mapDenormalizeDataTable.Rows.Cast<DataRow>(),
                    row => row[GenericConstant.StargateIDText], denorm1 => denorm1[GenericConstant.ItemIDText],
                    (row, denorm1) => new { row, denorm1 })
                .Join(s_mapDenormalizeDataTable.Rows.Cast<DataRow>(),
                    a => a.row[GenericConstant.CelestialIDText], denorm2 => denorm2[GenericConstant.ItemIDText],
                    (row, denorm2) => new { row, denorm2 })
                .Where(
                    a =>
                        a.row.denorm1[GenericConstant.ConstellationIDText].ToString() !=
                        a.denorm2[GenericConstant.ConstellationIDText].ToString())
                .GroupBy(a => new
                {
                    FromRegionID = a.row.denorm1[GenericConstant.RegionIDText],
                    FromConstellationID = a.row.denorm1[GenericConstant.ConstellationIDText],
                    ToConstellationID = a.denorm2[GenericConstant.ConstellationIDText],
                    ToRegionID = a.denorm2[GenericConstant.RegionIDText]
                }, a => a.row.row)
                .OrderBy(grouped => grouped.Key.FromConstellationID)
                .ThenBy(grouped => grouped.Key.ToConstellationID)
                .Select(grouped => new
                {
                    grouped.Key.FromRegionID,
                    grouped.Key.FromConstellationID,
                    grouped.Key.ToConstellationID,
                    grouped.Key.ToRegionID
                });

            foreach (dynamic jump in constellationJumps)
            {
                DataRow row = s_mapConstellationJumpsDataTable.NewRow();
                s_mapConstellationJumpsDataTable.Rows.Add(row);

                row[GenericConstant.FromRegionIDText] = jump.FromRegionID;
                row[GenericConstant.FromConstellationIDText] = jump.FromConstellationID;
                row[GenericConstant.ToConstellationIDText] = jump.ToConstellationID;
                row[GenericConstant.ToRegionIDText] = jump.ToRegionID;
            }
        }

        /// <summary>
        /// Imports the region jumps.
        /// </summary>
        private static void ImportRegionJumps()
        {
            IEnumerable regionJumps = s_mapJumpsDataTable.Rows.Cast<DataRow>()
                .Join(s_mapDenormalizeDataTable.Rows.Cast<DataRow>(),
                    row => row[GenericConstant.StargateIDText],
                    denorm1 => denorm1[GenericConstant.ItemIDText],
                    (row, denorm1) => new { row, denorm1 })
                .Join(s_mapDenormalizeDataTable.Rows.Cast<DataRow>(),
                    a => a.row[GenericConstant.CelestialIDText],
                    denorm2 => denorm2[GenericConstant.ItemIDText],
                    (row, denorm2) => new { row, denorm2 })
                .Where(
                    a =>
                        a.row.denorm1[GenericConstant.RegionIDText].ToString() !=
                        a.denorm2[GenericConstant.RegionIDText].ToString())
                .GroupBy(a => new
                {
                    FromRegionID = a.row.denorm1[GenericConstant.RegionIDText],
                    ToRegionID = a.denorm2[GenericConstant.RegionIDText]
                }, a => a.row.row)
                .OrderBy(grouped => grouped.Key.FromRegionID)
                .ThenBy(grouped => grouped.Key.ToRegionID)
                .Select(grouped => new
                {
                    grouped.Key.FromRegionID,
                    grouped.Key.ToRegionID
                });

            foreach (dynamic jump in regionJumps)
            {
                DataRow row = s_mapRegionJumpsDataTable.NewRow();
                s_mapRegionJumpsDataTable.Rows.Add(row);

                row[GenericConstant.FromRegionIDText] = jump.FromRegionID;
                row[GenericConstant.ToRegionIDText] = jump.ToRegionID;
            }
        }

        /// <summary>
        /// Imports the region.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="rNode">The r node.</param>
        private static void ImportRegion(FileSystemInfo directory, YamlMappingNode rNode)
        {
            // mapRegions
            DataRow row = s_mapRegionsDataTable.NewRow();
            s_mapRegionsDataTable.Rows.Add(row);

            SqlInt32 regionID = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.RegionIDText);
            SqlInt32 factionID = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.FactionIDText);
            row[GenericConstant.RegionIDText] = regionID;
            row[GenericConstant.FactionIDText] = factionID;
            row[GenericConstant.RegionNameText] = directory.Name.ToSpaceSeparatedString();

            AssignPosition(rNode, row, GenericConstant.CenterText, new[] { "x", "y", "z" });
            AssignPosition(rNode, row, GenericConstant.MinText, new[] { "xMin", "yMin", "zMin" });
            AssignPosition(rNode, row, GenericConstant.MaxText, new[] { "xMax", "yMax", "zMax" });

            // 'radius' info for region is not included in the SDE
            // so we have to calculate it.
            // Unfortunately the numbers don't match with the legacy SDE
            // probably because of different implementation of Math.Sqrt
            // between .NET and Python (which CCP uses).
            // In case CCP decides to include it then we use that.
            SqlDouble radius = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText);
            radius = radius.IsNull ? GetRadius(row) : radius;
            row[GenericConstant.RadiusText] = radius;

            // mapLocationScenes
            ImportLocationScenes(rNode, regionID);

            // mapLocationWormholeClasses
            ImportLocationWormholeClasses(rNode, regionID);

            // mapDenormalize
            DenormalizeParameters denormParams = new DenormalizeParameters
            {
                TypeID = 3, // typeID for Region from invTypes 
                GroupID = 3, // groupID for Region from invGroups 
                ItemID = regionID,
                Radius = radius,
                ItemName = GetItemNameFor(GenericConstant.RegionNameText, rNode, regionID),
                PositionKey = GenericConstant.CenterText
            };
            ImportDenormalize(rNode, denormParams);

            ImportConstellations(directory, regionID, factionID);
        }

        /// <summary>
        /// Imports the constellations.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="factionID">The faction identifier.</param>
        private static void ImportConstellations(FileSystemInfo directory, SqlInt32 regionID, SqlInt32 factionID)
        {
            IEnumerable<string> constellations = Directory.GetDirectories(directory.FullName);
            foreach (string constellation in constellations)
            {
                string filePath = Util.CheckStaticDataYamlFileExists(constellation, "constellation");
                if (!File.Exists(filePath))
                    continue;

                YamlMappingNode rNode = Util.ParseYamlFile(filePath);

                if (s_isClosing)
                    return;

                if (rNode == null)
                    return;

                ImportConstellation(Directory.GetParent(filePath), regionID, factionID, rNode);
            }
        }

        /// <summary>
        /// Imports the constellation.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="factionID">The faction identifier.</param>
        /// <param name="rNode">The r node.</param>
        private static void ImportConstellation(FileSystemInfo directory, SqlInt32 regionID, SqlInt32 factionID,
            YamlMappingNode rNode)
        {
            DataRow row = s_mapConstellationsDataTable.NewRow();
            s_mapConstellationsDataTable.Rows.Add(row);

            SqlInt32 constellationID = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.ConstellationIDText);
            SqlDouble radius = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText);
            SqlInt32 constellationFactionID = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.FactionIDText);
            factionID = constellationFactionID.IsNull ? factionID : constellationFactionID;

            row[GenericConstant.ConstellationNameText] = directory.Name.ToSpaceSeparatedString();
            row[GenericConstant.RegionIDText] = regionID;
            row[GenericConstant.FactionIDText] = factionID;
            row[GenericConstant.ConstellationIDText] = constellationID;
            row[GenericConstant.RadiusText] = radius;

            AssignPosition(rNode, row, GenericConstant.CenterText, new[] { "x", "y", "z" });
            AssignPosition(rNode, row, GenericConstant.MinText, new[] { "xMin", "yMin", "zMin" });
            AssignPosition(rNode, row, GenericConstant.MaxText, new[] { "xMax", "yMax", "zMax" });

            // mapLocationWormholeClasses
            ImportLocationWormholeClasses(rNode, constellationID);

            // mapDenormalize
            DenormalizeParameters denormParams = new DenormalizeParameters
            {
                TypeID = 4, // typeID for Constellation from invTypes 
                GroupID = 4, // groupID for Constellation from invGroups 
                ItemID = constellationID,
                RegionID = regionID,
                Radius = radius,
                ItemName = GetItemNameFor(GenericConstant.ConstellationNameText, rNode, constellationID),
                PositionKey = GenericConstant.CenterText
            };
            ImportDenormalize(rNode, denormParams);

            ImportSolarSystems(directory, regionID, constellationID, factionID);
        }

        /// <summary>
        /// Imports the solar systems.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="constellationID">The constellation identifier.</param>
        /// <param name="factionID">The faction identifier.</param>
        private static void ImportSolarSystems(FileSystemInfo directory, SqlInt32 regionID, SqlInt32 constellationID,
            SqlInt32 factionID)
        {
            IEnumerable<string> solarSystems = Directory.GetDirectories(directory.FullName);
            foreach (string solarSystem in solarSystems)
            {
                string filePath = Util.CheckStaticDataYamlFileExists(solarSystem, "solarsystem");
                if (!File.Exists(filePath))
                    continue;

                YamlMappingNode rNode = Util.ParseYamlFile(filePath);

                if (s_isClosing)
                    return;

                if (rNode == null)
                    return;

                ImportSolarSystem(Directory.GetParent(filePath), regionID, constellationID, factionID, rNode);
            }
        }

        /// <summary>
        /// Imports the solar system.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="constellationID">The constellation identifier.</param>
        /// <param name="factionID">The faction identifier.</param>
        /// <param name="rNode">The r node.</param>
        private static void ImportSolarSystem(FileSystemInfo directory, SqlInt32 regionID, SqlInt32 constellationID,
            SqlInt32 factionID, YamlMappingNode rNode)
        {
            DataRow row = s_mapSolarSystemsDataTable.NewRow();
            s_mapSolarSystemsDataTable.Rows.Add(row);

            SqlInt32 solarSystemID = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.SolarSystemIDText);
            SqlDouble radius = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText);
            SqlDouble security = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.SecurityText);
            SqlInt32 solarSystemFactionID = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.FactionIDText);
            factionID = solarSystemFactionID.IsNull ? factionID : solarSystemFactionID;
            string solarSystemName = directory.Name.ToSpaceSeparatedString();

            row[GenericConstant.SolarSystemNameText] = solarSystemName;
            row[GenericConstant.RegionIDText] = regionID;
            row[GenericConstant.FactionIDText] = factionID;
            row[GenericConstant.ConstellationIDText] = constellationID;
            row[GenericConstant.SolarSystemIDText] = solarSystemID;
            row[GenericConstant.RadiusText] = radius;
            row[GenericConstant.LuminosityText] = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.LuminosityText);
            row[GenericConstant.BorderText] = rNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.BorderText);
            row[GenericConstant.FringeText] = rNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.FringeText);
            row[GenericConstant.CorridorText] = rNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.CorridorText);
            row[GenericConstant.HubText] = rNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.HubText);
            row[GenericConstant.InternationalText] =
                rNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.InternationalText);
            row[GenericConstant.RegionalText] = rNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.RegionalText);
            row[GenericConstant.SecurityText] = security;
            row[GenericConstant.SunTypeIDText] = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.SunTypeIDText);
            row[GenericConstant.SecurityClassText] =
                rNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.SecurityClassText);
            row["constellation"] = false;

            AssignPosition(rNode, row, GenericConstant.CenterText, new[] { "x", "y", "z" });
            AssignPosition(rNode, row, "min", new[] { "xMin", "yMin", "zMin" });
            AssignPosition(rNode, row, "max", new[] { "xMax", "yMax", "zMax" });

            // mapJumps
            ImportJumps(rNode);

            // mapLocationWormholeClasses
            ImportLocationWormholeClasses(rNode, solarSystemID);

            // mapDenormalize
            DenormalizeParameters denormParams = new DenormalizeParameters
            {
                TypeID = 5, // typeID for Solar System from invTypes 
                GroupID = 5, // groupID for Solar System from invGroups 
                ItemID = solarSystemID,
                RegionID = regionID,
                ConstellationID = constellationID,
                Radius = radius,
                Security = security,
                ItemName = GetItemNameFor(GenericConstant.SolarSystemNameText, rNode, solarSystemID),
                PositionKey = GenericConstant.CenterText
            };
            ImportDenormalize(rNode, denormParams);

            // star
            ImportStar(rNode, regionID, constellationID, solarSystemID);

            // planets
            ImportPlanets(rNode, regionID, constellationID, solarSystemID);

            // secondary sun
            ImportSecondarySun(rNode, regionID, constellationID, solarSystemID);

            // stargates
            ImportStargates(rNode, regionID, constellationID, solarSystemID);
        }

        /// <summary>
        /// Imports the star.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="constellationID">The constellation identifier.</param>
        /// <param name="solarSystemID">The solar system identifier.</param>
        private static void ImportStar(YamlMappingNode rNode, SqlInt32 regionID, SqlInt32 constellationID,
            SqlInt32 solarSystemID)
        {
            YamlMappingNode cNode = rNode.Children[new YamlScalarNode(GenericConstant.StarText)] as YamlMappingNode;
            if (cNode == null)
                return;

            YamlNode celestialID = cNode.Children[new YamlScalarNode(GenericConstant.IDText)];
            var pair = new KeyValuePair<YamlNode, YamlNode>(celestialID, new YamlScalarNode());
            ImportStatistics(pair, cNode);

            SqlInt32 itemID = SqlInt32.Parse(celestialID.ToString());

            // mapDenormalize
            DenormalizeParameters denormParams = new DenormalizeParameters
            {
                TypeID = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText),
                GroupID = 6, // groupID for Sun from invGroups
                ItemID = itemID,
                RegionID = regionID,
                ConstellationID = constellationID,
                SolarSystemID = solarSystemID,
                Radius = cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText),
                Security = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.SecurityText),
                CelestialIndex = cNode.Children.GetSqlTypeOrDefault<SqlByte>(GenericConstant.CelestialIndexText),
                ItemName = GetItemNameFor(GenericConstant.NameText, rNode, itemID)
            };
            ImportDenormalize(cNode, denormParams);
        }

        /// <summary>
        /// Imports the planets.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="constellationID">The constellation identifier.</param>
        /// <param name="solarSystemID">The solar system identifier.</param>
        private static void ImportPlanets(YamlMappingNode rNode, SqlInt32 regionID, SqlInt32 constellationID,
            SqlInt32 solarSystemID)
        {
            YamlMappingNode planetsNode = rNode.Children[new YamlScalarNode(GenericConstant.PlanetsText)] as YamlMappingNode;
            if (planetsNode == null)
                return;

            YamlMappingNode starNode = (YamlMappingNode)rNode.Children[new YamlScalarNode(GenericConstant.StarText)];
            SqlInt32 orbitID = SqlInt32.Parse(starNode.Children[new YamlScalarNode(GenericConstant.IDText)].ToString());

            foreach (KeyValuePair<YamlNode, YamlNode> pair in planetsNode.Children)
            {
                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                ImportStatistics(pair, cNode);

                SqlInt32 planetID = SqlInt32.Parse(pair.Key.ToString());
                SqlString itemName = GetItemNameFor(GenericConstant.NameText, rNode, planetID);

                if (itemName.IsNull && !s_showMissingDataWarning)
                    s_showMissingDataWarning = true;

                // mapDenormalize
                DenormalizeParameters denormParams = new DenormalizeParameters
                {
                    TypeID = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText),
                    GroupID = 7, // groupID for Planet from invGroups 
                    ItemID = planetID,
                    RegionID = regionID,
                    ConstellationID = constellationID,
                    SolarSystemID = solarSystemID,
                    Security = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.SecurityText),
                    OrbitID = orbitID,
                    Radius = cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText),
                    CelestialIndex = cNode.Children.GetSqlTypeOrDefault<SqlByte>(GenericConstant.CelestialIndexText),
                    ItemName = itemName
                };

                ImportDenormalize(cNode, denormParams);

                // moons
                denormParams.OrbitID = planetID;
                ImportMoons(cNode, denormParams);

                // asteroidBelts
                denormParams.OrbitID = planetID;
                ImportAsteroidBelts(cNode, denormParams);

                // stations
                denormParams.OrbitID = planetID;
                ImportStations(cNode, denormParams);
            }
        }

        /// <summary>
        /// Imports the moons.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="denormParams">The denorm parameters.</param>
        private static void ImportMoons(YamlMappingNode rNode, DenormalizeParameters denormParams)
        {
            if (!rNode.Children.ContainsKey(new YamlScalarNode(GenericConstant.MoonsText)))
                return;

            YamlMappingNode moonsNode = rNode.Children[new YamlScalarNode(GenericConstant.MoonsText)] as YamlMappingNode;

            if (moonsNode == null)
                return;

            SqlInt32 planetID = denormParams.OrbitID;

            foreach (KeyValuePair<YamlNode, YamlNode> pair in moonsNode.Children)
            {
                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                ImportStatistics(pair, cNode);

                SqlInt32 itemID = SqlInt32.Parse(pair.Key.ToString());
                SqlString itemName = GetItemNameFor(GenericConstant.MoonNameText, rNode, itemID);
                SqlByte orbitIndex = cNode.Children.GetSqlTypeOrDefault<SqlByte>(GenericConstant.OrbitIndexText);

                if (itemName.IsNull && !s_showMissingDataWarning)
                    s_showMissingDataWarning = true;

                // mapDenormalize
                denormParams.TypeID = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText);
                denormParams.GroupID = 8; // groupID for Moon from invGroups 
                denormParams.ItemID = itemID;
                denormParams.Radius = cNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText);
                denormParams.OrbitIndex = orbitIndex.IsNull ? ParseOrbitIndexFrom(itemName) : orbitIndex;
                denormParams.OrbitID = planetID;
                denormParams.ItemName = itemName;

                ImportDenormalize(cNode, denormParams);

                // stations
                denormParams.OrbitID = SqlInt32.Parse(pair.Key.ToString());
                ImportStations(cNode, denormParams);
            }
        }

        /// <summary>
        /// Imports the asteroid belts.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="denormParams">The denorm parameters.</param>
        private static void ImportAsteroidBelts(YamlMappingNode rNode, DenormalizeParameters denormParams)
        {
            if (!rNode.Children.ContainsKey(new YamlScalarNode(GenericConstant.AsteroidBeltsText)))
                return;

            YamlMappingNode asteroidBeltsNode =
                rNode.Children[new YamlScalarNode(GenericConstant.AsteroidBeltsText)] as YamlMappingNode;

            if (asteroidBeltsNode == null)
                return;

            foreach (KeyValuePair<YamlNode, YamlNode> pair in asteroidBeltsNode.Children)
            {
                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                ImportStatistics(pair, cNode);

                SqlInt32 itemID = SqlInt32.Parse(pair.Key.ToString());
                SqlString itemName = GetItemNameFor(GenericConstant.AsteroidBeltNameText, rNode, itemID);
                SqlByte orbitIndex = cNode.Children.GetSqlTypeOrDefault<SqlByte>(GenericConstant.OrbitIndexText);

                if (itemName.IsNull && !s_showMissingDataWarning)
                    s_showMissingDataWarning = true;

                // mapDenormalize
                denormParams.TypeID = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText);
                denormParams.GroupID = 9; // groupID for Asteroid Belt from invGroups 
                denormParams.ItemID = itemID;
                denormParams.Radius = 1;
                denormParams.OrbitIndex = denormParams.OrbitIndex = orbitIndex.IsNull ? ParseOrbitIndexFrom(itemName) : orbitIndex;
                denormParams.ItemName = itemName;

                ImportDenormalize(cNode, denormParams);
            }
        }

        /// <summary>
        /// Imports the stations.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="denormParams">The denorm parameters.</param>
        private static void ImportStations(YamlMappingNode rNode, DenormalizeParameters denormParams)
        {
            if (!rNode.Children.ContainsKey(new YamlScalarNode(GenericConstant.NpcStationsText)))
                return;

            YamlMappingNode npcStationsNode =
                rNode.Children[new YamlScalarNode(GenericConstant.NpcStationsText)] as YamlMappingNode;

            if (npcStationsNode == null)
                return;

            foreach (KeyValuePair<YamlNode, YamlNode> pair in npcStationsNode.Children)
            {
                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                SqlInt32 itemID = SqlInt32.Parse(pair.Key.ToString());
                SqlString itemName = GetItemNameFor(GenericConstant.OwnerNameText, rNode, itemID);

                if (itemName.IsNull && !s_showMissingDataWarning)
                    s_showMissingDataWarning = true;

                // mapDenormalize
                DenormalizeParameters newDenormParams = new DenormalizeParameters
                {
                    TypeID = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText),
                    GroupID = 15, // groupID for Station from invGroups
                    ItemID = itemID,
                    RegionID = denormParams.RegionID,
                    ConstellationID = denormParams.ConstellationID,
                    SolarSystemID = denormParams.SolarSystemID,
                    OrbitID = denormParams.OrbitID,
                    Security = denormParams.Security,
                    ItemName = itemName
                };

                ImportDenormalize(cNode, newDenormParams);
            }
        }

        /// <summary>
        /// Imports the secondary sun.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="constellationID">The constellation identifier.</param>
        /// <param name="solarSystemID">The solar system identifier.</param>
        private static void ImportSecondarySun(YamlMappingNode rNode, SqlInt32 regionID, SqlInt32 constellationID,
            SqlInt32 solarSystemID)
        {
            if (!rNode.Children.ContainsKey(new YamlScalarNode(GenericConstant.SecondarySunText)))
                return;

            YamlMappingNode cNode = rNode.Children[new YamlScalarNode(GenericConstant.SecondarySunText)] as YamlMappingNode;
            if (cNode == null)
                return;

            YamlNode itemIDNode = cNode.Children[new YamlScalarNode(GenericConstant.ItemIDText)];

            // mapDenormalize
            DenormalizeParameters denormParams = new DenormalizeParameters
            {
                TypeID = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText),
                GroupID = 995, // groupID for Secondary Sun from invGroups
                ItemID = SqlInt32.Parse(itemIDNode.ToString()),
                RegionID = regionID,
                ConstellationID = constellationID,
                SolarSystemID = solarSystemID,
                Security = 0,
                ItemName = "Unknown Anomaly"
            };
            ImportDenormalize(cNode, denormParams);
        }

        /// <summary>
        /// Imports the stargates.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="regionID">The region identifier.</param>
        /// <param name="constellationID">The constellation identifier.</param>
        /// <param name="solarSystemID">The solar system identifier.</param>
        private static void ImportStargates(YamlMappingNode rNode, SqlInt32 regionID, SqlInt32 constellationID,
            SqlInt32 solarSystemID)
        {
            YamlMappingNode stargatesNode = rNode.Children[new YamlScalarNode(GenericConstant.StargatesText)] as YamlMappingNode;
            if (stargatesNode == null)
                return;

            foreach (KeyValuePair<YamlNode, YamlNode> pair in stargatesNode.Children)
            {
                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                // mapDenormalize
                DenormalizeParameters denormParams = new DenormalizeParameters
                {
                    TypeID = cNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.TypeIDText),
                    GroupID = 10, // groupID for Stargate from invGroups
                    ItemID = SqlInt32.Parse(pair.Key.ToString()),
                    RegionID = regionID,
                    ConstellationID = constellationID,
                    SolarSystemID = solarSystemID,
                    Security = rNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.SecurityText)
                };
                ImportDenormalize(cNode, denormParams);
            }
        }

        /// <summary>
        /// Imports the denormalize.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="denormParams">The denorm parameters.</param>
        private static void ImportDenormalize(YamlMappingNode rNode, DenormalizeParameters denormParams)
        {
            DataRow row = s_mapDenormalizeDataTable.NewRow();
            s_mapDenormalizeDataTable.Rows.Add(row);

            row[GenericConstant.ItemIDText] = denormParams.ItemID;
            row[GenericConstant.TypeIDText] = denormParams.TypeID;
            row[GenericConstant.GroupIDText] = denormParams.GroupID;
            row[GenericConstant.SolarSystemIDText] = denormParams.SolarSystemID;
            row[GenericConstant.ConstellationIDText] = denormParams.ConstellationID;
            row[GenericConstant.RegionIDText] = denormParams.RegionID;
            row[GenericConstant.OrbitIDText] = denormParams.OrbitID;
            row[GenericConstant.RadiusText] = denormParams.Radius;
            row[GenericConstant.ItemNameText] = denormParams.ItemName;
            row[GenericConstant.SecurityText] = denormParams.Security;
            row[GenericConstant.CelestialIndexText] = denormParams.CelestialIndex;
            row[GenericConstant.OrbitIndexText] = denormParams.OrbitIndex;

            AssignPosition(rNode, row, denormParams.PositionKey, new[] { "x", "y", "z" });
        }

        /// <summary>
        /// Gets the item name for the specified field.
        /// </summary>
        /// <param name="field">The field.</param>
        /// <param name="cNode">The c node.</param>
        /// <param name="itemID">The item identifier.</param>
        /// <returns></returns>
        private static SqlString GetItemNameFor(string field, YamlMappingNode cNode, SqlInt32 itemID)
        {
            SqlString itemName = cNode.Children.GetSqlTypeOrDefault<SqlString>(field);
            return itemName.IsNull
                ? GetItemNameFromNamesTable(itemID)
                : itemName;
        }

        /// <summary>
        /// Gets the item name from names table.
        /// </summary>
        /// <param name="itemID">The item identifier.</param>
        /// <returns></returns>
        private static SqlString GetItemNameFromNamesTable(SqlInt32 itemID)
        {
            if (s_invNamesDataTable == null)
            {
                if (s_sqlConnectionProvider.CheckTableExists(TableNamesConstant.InvNamesTableName))
                {
                    s_invNamesDataTable = s_sqlConnectionProvider.GetDataTable(TableNamesConstant.InvNamesTableName);
                }
                else
                {
                    string filename = Util.GetYamlFilesInBsdDirectory()
                        .FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == TableNamesConstant.InvNamesTableName);

                    if (string.IsNullOrEmpty(filename))
                        return SqlString.Null;

                    YamlSequenceNode rNode = Util.ParsePerTableFileYamlFile(filename);

                    if (rNode == null)
                        return SqlString.Null;

                    string tableName = Path.GetFileNameWithoutExtension(filename);
                    s_invNamesDataTable = Util.GetDataTable(tableName, rNode, 0);
                }
            }

            s_showItemNameFromNamesTableWarning = true;

            string filterExpression = $"itemID = {itemID}";
            DataRow[] rows = s_invNamesDataTable.Select(filterExpression);

            if (rows.Length != 1)
                return SqlString.Null;

            object val = rows.FirstOrDefault()[GenericConstant.ItemNameText];

            if (val == null)
                return SqlString.Null;

            string text = val as string;
            return text != null
                ? new SqlString(text)
                : val as SqlString? ?? SqlString.Null;
        }

        /// <summary>
        /// Checks the names data exists.
        /// </summary>
        /// <returns></returns>
        private static bool CheckNamesDataExists()
        {
            return s_sqlConnectionProvider.CheckTableExists(TableNamesConstant.InvNamesTableName) ||
                   !string.IsNullOrEmpty(Util.GetYamlFilesInBsdDirectory()
                       .FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == TableNamesConstant.InvNamesTableName));
        }

        /// <summary>
        /// Imports the location scenes.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="regionID">The region identifier.</param>
        private static void ImportLocationScenes(YamlMappingNode rNode, SqlInt32 regionID)
        {
            DataRow row = s_mapLocationScenesDataTable.NewRow();
            s_mapLocationScenesDataTable.Rows.Add(row);

            row[GenericConstant.LocationIDText] = regionID;
            row[GenericConstant.GraphicIDText] = rNode.Children.GetSqlTypeOrDefault<SqlInt32>("nebula");
        }

        /// <summary>
        /// Imports the location wormhole classes.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="locationID">The location identifier.</param>
        private static void ImportLocationWormholeClasses(YamlMappingNode rNode, SqlInt32 locationID)
        {
            SqlInt32 wormholeClassID = rNode.Children.GetSqlTypeOrDefault<SqlInt32>(GenericConstant.WormholeClassIDText);

            if (wormholeClassID.IsNull)
                return;

            DataRow row = s_mapLocationWormholeClassesDataTable.NewRow();
            s_mapLocationWormholeClassesDataTable.Rows.Add(row);

            row[GenericConstant.LocationIDText] = locationID;
            row[GenericConstant.WormholeClassIDText] =
                rNode.Children.GetSqlTypeOrDefault<SqlByte>(GenericConstant.WormholeClassIDText);
        }

        /// <summary>
        /// Imports the statistics.
        /// </summary>
        /// <param name="pair">The pair.</param>
        /// <param name="node">The node.</param>
        private static void ImportStatistics(KeyValuePair<YamlNode, YamlNode> pair, YamlMappingNode node)
        {
            if (!node.Children.ContainsKey(new YamlScalarNode(GenericConstant.StatisticsText)))
                return;

            DataRow row = s_mapCelestialStatisticsDataTable.NewRow();
            s_mapCelestialStatisticsDataTable.Rows.Add(row);

            row[GenericConstant.CelestialIDText] = SqlInt32.Parse(pair.Key.ToString());

            YamlMappingNode statNode = node.Children[new YamlScalarNode(GenericConstant.StatisticsText)] as YamlMappingNode;

            if (statNode == null)
                return;

            row[GenericConstant.TemperatureText] =
                statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.TemperatureText);
            row[GenericConstant.SpectralClassText] =
                statNode.Children.GetSqlTypeOrDefault<SqlString>(GenericConstant.SpectralClassText);
            row[GenericConstant.LuminosityText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(
                GenericConstant.LuminosityText, defaultValue: "0");
            row[GenericConstant.AgeText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.AgeText,
                defaultValue: "0");
            row[GenericConstant.LifeText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.LifeText);
            row[GenericConstant.OrbitRadiusText] =
                statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.OrbitRadiusText, defaultValue: "0");
            row[GenericConstant.EccentricityText] =
                statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.EccentricityText, defaultValue: "0");
            row[GenericConstant.MassDustText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.MassDustText,
                defaultValue: "0");
            row[GenericConstant.MassGasText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.MassGasText,
                defaultValue: "0");
            row[GenericConstant.FragmentedText] = statNode.Children.GetSqlTypeOrDefault<SqlBoolean>(
                GenericConstant.FragmentedText, defaultValue: "0");
            row[GenericConstant.DensityText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.DensityText,
                defaultValue: "0");
            row[GenericConstant.SurfaceGravityText] =
                statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.SurfaceGravityText, defaultValue: "0");
            row[GenericConstant.EscapeVelocityText] =
                statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.EscapeVelocityText, defaultValue: "0");
            row[GenericConstant.OrbitPeriodText] =
                statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.OrbitPeriodText, defaultValue: "0");
            row[GenericConstant.RotationRateText] =
                statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RotationRateText, defaultValue: "0");
            row[GenericConstant.LockedText] = statNode.Children.GetSqlTypeOrDefault<SqlBoolean>(GenericConstant.LockedText,
                defaultValue: "0");
            row[GenericConstant.PressureText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.PressureText,
                defaultValue: "0");
            row[GenericConstant.RadiusText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.RadiusText);
            row[GenericConstant.MassText] = statNode.Children.GetSqlTypeOrDefault<SqlDouble>(GenericConstant.MassText,
                defaultValue: "0");
        }

        /// <summary>
        /// Imports the jumps.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        private static void ImportJumps(YamlMappingNode rNode)
        {
            YamlMappingNode stargatesNode = rNode.Children[new YamlScalarNode(GenericConstant.StargatesText)] as YamlMappingNode;

            if (stargatesNode == null)
                return;

            foreach (KeyValuePair<YamlNode, YamlNode> pair in stargatesNode.Children)
            {
                YamlMappingNode cNode = pair.Value as YamlMappingNode;

                if (cNode == null)
                    continue;

                DataRow row = s_mapJumpsDataTable.NewRow();
                s_mapJumpsDataTable.Rows.Add(row);

                row[GenericConstant.StargateIDText] = SqlInt32.Parse(pair.Key.ToString());
                row[GenericConstant.CelestialIDText] = cNode.Children.GetSqlTypeOrDefault<SqlInt32>("destination");
            }
        }

        /// <summary>
        /// Assigns the position.
        /// </summary>
        /// <param name="rNode">The r node.</param>
        /// <param name="row">The row.</param>
        /// <param name="key">The key.</param>
        /// <param name="parameters">The parameters.</param>
        private static void AssignPosition(YamlMappingNode rNode, DataRow row, string key, string[] parameters)
        {
            if (!rNode.Children.ContainsKey(new YamlScalarNode(key)))
            {
                foreach (string parameter in parameters)
                {
                    row[parameter] = SqlDouble.Zero;
                }

                return;
            }

            YamlSequenceNode position = rNode.Children[new YamlScalarNode(key)] as YamlSequenceNode;
            if (position == null || position.Count() != 3)
                return;

            foreach (string parameter in parameters)
            {
                int index = Array.IndexOf(parameters, parameter);
                row[parameter] = SqlDouble.Parse(position.Children[index].ToString());
            }
        }

        /// <summary>
        /// Gets the radius.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <returns></returns>
        private static SqlDouble GetRadius(DataRow row)
        {
            double xMin = double.Parse(row["xMin"].ToString());
            double xMax = double.Parse(row["xMax"].ToString());
            double yMin = double.Parse(row["yMin"].ToString());
            double yMax = double.Parse(row["yMax"].ToString());
            double zMin = double.Parse(row["zMin"].ToString());
            double zMax = double.Parse(row["zMax"].ToString());

            double radius = Math.Sqrt(Math.Pow(xMax - xMin, 2) + Math.Pow(yMax - yMin, 2) + Math.Pow(zMax - zMin, 2)) / 2;

            return radius;
        }

        /// <summary>
        /// To the space separated string.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        private static string ToSpaceSeparatedString(this string name)
        {
            return Regex.Replace(name, "((?<=[a-z])(?=[A-Z]|(?:ofthe)|(?:the[A-Z])))", " ");
        }

        /// <summary>
        /// Parses the orbit index from the specified item name.
        /// </summary>
        /// <param name="itemName">Name of the item.</param>
        /// <returns></returns>
        private static SqlByte ParseOrbitIndexFrom(SqlString itemName)
        {
            // Special case for Kor-Azor Prime IV (Eclipticum) - Moons
            switch (itemName.Value)
            {
                case "Kor-Azor Prime IV (Eclipticum) - Moon Griklaeum":
                    return 1;
                case "Kor-Azor Prime IV (Eclipticum) - Moon Black Viperia":
                    return 2;
                case "Kor-Azor Prime IV (Eclipticum) - Moon Kileakum":
                    return 3;
            }

            Match match = Regex.Match(itemName.ToString(), ".*\\s-\\s(?:Moon|Asteroid Belt)\\s(?<index>\\d+)",
                RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);

            if (match.Success && match.Groups.Count == 2)
                return SqlByte.Parse(match.Groups["index"].Value);

            return SqlByte.Null;
        }

        /// <summary>
        /// To the roman letter.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <returns></returns>
        private static string ToRomanLetter(this object o)
        {
            string roman = string.Empty;
            int num;
            if (!int.TryParse(o.ToString(), out num))
                return roman;

            string[] ones = { "", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
            string[] ten = { "", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC" };
            roman += ten[(num / 10) % 10];
            roman += ones[num % 10];
            return roman;
        }

        private class DenormalizeParameters
        {
            /// <summary>
            /// Gets or sets the item identifier.
            /// </summary>
            /// <value>
            /// The item identifier.
            /// </value>
            internal SqlInt32 ItemID { get; set; }

            /// <summary>
            /// Gets or sets the type identifier.
            /// </summary>
            /// <value>
            /// The type identifier.
            /// </value>
            internal SqlInt32 TypeID { get; set; }

            /// <summary>
            /// Gets or sets the radius.
            /// </summary>
            /// <value>
            /// The radius.
            /// </value>
            internal SqlDouble Radius { get; set; }

            /// <summary>
            /// Gets or sets the index of the celestial.
            /// </summary>
            /// <value>
            /// The index of the celestial.
            /// </value>
            internal SqlByte CelestialIndex { get; set; }

            /// <summary>
            /// Gets or sets the index of the orbit.
            /// </summary>
            /// <value>
            /// The index of the orbit.
            /// </value>
            internal SqlByte OrbitIndex { get; set; }

            /// <summary>
            /// Gets or sets the group identifier.
            /// </summary>
            /// <value>
            /// The group identifier.
            /// </value>
            internal SqlInt32 GroupID { get; set; }

            /// <summary>
            /// Gets or sets the name of the item.
            /// </summary>
            /// <value>
            /// The name of the item.
            /// </value>
            internal SqlString ItemName { get; set; }

            /// <summary>
            /// Gets or sets the region identifier.
            /// </summary>
            /// <value>
            /// The region identifier.
            /// </value>
            internal SqlInt32 RegionID { get; set; }

            /// <summary>
            /// Gets or sets the constellation identifier.
            /// </summary>
            /// <value>
            /// The constellation identifier.
            /// </value>
            internal SqlInt32 ConstellationID { get; set; }

            /// <summary>
            /// Gets or sets the solar system identifier.
            /// </summary>
            /// <value>
            /// The solar system identifier.
            /// </value>
            internal SqlInt32 SolarSystemID { get; set; }

            /// <summary>
            /// Gets or sets the security.
            /// </summary>
            /// <value>
            /// The security.
            /// </value>
            internal SqlDouble Security { get; set; }

            /// <summary>
            /// Gets or sets the orbit identifier.
            /// </summary>
            /// <value>
            /// The orbit identifier.
            /// </value>
            internal SqlInt32 OrbitID { get; set; }

            /// <summary>
            /// Gets or sets the position key.
            /// </summary>
            /// <value>
            /// The position key.
            /// </value>
            internal string PositionKey { get; set; } = GenericConstant.PositionText;
        }
    }
}
