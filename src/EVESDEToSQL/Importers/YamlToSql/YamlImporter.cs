﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Diagnostics;
using EVESDEToSQL.Importers.YamlToSQL.Importation;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;

namespace EVESDEToSQL.Importers.YamlToSQL
{
    internal class YamlImporter : IImporter
    {
        private readonly DbConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        /// <summary>
        /// Initializes a new instance of the <see cref="YamlImporter"/> class.
        /// </summary>
        internal YamlImporter(DbConnectionProvider sqlConnectionProvider)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));

            m_sqlConnectionProvider = sqlConnectionProvider;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Imports the data.
        /// </summary>
        public void ImportData()
        {
            if (m_isClosing)
                return;

            if (Util.CheckSDEDirectoryExists())
            {
                DataTablePerYamlFile.Import(m_sqlConnectionProvider);
                UniverseDataPerYamlFile.Import(m_sqlConnectionProvider);
            }

            if (Debugger.IsAttached)
                Types.Import(m_sqlConnectionProvider);
            else
            {
                Categories.Import(m_sqlConnectionProvider);
                Groups.Import(m_sqlConnectionProvider);
                Graphics.Import(m_sqlConnectionProvider);
                Icons.Import(m_sqlConnectionProvider);
                Skins.Import(m_sqlConnectionProvider);
                SkinMaterials.Import(m_sqlConnectionProvider);
                SkinLicenses.Import(m_sqlConnectionProvider);
                Types.Import(m_sqlConnectionProvider);
                Certificates.Import(m_sqlConnectionProvider);
                Blueprints.Import(m_sqlConnectionProvider);
            }

            Console.WriteLine();
        }
    }
}
