﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Diagnostics;
using System.Linq;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL.Importers.DataDumpToSQL
{
    internal class DataDumpImporter : IImporter
    {
        private readonly SqlConnectionProvider m_sqlConnectionProvider;

        private bool m_isClosing;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataDumpImporter"/> class.
        /// </summary>
        /// <param name="sqlConnectionProvider">The SQL connection provider.</param>
        /// <param name="restore">The restore.</param>
        /// <exception cref="System.ArgumentNullException">
        /// sqlConnectionProvider
        /// or
        /// restore
        /// </exception>
        internal DataDumpImporter(DbConnectionProvider sqlConnectionProvider, Restore restore)
        {
            if (sqlConnectionProvider == null)
                throw new ArgumentNullException(nameof(sqlConnectionProvider));
            if (restore == null)
                throw new ArgumentNullException(nameof(restore));

            m_sqlConnectionProvider = (SqlConnectionProvider)sqlConnectionProvider;
            Restore = restore;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets the restore.
        /// </summary>
        /// <value>
        /// The restore.
        /// </value>
        internal Restore Restore { get; }

        /// <summary>
        /// Imports the data.
        /// </summary>
        public void ImportData()
        {
            if (m_isClosing)
                return;

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            string filePath = Util.CheckDataDumpExists().Single();
            
            string actionText = $"Importing SQL data dump to '{m_sqlConnectionProvider.Connection.Database}' database... ";

            Console.Write(actionText);

            try
            {
                RestoreData(filePath);
            }
            catch (Exception ex)
            {
                string failText =
                    $"Importing SQL data dump to '{m_sqlConnectionProvider.Connection.Database}' database: Failed\n{ex.Message}";

                Util.HandleExceptionWithReason(actionText, failText, ex.GetBaseExceptionMessage());
            }

            Util.DisplayEndTime(stopwatch);

            Console.WriteLine();
        }

        /// <summary>
        /// Restores the data.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        private void RestoreData(string filePath)
        {
            Server server = m_sqlConnectionProvider.Server;

            string defaultDataPath = string.IsNullOrEmpty(server.Settings.DefaultFile)
                ? server.MasterDBPath
                : server.Settings.DefaultFile;
            string defaultLogPath = string.IsNullOrEmpty(server.Settings.DefaultLog)
                ? server.MasterDBLogPath
                : server.Settings.DefaultLog;

            Restore.Database = m_sqlConnectionProvider.Connection.Database;
            Restore.ReplaceDatabase = true;
            Restore.PercentCompleteNotification = 1;
            Restore.PercentComplete += Restore_PercentComplete;
            Restore.Devices.AddDevice(filePath, DeviceType.File);
            Restore.RelocateFiles.AddRange(
                new[]
                {
                    new RelocateFile("ebs_DATADUMP",
                        $"{defaultDataPath}{Restore.Database}.mdf"),
                    new RelocateFile("ebs_DATADUMP_log",
                        $"{defaultLogPath}{Restore.Database}_log.ldf")
                });

            if (m_isClosing)
                return;

            Restore.SqlRestore(server);
            Restore.Devices.Clear();
        }

        /// <summary>
        /// Handles the PercentComplete event of the Restore control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="PercentCompleteEventArgs"/> instance containing the event data.</param>
        private static void Restore_PercentComplete(object sender, PercentCompleteEventArgs e)
        {
            Util.UpdatePercentDone(100);
        }
    }
}
