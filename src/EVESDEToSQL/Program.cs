﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Utils;

namespace EVESDEToSQL
{
    internal class Program
    {
        private static SafeNativeMethods.EventHandler s_handler;
        private static string s_assemblyName;

        /// <summary>
        /// Gets or sets a value indicating whether this instance is debug build.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is debug build; otherwise, <c>false</c>.
        /// </value>
        internal static bool IsDebugBuild { get; private set; }

        /// <summary>
        /// The entry point.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            CheckIsDebug();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            s_assemblyName = typeof(Program).Assembly.GetName().Name;

            s_handler += CtrlHandler;
            SafeNativeMethods.SetConsoleCtrlHandler(s_handler, add: true);

            string assemblyDirectory = Path.GetDirectoryName(typeof(Program).Assembly.Location) ??
                                       Directory.GetCurrentDirectory();

            if (Directory.GetCurrentDirectory() != assemblyDirectory)
                Directory.SetCurrentDirectory(assemblyDirectory);

            using (StreamWriter traceStream = File.CreateText("trace.txt"))
            using (TextWriterTraceListener traceListener = new TextWriterTraceListener(traceStream))
            {
                Trace.Listeners.Add(traceListener);
                Trace.AutoFlush = true;

                Trace.WriteLine($"v{Util.GetProductVersion()}", s_assemblyName);
                Trace.WriteLine("Started", s_assemblyName);

                if (ShowHelpOrHasNoValidArgs(args))
                    return;

                if (args.Any(x => x == CommandConstant.Import))
                    Importer.Start(args);

                if (args.Any(x => x == CommandConstant.Export))
                    Exporter.Start(args);

                LogClose();
            }
        }

        /// <summary>
        /// Shows the help or has no valid arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private static bool ShowHelpOrHasNoValidArgs(string[] args)
        {
            if (args.Any() && args.First() == CommandConstant.Version)
            {
                if (args.First() == CommandConstant.Version)
                    Console.WriteLine($"v{Util.GetProductVersion()}");

                if (IsDebugBuild)
                    Console.ReadKey(true);

                LogClose();
                return true;
            }

            if (IsValid(args) && !HasHelp(args))
                return false;

            Util.ShowHelp();

            if (Debugger.IsAttached)
                Console.ReadKey(true);

            LogClose();
            return true;
        }

        /// <summary>
        /// Logs the closing of this instance.
        /// </summary>
        private static void LogClose()
        {
            Trace.WriteLine("Closed", s_assemblyName);
        }

        /// <summary>
        /// Determines whether the specified arguments are valid.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private static bool IsValid(string[] args)
            => !(!args.Intersect(CommandConstant.AllowedCommands).Any() ||
                 args.Count(x => x == CommandConstant.Import) > 1 ||
                 args.Count(x => x == CommandConstant.Export) > 1);

        /// <summary>
        /// Determines whether 'help' is requested.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private static bool HasHelp(IEnumerable<string> args)
        {
            return args.Any(x => x == CommandConstant.Help ||
                                 x == SwitchConstant.Help);
        }

        /// <summary>
        /// Handlers the specified control type.
        /// </summary>
        /// <param name="ctrlType">Type of the control.</param>
        /// <returns></returns>
        private static bool CtrlHandler(CtrlType ctrlType)
        {
            switch (ctrlType)
            {
                case CtrlType.CtrlCEvent:
                case CtrlType.CtrlBreakEvent:
                case CtrlType.CtrlCloseEvent:
                case CtrlType.CtrlLogoffEvent:
                case CtrlType.CtrlShutdownEvent:
                {
                    Util.OnClosing();
                    Console.WriteLine();
                    LogClose();
                    Environment.Exit(0);
                    break;
                }
                default:
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Will only execute if DEBUG is set, thus lets us avoid #IFDEF.
        /// </summary>
        [Conditional("DEBUG")]
        private static void CheckIsDebug()
        {
            IsDebugBuild = true;
        }
    }
}
