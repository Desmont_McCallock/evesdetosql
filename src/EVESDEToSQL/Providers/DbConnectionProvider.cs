﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using EVESDEToSQL.Utils;

namespace EVESDEToSQL.Providers
{
    internal abstract class DbConnectionProvider
    {
        protected string Text;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnectionProvider"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="nameOrConnectionString">The name or connection string.</param>
        protected DbConnectionProvider(Type type, string nameOrConnectionString)
        {
            CreateConnection(type, nameOrConnectionString);
        }

        /// <summary>
        /// Gets the connection.
        /// </summary>
        /// <value>
        /// The connection.
        /// </value>
        protected internal DbConnection Connection { get; private set; }

        /// <summary>
        /// Creates the connection.
        /// </summary>
        /// <param name="connectionType">Type of the connection.</param>
        /// <param name="nameOrConnectionString">The name or connection string.</param>
        /// <exception cref="System.ArgumentNullException">nameOrConnectionString</exception>
        private void CreateConnection(Type connectionType, string nameOrConnectionString)
        {
            if (string.IsNullOrWhiteSpace(nameOrConnectionString))
                throw new ArgumentNullException(nameof(nameOrConnectionString));

            if (nameOrConnectionString.StartsWith("name=", StringComparison.OrdinalIgnoreCase))
                nameOrConnectionString = nameOrConnectionString.Replace("name=", string.Empty);

            ConnectionStringSettings connectionStringSetting = ConfigurationManager.ConnectionStrings[nameOrConnectionString];

            string connectionString = connectionStringSetting == null
                ? nameOrConnectionString
                : connectionStringSetting.ConnectionString;

            if (connectionType != typeof(SqlConnection))
            {
                GroupCollection groups = Regex.Match(connectionString, "data source=(?<filePath>.*\\.[a-z]+)",
                    RegexOptions.IgnoreCase | RegexOptions.Compiled).Groups;

                if (groups.Count != 2 || !File.Exists(groups["filePath"].Value))
                {
                    if (Console.CursorLeft > 0)
                        Util.ResetConsoleCursorPosition(Text);

                    Console.WriteLine(@"Database {0}file does not exists!",
                        groups.Count != 2
                            ? string.Empty
                            : $"{Path.GetFileName(groups["filePath"].Value)} ");

                    Console.WriteLine();
                    Console.WriteLine();

                    return;
                }
            }

            if (string.IsNullOrWhiteSpace(connectionString))
            {
                Util.ResetConsoleCursorPosition(Text);
                Console.WriteLine($"Can not find connection string: {nameOrConnectionString}");
                Util.PressAnyKey(-1);
            }

            ConstructorInfo ci = connectionType.GetConstructor(new[]
            {
                typeof(string)
            });

            if (ci == null)
                return;

            Connection = (DbConnection)ci.Invoke(new object[]
            {
                connectionString
            });
        }

        /// <summary>
        /// Opens the connection.
        /// </summary>
        protected internal void OpenConnection()
        {
            if (Connection == null)
                return;

            string databaseTypeName = Connection is SqlConnection
                ? "SQL Server"
                : Connection.GetType().Name.Replace("Connection", string.Empty);

            string databaseName = string.IsNullOrWhiteSpace(Connection.Database) ? "default" : Connection.Database;

            Text = $"Connecting to {databaseTypeName} '{databaseName}' database... ";

            Console.Write(Text);

            try
            {
                Connection.Open();

                Util.ResetConsoleCursorPosition(Text);
                Console.WriteLine($"Connection to {databaseTypeName} '{databaseName}' database: Successful");
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string text = $"Connection to {databaseTypeName} '{databaseName}' database: Failed";
                Util.HandleExceptionWithReason(Text, text, ex.Message);
            }
        }

        /// <summary>
        /// Closes the connection.
        /// </summary>
        protected internal void CloseConnection()
        {
            if (Connection == null || Connection.State == ConnectionState.Closed)
                return;

            string databaseTypeName = Connection is SqlConnection
                ? "SQL Server"
                : Connection.GetType().Name.Replace("Connection", string.Empty);

            string databaseName = string.IsNullOrWhiteSpace(Connection.Database) ? "default" : Connection.Database;

            Text = $"Disconnecting from {databaseTypeName} '{databaseName}' database... ";
            Console.Write(Text);

            try
            {
                Connection.Close();

                Util.ResetConsoleCursorPosition(Text);
                Console.WriteLine($"Disconnection from {databaseTypeName} '{databaseName}' database: Successful");
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                string text = $"Disconnection from {databaseTypeName} '{databaseName}' database: Failed";
                Util.HandleExceptionWithReason(Text, text, ex.Message);
            }
            finally
            {
                Connection = null;
            }
        }

        /// <summary>
        /// SQL insert command text.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected internal static string InsertCommandText(string tableName, IDictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                sb.Append(parameter.Key);
                if (!parameter.Equals(parameters.Last()))
                    sb.Append(", ");
            }
            sb.Append(") VALUES (");

            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                sb.Append(parameter.Value);
                sb.Append(!parameter.Equals(parameters.Last()) ? ", " : ")");
            }

            return $"INSERT INTO {tableName} {sb}";
        }

        /// <summary>
        /// SQL update command text.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected internal static string UpdateCommandText(string tableName, IDictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();
            Dictionary<string, string> pars = parameters
                .Where(par =>
                    !par.Key.StartsWith("columnFilter", StringComparison.OrdinalIgnoreCase) &&
                    !par.Key.StartsWith("id", StringComparison.OrdinalIgnoreCase))
                .ToDictionary(pair => pair.Key, pair => pair.Value);

            foreach (var parameter in pars)
            {
                sb.Append($"{parameter.Key} = {parameter.Value}");
                if (!parameter.Equals(pars.Last()))
                    sb.Append(", ");
            }

            if (string.IsNullOrWhiteSpace(parameters["columnFilter1"]) || string.IsNullOrWhiteSpace(parameters["id1"]))
                return $"UPDATE {tableName} SET {sb}";

            for (int i = 1; i < 5; i++)
            {
                string filterName = "columnFilter" + i;
                string idName = "id" + i;

                if (parameters.ContainsKey(filterName) &&
                    parameters.ContainsKey(idName) &&
                    !string.IsNullOrWhiteSpace(parameters[filterName]) &&
                    !string.IsNullOrWhiteSpace(parameters[idName]))
                {
                    sb.AppendFormat("{0}{1} = {2}", i == 1 ? " WHERE " : " AND ", parameters[filterName], parameters[idName]);
                }
            }

            return $"UPDATE {tableName} SET {sb}";
        }

        /// <summary>
        /// SQL delete command text.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        protected internal static string DeleteCommandText(string tableName, IDictionary<string, string> parameters)
        {
            StringBuilder sb = new StringBuilder();

            if (string.IsNullOrWhiteSpace(parameters["columnFilter1"]) || string.IsNullOrWhiteSpace(parameters["id1"]))
                return $"DELETE {tableName}{sb}";

            for (int i = 1; i < 5; i++)
            {
                string filterName = "columnFilter" + i;
                string idName = "id" + i;

                if (parameters.ContainsKey(filterName) &&
                    parameters.ContainsKey(idName) &&
                    !string.IsNullOrWhiteSpace(parameters[filterName]) &&
                    !string.IsNullOrWhiteSpace(parameters[idName]))
                {
                    sb.Append($"{(i == 1 ? " WHERE " : " AND ")}{parameters[filterName]} = {parameters[idName]}");
                }
            }

            return $"DELETE {tableName}{sb}";
        }

        /// <summary>
        /// Shrinks the database.
        /// </summary>
        protected internal virtual void ShrinkDatabase()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Reorganizes all indexes.
        /// </summary>
        protected internal virtual void ReorganizeAllIndexes()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Drops and Creates the specified table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        protected internal abstract void DropAndCreateTable(string tableName);

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="notify">if set to <c>true</c> notifies on row change.</param>
        protected internal abstract void ImportDataBulk(DataTable dataTable, bool notify = true);
    }
}
