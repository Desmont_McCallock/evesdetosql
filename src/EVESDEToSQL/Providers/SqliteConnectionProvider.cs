﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using EVESDEToSQL.Utils;

namespace EVESDEToSQL.Providers
{
    internal sealed class SqliteConnectionProvider : DbConnectionProvider
    {
        private readonly SQLiteConnection m_sqliteConnection;

        private bool m_isClosing;
        private double m_rowCount;

        /// <summary>
        /// Initializes a new instance of the <see cref="SqliteConnectionProvider"/> class.
        /// </summary>
        /// <param name="nameOrConnectionString">The name or connection string.</param>
        internal SqliteConnectionProvider(string nameOrConnectionString)
            : base(typeof(SQLiteConnection), nameOrConnectionString)
        {
            m_sqliteConnection = (SQLiteConnection)Connection;

            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Util_Closing(object sender, EventArgs e)
        {
            m_isClosing = true;
        }

        /// <summary>
        /// Gets the data table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <returns></returns>
        internal DataTable GetDataTable(string tableName)
        {
            Util.UpdatePercentDone(0);

            // Get the count of the table rows
            string commandText = $"SELECT COUNT(*) FROM [{tableName}]";

            using (SQLiteCommand command = new SQLiteCommand(commandText, m_sqliteConnection))
            {
                Util.SetCommandTimeout(command);

                m_rowCount = Convert.ToDouble(command.ExecuteScalar());
            }

            m_rowCount = Math.Round(m_rowCount * 2, 0, MidpointRounding.AwayFromZero);

            commandText = $"SELECT * FROM [{tableName}]";

            // Usage of SqlDataAdapter to get the table as DataTable
            using (DataTable dataTable = new DataTable(tableName))
            using (SQLiteCommand command = new SQLiteCommand(commandText, m_sqliteConnection))
            using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(command))
            {
                Util.SetCommandTimeout(command);

                dataTable.RowChanged += DataTable_RowChanged;

                // Fill the DataTable with the result of the SQL statement
                adapter.AcceptChangesDuringFill = false;
                adapter.FillSchema(dataTable, SchemaType.Source);
                adapter.Fill(dataTable);

                dataTable.RowChanged -= DataTable_RowChanged;

                return dataTable;
            }
        }

        /// <summary>
        /// Handles the RowChanged event of the DataTable control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataRowChangeEventArgs"/> instance containing the event data.</param>
        private void DataTable_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            Util.UpdatePercentDone(m_rowCount);
        }

        /// <summary>
        /// Drops and Creates the specified table.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        protected internal override void DropAndCreateTable(string tableName)
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            using (DbCommand command = new SQLiteCommand(
                string.Empty,
                m_sqliteConnection,
                m_sqliteConnection.BeginTransaction()))
            {
                Util.SetCommandTimeout(command);

                try
                {
                    foreach (string commandText in Util.GetScriptFor<SQLiteCommand>(tableName)
                        .Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        command.CommandText = commandText;
                        command.ExecuteNonQuery();
                    }

                    command.Transaction.Commit();
                }
                catch (SQLiteException ex)
                {
                    Util.HandleExceptionForCommand(command, ex);

                    command.Transaction?.Rollback();
                }
            }
        }

        /// <summary>
        /// Imports the data bulk.
        /// </summary>
        /// <param name="dataTable">The data table.</param>
        /// <param name="notify">if set to <c>true</c> notifies on row change.</param>
        protected internal override void ImportDataBulk(DataTable dataTable, bool notify = true)
        {
            if (m_isClosing)
                return;

            if (Connection == null)
                return;

            m_rowCount = Math.Round((double)dataTable.Rows.Count * 2, 0, MidpointRounding.AwayFromZero);

            using (SQLiteBulkCopy sqliteBulkCopy = new SQLiteBulkCopy(Connection.ConnectionString))
            {
                sqliteBulkCopy.DestinationTableName = dataTable.TableName;
                sqliteBulkCopy.NotifyAfter = 1;

                if (notify)
                    sqliteBulkCopy.SqliteRowsCopied += SqliteBulkCopy_SqliteRowsCopied;

                try
                {
                    sqliteBulkCopy.WriteToFile(dataTable);
                }
                catch (Exception e)
                {
                    string text = $"Unable to import {dataTable.TableName}                ";
                    Util.HandleExceptionWithReason(Text, text, e.Message);
                }
                finally
                {
                    if (notify)
                        sqliteBulkCopy.SqliteRowsCopied -= SqliteBulkCopy_SqliteRowsCopied;
                }
            }
        }

        /// <summary>
        /// Handles the SqlRowsCopied event of the SqlBulkCopy control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="SqliteRowsCopiedEventArgs"/> instance containing the event data.</param>
        private void SqliteBulkCopy_SqliteRowsCopied(object sender, SqliteRowsCopiedEventArgs e)
        {
            Util.UpdatePercentDone(m_rowCount);
        }

        private sealed class SqliteRowsCopiedEventArgs : EventArgs
        {
            public new static readonly SqliteRowsCopiedEventArgs Empty = new SqliteRowsCopiedEventArgs();
        }

        private sealed class SQLiteBulkCopy : IDisposable
        {
            internal event EventHandler<SqliteRowsCopiedEventArgs> SqliteRowsCopied;

            private string m_destinationTableName;
            private SQLiteConnection m_connection;
            private SQLiteTransaction m_internalTransaction;

            /// <summary>
            /// Initializes a new instance of the <see cref="SQLiteBulkCopy"/> class.
            /// </summary>
            /// <param name="connectionString">The connection string.</param>
            /// <exception cref="System.ArgumentNullException">connectionString</exception>
            internal SQLiteBulkCopy(string connectionString)
            {
                if (string.IsNullOrWhiteSpace(connectionString))
                    throw new ArgumentNullException(nameof(connectionString));

                m_connection = new SQLiteConnection(connectionString);

                if (m_connection.State == ConnectionState.Closed)
                    m_connection.Open();

                m_internalTransaction = m_connection.BeginTransaction();
            }

            /// <summary>
            /// Gets or sets the name of the destination table.
            /// </summary>
            /// <value>
            /// The name of the destination table.
            /// </value>
            /// <exception cref="System.ArgumentNullException"></exception>
            /// <exception cref="System.ArgumentOutOfRangeException"></exception>
            internal string DestinationTableName
            {
                get { return m_destinationTableName; }
                set
                {
                    if (value == null)
                        throw new ArgumentNullException();

                    if (value.Length == 0)
                        throw new ArgumentOutOfRangeException();

                    m_destinationTableName = value;
                }
            }

            /// <summary>
            /// Gets or sets the notify after.
            /// </summary>
            /// <value>
            /// The notify after.
            /// </value>
            internal int NotifyAfter { private get; set; }

            /// <summary>
            /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
            /// </summary>
            void IDisposable.Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            /// <summary>
            /// Releases unmanaged and - optionally - managed resources.
            /// </summary>
            /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
            private void Dispose(bool disposing)
            {
                if (!disposing)
                    return;

                try
                {
                    if (m_internalTransaction == null)
                        return;

                    m_internalTransaction.Dispose();
                    m_internalTransaction = null;
                }
                catch (Exception ex)
                {
                    Util.HandleException(string.Empty, ex.Message);
                }
                finally
                {
                    if (m_connection != null)
                    {
                        m_connection.Close();
                        m_connection = null;
                    }
                }
            }

            /// <summary>
            /// Writes to server.
            /// </summary>
            /// <param name="table">The table.</param>
            /// <exception cref="System.ArgumentNullException">table</exception>
            internal void WriteToFile(DataTable table)
            {
                if (table == null)
                    throw new ArgumentNullException(nameof(table));

                string commandText = $"SELECT * FROM [{table.TableName}]";

                try
                {
                    using (SQLiteCommand command = new SQLiteCommand(commandText, m_connection, m_internalTransaction))
                    using (SQLiteDataAdapter adapter = new SQLiteDataAdapter(command))
                    using (new SQLiteCommandBuilder(adapter))
                    {
                        Util.SetCommandTimeout(command);

                        adapter.RowUpdated += Adapter_RowUpdated;
                        adapter.Update(table);
                        adapter.RowUpdated -= Adapter_RowUpdated;

                        m_internalTransaction.Commit();
                    }
                }
                catch (SQLiteException ex)
                {
                    Util.HandleException(string.Empty, ex.Message);
                    m_internalTransaction.Rollback();
                }
            }

            /// <summary>
            /// Handles the RowUpdated event of the Adapter control.
            /// </summary>
            /// <param name="sender">The source of the event.</param>
            /// <param name="e">The <see cref="RowUpdatedEventArgs"/> instance containing the event data.</param>
            private void Adapter_RowUpdated(object sender, RowUpdatedEventArgs e)
            {
                if (e.RowCount != NotifyAfter)
                    return;

                SqliteRowsCopied?.Invoke(null, SqliteRowsCopiedEventArgs.Empty);
            }
        }
    }
}
