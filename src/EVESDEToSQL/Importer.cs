﻿﻿/* EVESDEToSQL - .NET 4/C# EVE Static Data Export To SQL Server Importer / xSQLx Exporter
 * Copyright (c) 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using EVESDEToSQL.Constants;
using EVESDEToSQL.Importers;
using EVESDEToSQL.Importers.DataDumpToSQL;
using EVESDEToSQL.Importers.SQLiteToSQL;
using EVESDEToSQL.Importers.YamlToSQL;
using EVESDEToSQL.Providers;
using EVESDEToSQL.Utils;
using Microsoft.SqlServer.Management.Smo;

namespace EVESDEToSQL
{
    internal static class Importer
    {
        private static DbConnectionProvider s_sqlConnectionProvider;
        private static DbConnectionProvider s_sqliteConnectionProvider;
        private static IImporter s_dataDumpImporter;

        private static bool s_isClosing;

        /// <summary>
        /// Initializes the <see cref="Importer"/> class.
        /// </summary>
        static Importer()
        {
            Util.Closing += Util_Closing;
        }

        /// <summary>
        /// Handles the Closing event of the Program control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private static void Util_Closing(object sender, EventArgs e)
        {
            s_isClosing = true;

            Console.WriteLine();

            ((DataDumpImporter)s_dataDumpImporter)?.Restore?.Abort();

            s_sqliteConnectionProvider?.CloseConnection();

            s_sqlConnectionProvider?.CloseConnection();

            Util.DeleteSDEFilesIfZipExists();
        }

        /// <summary>
        /// Starts the importer with the specified arguments.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <exception cref="System.Exception">test</exception>
        internal static void Start(string[] args)
        {
            IEnumerable<string> notAllowedArguments = args
                .SkipWhile(x=> x != CommandConstant.Import)
                .TakeWhile(x => x != CommandConstant.Export)
                .Except(CommandConstant.AllowedCommands)
                .Except(ArgumentConstant.AllowedImportArguments);

            if (notAllowedArguments.Any())
            {
                Util.ShowHelp();
                Console.ReadKey(true);
                return;
            }

            bool alsoExport = args.Any(x => x == CommandConstant.Export);

            args = args.Intersect(ArgumentConstant.AllowedImportArguments).ToArray();

            Util.DeleteSDEFilesIfZipExists();
            Util.InflateZipFileIfExists(args);

            Stopwatch stopwatch = Stopwatch.StartNew();
            Util.ResetCounters();

            try
            {
                ImportSDEFiles(args);
            }
            catch (Exception ex)
            {
                Trace.Write(ex.GetRecursiveStackTrace());

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine(@"An unhandled exception was thrown.");
                Console.WriteLine(@"For more info refer to the 'trace.txt' file.");
                Util.PressAnyKey(-1);
            }

            if (!ArgumentConstant.AllowedImportArguments.Except(args).Any() || s_isClosing)
                return;

            Console.WriteLine();
            Console.WriteLine($@"Importing files completed in {stopwatch.Elapsed:g}");

            if (alsoExport)
            {
                Console.WriteLine();
                Console.WriteLine();
                return;
            }

            Util.PressAnyKey();
        }

        /// <summary>
        /// Imports the sde files.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void ImportSDEFiles(string[] args)
        {
            s_sqlConnectionProvider = new SqlConnectionProvider("name=EveStaticData");

            if (!args.Any() || !args.Intersect(new[] { ArgumentConstant.NoRestore }).Any())
                ImportDataDump();

            if (!args.Any() || !args.Intersect(new[] { ArgumentConstant.NoYaml }).Any())
                ImportYamlFiles();

            if (!args.Any() || !args.Intersect(new[] { ArgumentConstant.NoSqlite }).Any())
                ImportSqlite();

            s_sqlConnectionProvider.ReorganizeAllIndexes();
            s_sqlConnectionProvider.ShrinkDatabase();
            
            if (s_sqlConnectionProvider.Connection == null ||
                s_sqlConnectionProvider.Connection.State == ConnectionState.Closed)
            {
                return;
            }

            s_sqlConnectionProvider.CloseConnection();
        }

        /// <summary>
        /// Imports the data dump.
        /// </summary>
        /// <returns></returns>
        private static void ImportDataDump()
        {
            if (s_isClosing)
                return;

            if (Util.CheckSDEDirectoryExists())
                return;

            s_dataDumpImporter = new DataDumpImporter(s_sqlConnectionProvider, new Restore());
            s_dataDumpImporter.ImportData();
        }

        /// <summary>
        /// Imports the yaml files.
        /// </summary>
        private static void ImportYamlFiles()
        {
            if (s_isClosing)
                return;

            ((SqlConnectionProvider)s_sqlConnectionProvider).CreateDatabaseIfNotExists();

            s_sqlConnectionProvider.OpenConnection();
            IImporter yamlImporter = new YamlImporter(s_sqlConnectionProvider);
            yamlImporter.ImportData();
        }

        /// <summary>
        /// Imports the sqlite.
        /// </summary>
        /// <returns></returns>
        private static void ImportSqlite()
        {
            if (s_isClosing)
                return;

            if (Util.CheckSDEDirectoryExists())
                return;

            if (s_sqlConnectionProvider.Connection.State == ConnectionState.Closed)
                s_sqlConnectionProvider.OpenConnection();

            string connectionString =
                $"data source={Path.Combine(Directory.GetCurrentDirectory(), @"SDEFiles\universeDataDx.db")}";

            s_sqliteConnectionProvider = new SqliteConnectionProvider(connectionString);
            s_sqliteConnectionProvider.OpenConnection();

            IImporter sqliteImporter = new SqliteImporter(s_sqliteConnectionProvider, s_sqlConnectionProvider);
            sqliteImporter.ImportData();
        }
    }
}
