EVESDEToSQL - .NET 4/C# 
--
EVE Static Data Export To SQL Server Importer / xSQLx Exporter
--
Copyright © 2015 Jimi 'Desmont McCallock' C <jimikar@gmail.com>

DESCRIPTION
--
EVESDEToSQL is a tool to import the EVE SDE back into an SQL Server instance and export it to various SQL formats.

LICENSE
--
EVESDEToSQL is distributed under [**MIT license**](https://opensource.org/licenses/MIT) (see license.txt that is included with the distribution).

SUPPORTED OS
--
Windows XP, Vista, 7, 8, 8.1, 10

REQUIREMENTS
--
- x86/x64 compatible processor.
- .NET Framework 4 or higher (possible support from Mono, although not tested)
- An SQL Server (v2014+) instance installation (any edition).

Notice: The SDE files are not distributed along with the tool. You will have to get them from the [EVE Developers Resources](https://developers.eveonline.com/resource/resources).

FEATURES
--
**- Importation**

* Restores the data dump, yaml and sqlite db files back into an SQL Server (no need to restore the data dump via SSMS).
* Imports any SDE (literally any, but in case you find an SDE with issues please let me known)
* Supports command line arguments (use 'help' to see the list)
* Cleaner config file (it only contains the needed connection info to your SQL Server, and it's the only thing that you may need to modify)
* Ulta-High Speed (it takes less than 2 minutes to import the entire SDE, depending on your machine's cpu power)
* One place for the SDE file(s) (just drop the SDE zip file as is or the files contained in the SDE zip file, into the 'SDEFiles' folder)

**- Exportation**

* Exports an SQL Server DATADUMP backup file
* Exports an SQLite database file
* Exports an MS Access database file ('mdb' as default, use '/ace' switch for 'accdb')
* Exports an MySQL dump file
* Exports an PostgreSQL dump file
* Exports individual CSV (semicolon seperated version) files
* Ability to export specific tables in any dump format 

HOW TO USE
--
***Importation Usage***

*EVESDEToSQL import* (Imports everything)

 		-norestore  Excludes the restoration of the SQL Server DATADUMP backup file
 		-noyaml     Excludes the importation of the yaml files
 		-nosqlite   Excludes the importation of the sqlite files

***Exportation Usage***

*EVESDEToSQL export* (Exports the SDE in every supported format)

		-sql [/et: listOfTables]           Exports an SQL Server DATADUMP backup file
		-mysql [/et: listOfTables]         Exports an MySQL dump file
		-postgresql [/et: listOfTables]    Exports an PostgreSQL dump file
		-sqlite [/et: listOfTables]        Exports an SQLite database file
		-access [/ace] [/et: listOfTables] Exports an MS Access database file
		-csv [/et: listOfTables]           Exports individual CSV files

SDE MAPPING
--
***Yaml files mapping:***

	blueprints.yaml -> invBlueprintTypes, ramTypeRequirements
	categoryIDs.yaml -> invCategories, translationTables, trnTranslationColumns, trnTranslations
	certificates.yaml -> crtClasses, crtCertificates, crtRecommendations, crtRelationships
	graphicIDs.yaml -> eveGraphics
	groupIDs.yaml -> invGroups, translationTables, trnTranslationColumns, trnTranslations
	iconIDs.yaml -> eveIcons
	skinLicenses.yaml -> sknLicenses
	skinMaterials.yaml -> sknMaterials
	skins.yaml -> sknSkins
	tournamentRuleSets.yaml -> ignored
	typeIDs.yaml -> invTypes, dgmMasteries, dgmTypeMasteries, dgmTraits, dgmTypeTraits, translationTables, trnTranslationColumns, trnTranslations

***Sqlite files mapping:***

	universeDataDx.db -> mapCelestialStatistics, mapConstellationJumps, mapConstellations, mapDenormalize, mapJumps, mapLandmarks, mapLocationScenes, mapLocationWormholeClasses, mapRegionJumps, mapRegions, mapSolarSystemJumps, mapSolarSystems
